using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SalesTax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCalc_Click(object sender, EventArgs e)
    {
        double dblSaleAmt = double.Parse(txtSaleAmt.Text);
        const double dblTaxRate = .06725;

        double dblTaxAmt = dblSaleAmt * dblTaxRate;

        double dblTotal = dblSaleAmt + dblTaxAmt;

        txtTax.Text = dblTaxAmt.ToString("C");
        txtTotal.Text = dblTotal.ToString("C");
    }
}
