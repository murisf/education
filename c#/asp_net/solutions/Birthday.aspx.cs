using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Birthday : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCompute_Click(object sender, EventArgs e)
    {
        DateTime dteDOB = DateTime.Parse(txtDOB.Text);

        TimeSpan ts = DateTime.Now.Subtract(dteDOB);

        double dblAge = Math.Round(ts.Days / 365.25,0);

        lblAge.Text = "You are " + dblAge.ToString() + " years old.";
    }
}
