using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Colors : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnRed_Click(object sender, EventArgs e)
    {
        lblSample.ForeColor = System.Drawing.Color.Red;
    }
    protected void brnGreen_Click(object sender, EventArgs e)
    {
        lblSample.ForeColor = System.Drawing.Color.Green;
    }
    protected void btnBlue_Click(object sender, EventArgs e)
    {
        lblSample.ForeColor = System.Drawing.Color.Blue;
    }
    protected void btnRandom_Click(object sender, EventArgs e)
    {
        Random rnd = new Random();
        lblSample.ForeColor = System.Drawing.Color.FromArgb(rnd.Next(255),rnd.Next(255),rnd.Next(255));
    }
}
