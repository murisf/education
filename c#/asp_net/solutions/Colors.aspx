<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Colors.aspx.cs" Inherits="Colors" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblSample" runat="server" Height="36px" Text="Label" Width="261px"></asp:Label><br />
        <asp:Button ID="btnRed" runat="server" Text="Button" OnClick="btnRed_Click" />
        <asp:Button ID="brnGreen" runat="server" Text="Button" OnClick="brnGreen_Click" />
        <asp:Button ID="btnBlue" runat="server" Text="Button" OnClick="btnBlue_Click" />
        <asp:Button ID="btnRandom" runat="server" Text="Button" OnClick="btnRandom_Click" /></div>
    </form>
</body>
</html>
