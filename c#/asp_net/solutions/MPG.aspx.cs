using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MPG : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnMPG_Click(object sender, EventArgs e)
    {
        int intStart = int.Parse(txtStart.Text);
        int intEnd = int.Parse(txtEnd.Text);
        int intGallons = int.Parse(txtGallons.Text);

        double dblMiles = intEnd - intStart;

        double dblMPG = dblMiles / intGallons;

        txtMPG.Text = dblMPG.ToString("N1");
    }
}
