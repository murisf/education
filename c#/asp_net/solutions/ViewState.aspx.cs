using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ViewState : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["pageNum"] = 0;
            lblField.Text = "Name:";

        }
        else
        {
            int intViewState = int.Parse(ViewState["pageNum"].ToString());
            switch (intViewState)
            {
                case 0:
                    ViewState["result"] += lblField.Text + txtField.Text + "<br>";
                    lblField.Text = "Street:";
                    txtField.Text = "";
                    break;
                case 1:
                    ViewState["result"] += lblField.Text + txtField.Text + "<br>";
                    lblField.Text = "City:";
                    txtField.Text = "";
                    break;
                case 2:
                    ViewState["result"] += lblField.Text + txtField.Text + "<br>";
                    lblField.Text = "State:";
                    txtField.Text = "";
                    break;
                case 3:
                    ViewState["result"] += lblField.Text + txtField.Text + "<br>";
                    lblField.Text = "Zip:";
                    txtField.Text = "";
                    break;
                case 4:
                    ViewState["result"] += lblField.Text + txtField.Text + "<br>";
                    lblField.Text = ViewState["result"].ToString();
                    txtField.Visible = false;
                    btnField.Text = "Restart";
                    break;
                default:
                    ViewState["pageNum"] = -1;
                    ViewState["result"] = "";
                    lblField.Text = "Name:";
                    txtField.Visible = true;
                    txtField.Text = "";
                    btnField.Text = "Submit";
                    break;

            }
            intViewState += 1;
            ViewState["pageNum"] = intViewState ;

        }
    }
}
