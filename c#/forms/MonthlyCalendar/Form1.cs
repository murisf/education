﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MonthlyCalendar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Label[] lblDays = new Label[42];

        int colCnt = 7;
        int lblWidth = 32;
        int lblHeight = 24; 
        int leftBorder = 20;
        int topBorder = 85;

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 42; i++)
            {
                Label newlbl = new Label();
                this.Controls.Add(newlbl);
                newlbl.TextAlign = ContentAlignment.MiddleRight;
                newlbl.Size = new Size(lblWidth, lblHeight);
                lblDays[i] = newlbl;
                int intTop = Convert.ToInt16(i / colCnt) * lblHeight + topBorder;
                int intLeft = (i % colCnt) * lblWidth + leftBorder;
                newlbl.Location = new Point(intLeft, intTop);
            }

            DateTime cal = DateTime.Now;

            int mo = cal.Month;
            int lblno = 0;

            cal = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            int wday = Convert.ToInt16(cal.DayOfWeek);

            for (int i = Convert.ToInt16(DayOfWeek.Sunday); i < wday; i++)
            {
                lblno += 1;
            }

            while (cal.Month == mo)
            {
                lblDays[lblno].Text = cal.Day + "";
                if (cal.Day == DateTime.Now.Day)
                {
                    lblDays[lblno].BorderStyle = BorderStyle.FixedSingle;
                    lblDays[lblno].ForeColor = Color.Blue;
                }
                lblno += 1;
                cal = cal.AddDays(1);
            }

            lblOutput.Text = DateTime.Now.ToString("MMMM") + " " +
                DateTime.Now.Year;
        }
    }
}
