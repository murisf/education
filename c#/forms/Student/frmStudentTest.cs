﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjStudent
{
    public partial class frmStudentTest : Form
    {
        public frmStudentTest()
        {
            InitializeComponent();
        }

        Student stud1 = new Student("A001", "Gates", "Bill");

        private void Form1_Load(object sender, EventArgs e)
        {
            txtID.Text = stud1.Id;
            txtName.Text = stud1.FullName;
            txtCurrCredits.Text = stud1.Credits.ToString();
            txtTotalCredits.Text = stud1.TotalCreds.ToString();
            //MessageBox.Show("Max Credits: " + Student.MAX_CREDITS);
        }

        private void btnAdd5_Click(object sender, EventArgs e)
        {
            stud1.AddCredits(5);
            txtCurrCredits.Text = stud1.Credits.ToString();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            stud1.ResetCredits();
            txtCurrCredits.Text = stud1.Credits.ToString();
            txtTotalCredits.Text = stud1.TotalCreds.ToString();
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show(stud1.GetInfo());
        }

        private void btnAdd1_Click(object sender, EventArgs e)
        {
            stud1.AddCredits();
            txtCurrCredits.Text = stud1.Credits.ToString();
        }
    }
}
