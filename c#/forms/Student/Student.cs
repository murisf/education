﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjStudent
{
    public class Student
    {
        public const int MAX_CREDITS = 18;
        // fields(attributes)
        private String studentID;
        private String lastName;
        private String firstName;
        private int currCredits;
        private int totalCredits;
        // constructors
        public Student(String id, String first, String last)
        {
            studentID = id;
            lastName = last;
            firstName = first;
            currCredits = 0;
            totalCredits = 0;
        }
        public Student(String id, String first, String last,
                int transferCredits)
        {
            studentID = id;
            lastName = last;
            firstName = first;
            currCredits = 0;
            totalCredits = transferCredits;
        }
        // properties(getter and setters)
        public String Id
        {
            get
            {
                return studentID;
            }
        }
        public String FullName
        {
            get
            {
                return firstName + " " + lastName;
            }
        }
        public int Credits
        {
            get
            {
                return currCredits;
            }
        }
        public int TotalCreds
        {
            get
            {
                return totalCredits;
            }
        }
        public String StudentFirstName
        {
            get
            {
                return firstName;
            }
            set 
            {
                firstName = value;
            }
        }
        // methods(behaviors)
        public void AddCredits(int newCredits)
        {
            int tempCredits = currCredits + newCredits;

            if (tempCredits > MAX_CREDITS)
                MessageBox.Show("Credit limit exceeded");
            else
                currCredits = tempCredits;
        }

        public void AddCredits()
        {
            AddCredits(1);
        }

        public void ResetCredits()
        {
            totalCredits += currCredits;
            currCredits = 0;
        }
        public String GetInfo()
        {
            String info = "";
            info += "Student ID: " + studentID + "\n";
            info += "Student Name: " + FullName + "\n";
            info += "Current Credits: " + currCredits + "\n";
            info += "Total Credits: " + totalCredits + "\n";

            return info;
        }
    }
}
