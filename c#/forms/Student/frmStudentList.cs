﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace prjStudent
{
    public partial class frmStudentList : Form
    {
        public frmStudentList()
        {
            InitializeComponent();
        }

        ArrayList students = new ArrayList(12);

        private void frmStudentList_Load(object sender, EventArgs e)
        {
            students.Add(new Student("A001", "Larry", "Limbo"));
            students.Add(new Student("A002", "Becky", "Blue", 30));
            students.Add(new Student("A003", "Simon", "Smith"));
            students.Add(new Student("A004", "Fanny", "Fargo"));
            students.Add(new Student("A005", "Pete", "Smith"));
            students.Add(new Student("A006", "Bill", "Bailey"));
            students.Add(new Student("A007", "John", "Long"));
            students.Add(new Student("A008", "Van", "Hill"));
            students.Add(new Student("A009", "Cindy", "Jones"));
            students.Add(new Student("A010", "Sean", "Childs"));
            students.Add(new Student("A011", "Marcy", "Michaels"));

            lblCount.Text = "Count: " + students.Count;
            dgvStudents.DataSource = students;
        }

        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            students.Insert(0, new Student("A012", "Kim", "Casey"));
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            dgvStudents.DataSource = null;
            dgvStudents.DataSource = students;
            lblCount.Text = "Count: " + students.Count;
        }

        private void btnAddCredits_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < students.Count; i++)
            {
                Student s = (Student) students[i];
                s.AddCredits(5);
            }

            dgvStudents.Refresh();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            students[0] = new Student("A013", "Kelly", "Casey", 12);
            dgvStudents.Refresh();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                students.RemoveAt(0);
                RefreshGrid();
            }
            catch
            {
                MessageBox.Show("NO more students...stop clicking that button");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            students.Clear();
            RefreshGrid();
        }
    }
}
