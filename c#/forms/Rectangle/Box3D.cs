﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prjRectangle
{
    public class Box3D: Rectangle
    {
        private int boxDepth;

        public Box3D() : base()
        {
            boxDepth = 1;
        }
        public Box3D(int sameSize)
            : base(sameSize)
        {
            boxDepth = sameSize;
        }
        public Box3D(int h, int w, int d)
            : base(h,w)
        {
            boxDepth = d;
        }

        public int BoxDepth
        {
            get { return boxDepth; }
            set { boxDepth = value; }
        }
        public int Volume
        {
            get { return base.Height * base.Width * boxDepth; }
        }
        public override int Area
        {
            get
            {
                return (base.Area * 2) +
                       (boxDepth * Height * 2) +
                       (boxDepth * Width * 2);
            } 
        }
    }
}
