﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjRectangle
{
    public partial class frmBoxTest : Form
    {
        public frmBoxTest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Box3D box1 = new Box3D();
            String info = "Height: " + box1.Height + "\n";
            info += "Width: " + box1.Width + "\n";
            info += "Depth: " + box1.BoxDepth + "\n";
            info += "Area: " + box1.Area + "\n";
            info += "Volume: " + box1.Volume + "\n";
            MessageBox.Show(info);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Box3D box1 = new Box3D(4);
            String info = "Height: " + box1.Height + "\n";
            info += "Width: " + box1.Width + "\n";
            info += "Depth: " + box1.BoxDepth + "\n";
            info += "Area: " + box1.Area + "\n";
            info += "Volume: " + box1.Volume + "\n";
            MessageBox.Show(info);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Box3D box1 = new Box3D(4,5,6);
            String info = "Height: " + box1.Height + "\n";
            info += "Width: " + box1.Width + "\n";
            info += "Depth: " + box1.BoxDepth + "\n";
            info += "Area: " + box1.Area + "\n";
            info += "Volume: " + box1.Volume + "\n";
            MessageBox.Show(info);
        }
    }
}
