﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjRectangle
{
    public partial class frmRecTest : Form
    {
        public frmRecTest()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            Rectangle rec1 = new Rectangle();
            txtH.Text = rec1.Height.ToString();
            txtW.Text = rec1.Width.ToString();
            txtArea.Text = rec1.Area.ToString();
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            Rectangle rec1 = new Rectangle(4);
            txtH.Text = rec1.Height.ToString();
            txtW.Text = rec1.Width.ToString();
            txtArea.Text = rec1.Area.ToString();
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            Rectangle rec1 = new Rectangle(4,5);
            txtH.Text = rec1.Height.ToString();
            txtW.Text = rec1.Width.ToString();
            txtArea.Text = rec1.Area.ToString();
        }

        private void btnArea_Click(object sender, EventArgs e)
        {
            Rectangle rec1 = new Rectangle(int.Parse(txtH.Text),
                    int.Parse(txtW.Text));
            txtArea.Text = rec1.Area.ToString();
        }
    }
}
