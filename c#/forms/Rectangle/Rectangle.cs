﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prjRectangle
{
    public class Rectangle
    {
        private int recHeight;
        private int recWidth;

        public Rectangle()
        {
            recHeight = 1;
            recWidth = 1;
        }
        public Rectangle(int sameSize)
        {
            recHeight = sameSize;
            recWidth = sameSize;
        }
        public Rectangle(int h, int w)
        {
            recHeight = h;
            recWidth = w;
        }

        public int Height
        {
            get
            {
                return recHeight;
            }
            set
            {
                recHeight = value;
            }
        }
        public int Width
        {
            get
            {
                return recWidth;
            }
            set
            {
                recWidth = value;
            }
        }

        public virtual int Area
        {
            get
            {
                return recHeight * recWidth;
            }

        }
    }
}
