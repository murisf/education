﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace DateTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DateTime bday = new DateTime(1969, 09, 16); // constructor
            bday = bday.AddDays(3456); // method
            lblSample.Text = bday.ToString() + "\n";
            lblSample.Text += "Month: " + bday.Month + "\n"; // property
            lblSample.Text += bday.ToString("F") + "\n";
            lblSample.Text += bday.ToString("dddd") + "\n";
            lblSample.Text += bday.ToString("f",new CultureInfo("fr-FR")) + "\n";
        }
    }
}
