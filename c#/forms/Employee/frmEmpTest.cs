﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjEmployee
{
    public partial class frmEmpTest : Form
    {
        public frmEmpTest()
        {
            InitializeComponent();
        }

        Employee emp1 = new Employee("Bill Gates", "Janitor", 12);

        private void frmEmpTest_Load(object sender, EventArgs e)
        {
            txtName.Text = emp1.EmployeeName;
            txtTitle.Text = emp1.JobTitle;
            txtRate.Text = emp1.HourlyRate.ToString("C");
            txtHours.Text = emp1.EmpHours.ToString();
            txtWeeklyPay.Text = emp1.GetWeeklyPay().ToString("C");
        }

        private void btnRaise_Click(object sender, EventArgs e)
        {
            emp1.RaisePay(.10);
            txtRate.Text = emp1.HourlyRate.ToString("C");
            txtWeeklyPay.Text = emp1.GetWeeklyPay().ToString("C");
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show(emp1.GetInfo());
        }

        private void btnHours_Click(object sender, EventArgs e)
        {
            emp1.EmpHours = double.Parse(txtHours.Text);
            txtWeeklyPay.Text = emp1.GetWeeklyPay().ToString("C");
        }
    }
}
