﻿namespace prjEmployee
{
    partial class frmEmpList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvEmps = new System.Windows.Forms.DataGridView();
            this.lblPayroll = new System.Windows.Forms.Label();
            this.btnRaise = new System.Windows.Forms.Button();
            this.lblCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmps)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvEmps
            // 
            this.dgvEmps.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEmps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmps.Location = new System.Drawing.Point(16, 12);
            this.dgvEmps.Name = "dgvEmps";
            this.dgvEmps.Size = new System.Drawing.Size(607, 317);
            this.dgvEmps.TabIndex = 0;
            // 
            // lblPayroll
            // 
            this.lblPayroll.AutoSize = true;
            this.lblPayroll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPayroll.Location = new System.Drawing.Point(13, 344);
            this.lblPayroll.Name = "lblPayroll";
            this.lblPayroll.Size = new System.Drawing.Size(69, 16);
            this.lblPayroll.TabIndex = 1;
            this.lblPayroll.Text = "Payroll =";
            // 
            // btnRaise
            // 
            this.btnRaise.Location = new System.Drawing.Point(465, 344);
            this.btnRaise.Name = "btnRaise";
            this.btnRaise.Size = new System.Drawing.Size(158, 32);
            this.btnRaise.TabIndex = 2;
            this.btnRaise.Text = "Raise Pay 10%";
            this.btnRaise.UseVisualStyleBackColor = true;
            this.btnRaise.Click += new System.EventHandler(this.btnRaise_Click);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.Location = new System.Drawing.Point(13, 369);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(59, 16);
            this.lblCount.TabIndex = 3;
            this.lblCount.Text = "Count =";
            // 
            // frmEmpList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 394);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.btnRaise);
            this.Controls.Add(this.lblPayroll);
            this.Controls.Add(this.dgvEmps);
            this.Name = "frmEmpList";
            this.Text = "frmEmpList";
            this.Load += new System.EventHandler(this.frmEmpList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmps)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEmps;
        private System.Windows.Forms.Label lblPayroll;
        private System.Windows.Forms.Button btnRaise;
        private System.Windows.Forms.Label lblCount;
    }
}