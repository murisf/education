﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace prjEmployee
{
    public class Employee
    {
        public const double MAX_RATE = 50;

        private String empName, empTitle;
        private double empHrlyRate;
        private DateTime empHireDate;
        private String empLogin;
        private double empHours;

   

        public Employee(String name, String title)
        {
            empName = name;
            empTitle = title;
            empHrlyRate = 10;
            empHireDate = DateTime.Now;
            empLogin = empName.Substring(0, 3) + empTitle.Substring(0, 3);
            empHours = 40;
        }
        public Employee(String name, String title, double rate)
        {
            empName = name;
            empTitle = title;
            if (rate > MAX_RATE)
                empHrlyRate = MAX_RATE;
            else
                empHrlyRate = rate;
            empHireDate = DateTime.Now;
            empLogin = empName.Substring(0, 3) + empTitle.Substring(0, 3);
            empHours = 40;
        }
        public String EmployeeName
        {
            get
            {
                return empName;
            }
        }
       
        public String JobTitle
        {
            get
            {
                return empTitle;
            }
            set
            {
                empTitle = value;
            }
        }

        public String LogonID
        {
            get
            {
                return empLogin;
            }
            set
            {
                empLogin = value;
            }
        }
        public double HourlyRate
        {
            get
            {
                return empHrlyRate;
            }
        }
        public double EmpHours
        {
            get { return empHours; }
            set { empHours = value; }
        }
        public double WeeklyPay
        {
            get
            {
                return GetWeeklyPay();
            }
        }

        public void RaisePay(double raisePerc)
        {
            double proposedRate = empHrlyRate * (1 + raisePerc);
            if (proposedRate > MAX_RATE)
            {
                empHrlyRate = MAX_RATE;
                MessageBox.Show("You have reached the max rate");
            }
            else
                empHrlyRate = proposedRate;
        }
        public String GetInfo()
        {
            String info = "";
            info += "Name: " + empName + "\n";
            info += "Title: " + empTitle + "\n";
            info += "Hourly Rate: " + empHrlyRate.ToString("c") + "\n";
            info += "Hours worked: " + empHours.ToString() + "\n";
            info += "Weekly Pay: " + GetWeeklyPay().ToString("c") + "\n";
            info += "Hire Date: " + empHireDate.ToString("d") + "\n";
            info += "Logon: " + empLogin + "\n";
            return info;
        }

        public double GetWeeklyPay()
        {
            return empHrlyRate * empHours;
        }
    }
}
