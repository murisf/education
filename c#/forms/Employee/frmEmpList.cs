﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace prjEmployee
{
    public partial class frmEmpList : Form
    {
        public frmEmpList()
        {
            InitializeComponent();
        }

        ArrayList emps = new ArrayList(20);

        private void frmEmpList_Load(object sender, EventArgs e)
        {
            emps.Add(new Employee("Bill Gates", "CEO", 20));
            emps.Add(new Employee("Bob Gates", "TOP", 4));
            emps.Add(new Employee("Brian Gates", "VIP", 5));
            emps.Add(new Employee("Betty Gates", "VIP", 6));
            emps.Add(new Employee("Beth Gates", "CFO", 7));
            emps.Add(new Employee("Benard Gates", "PRES", 8));
            emps.Add(new Employee("Brent Gates", "EXE", 3));
            emps.Add(new Employee("Brenda Gates", "MKT", 2));
            emps.Add(new Employee("Belinda Gates", "BOS", 3));
            emps.Add(new Employee("Bernice Gates", "OWN", 5));
            emps.Add(new Employee("Brad Gates", "SEC", 8));
            emps.Add(new Employee("Barkley Gates", "PMP", 33));
            emps.Add(new Employee("Bailey Gates", "PAM", 12));
            emps.Add(new Employee("Bonnie Gates", "ACT", 15));
            emps.Add(new Employee("Ben Gates", "CHL", 16));
            emps.Add(new Employee("Bart Gates", "AVP", 11));
            emps.Add(new Employee("Brett Gates", "SLR", 15));
            emps.Add(new Employee("Brianna Gates", "GOP", 18));
            emps.Add(new Employee("Barry Gates", "BOP", 22));
            emps.Add(new Employee("Blake Gates", "ASM", 21));

            RefreshGrid();
        }

        private void RefreshGrid()
        {
            dgvEmps.DataSource = null;
            dgvEmps.DataSource = emps;

            dgvEmps.Columns["HourlyRate"].DefaultCellStyle.Format = "C";
            dgvEmps.Columns["WeeklyPay"].DefaultCellStyle.Format = "C";
            dgvEmps.Columns["HourlyRate"].DefaultCellStyle.Alignment =
                    DataGridViewContentAlignment.MiddleRight;
            dgvEmps.Columns["WeeklyPay"].DefaultCellStyle.Alignment =
                    DataGridViewContentAlignment.MiddleRight;

            double dblPayroll = 0;
            for (int i = 0; i < emps.Count; i++)
            {
                Employee emp = (Employee) emps[i];
                dblPayroll += emp.GetWeeklyPay();
            }
            lblPayroll.Text = "Payroll = " + dblPayroll.ToString("C");
            lblCount.Text = "Count = " + emps.Count;
        }

        private void btnRaise_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < emps.Count; i++)
            {
                Employee emp = (Employee)emps[i];
                emp.RaisePay(.10);
            }
            RefreshGrid();
        }
    }
}
