﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ex1_Christmas
{
    public partial class frmChristmas : Form
    {
        public frmChristmas()
        {
            InitializeComponent();
        }

        private void frmChristmas_Load(object sender, EventArgs e)
        {
            for (int year = DateTime.Now.Year; year > 1899; year--)
            {
                cboYear.Items.Add(year.ToString());
            }
        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = int.Parse(cboYear.SelectedItem.ToString());

            DateTime xmas = new DateTime(year, 12, 25);

            lblOut.Text = "In " + year + ", Chirstmas is " +
                xmas.ToString("dddd");
        }
    }
}
