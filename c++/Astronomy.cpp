#include <iostream>
#include <cstdlib>
using namespace std;

int main ()
{
	
	int choice;
	
		cout << "\n\t Astronomy\n\n";
		cout << " 1. Telescope Basics \n\n";
		cout << " 2. Binocular Basics \n\n";
		cout << " 3. Exit \n";
		cout << " \nThe famous Earth rise picture was taken during the Apollo 8 mission." << endl;
		
		cin >> choice;
		
	switch (choice)
	{
		case 1: cout << "Empty for now, but will call a function.\n";
				break;
		case 2: cout << "Astronomy binoculars should have at least 40 mm diameter lenses.\n";
				cout << "Magnification should at least be 7 but at most 10 for handheld binoculars.\n";
				cout << "Other characteristics should be: porro-prism, quality lenses such as Bak-4 instead\n";
				cout << "of BK7, and color optics.\n";
				break;
		case 3: exit (1);
	}
	
	system ("pause");
	
	return 0;
}