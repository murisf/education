<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProdForm.aspx.vb" Inherits="ProdForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FormView ID="FormView1" runat="server" AllowPaging="True" DataKeyNames="Partnum"
            DataSourceID="AccessDataSource1" CellPadding="4" ForeColor="#333333" HeaderText="Items">
            <EditItemTemplate>
                Partnum:
                <asp:Label ID="PartnumLabel1" runat="server" Text='<%# Eval("Partnum") %>'></asp:Label><br />
                Descrip:
                <asp:TextBox ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>'></asp:TextBox><br />
                Auto:
                <asp:TextBox ID="AutoTextBox" runat="server" Text='<%# Bind("Auto") %>'></asp:TextBox><br />
                Quantity:
                <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox><br />
                Price:
                <asp:TextBox ID="PriceTextBox" runat="server" Text='<%# Bind("Price") %>'></asp:TextBox><br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                    Text="Update"></asp:LinkButton>
                <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                    Text="Cancel"></asp:LinkButton>
            </EditItemTemplate>
            <InsertItemTemplate>
                Partnum:
                <asp:TextBox ID="PartnumTextBox" runat="server" Text='<%# Bind("Partnum") %>'>
                </asp:TextBox><br />
                Descrip:
                <asp:TextBox ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>'>
                </asp:TextBox><br />
                Auto:
                <asp:TextBox ID="AutoTextBox" runat="server" Text='<%# Bind("Auto") %>'>
                </asp:TextBox><br />
                Quantity:
                <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>'>
                </asp:TextBox><br />
                Price:
                <asp:TextBox ID="PriceTextBox" runat="server" Text='<%# Bind("Price") %>'>
                </asp:TextBox><br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                    Text="Insert">
                </asp:LinkButton>
                <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                    Text="Cancel">
                </asp:LinkButton>
            </InsertItemTemplate>
            <ItemTemplate>
                <table>
                    <tr>
                        <td style="width: 263px; height: 92px" valign="top">
                            <table border="5">
                                <tr>
                                    <td style="width: 100px">
                                        Partnum:
                                    </td>
                                    <td style="width: 163px">
                <asp:Label ID="PartnumLabel" runat="server" Text='<%# Eval("Partnum") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        Descrip:
                                    </td>
                                    <td style="width: 163px">
                <asp:Label ID="DescripLabel" runat="server" Text='<%# Bind("Descrip") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        Auto:
                                    </td>
                                    <td style="width: 163px">
                <asp:Label ID="AutoLabel" runat="server" Text='<%# Bind("Auto") %>'></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 280px; height: 92px" valign="top">
                            <table border="5">
                                <tr>
                                    <td style="width: 100px">
                                        Quantity:
                                    </td>
                                    <td style="width: 132px">
                <asp:Label ID="QuantityLabel" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        Price:&nbsp;</td>
                                    <td style="width: 132px">
                                        <asp:Label ID="PriceLabel" runat="server" Text='<%# Bind("Price") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        Inventory Value:</td>
                                    <td style="width: 132px">
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("InvValue") %>'></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <PagerSettings Mode="NumericFirstLast" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="X-Large" ForeColor="White" />
            <HeaderTemplate>
                Inventory
            </HeaderTemplate>
        </asp:FormView>
        <asp:AccessDataSource ID="AccessDataSource1" runat="server" ConflictDetection="CompareAllValues"
            DataFile="~/App_Data/Products.mdb" DeleteCommand="DELETE FROM [Products] WHERE [Partnum] = ? AND [Descrip] = ? AND [Auto] = ? AND [Quantity] = ? AND [Price] = ?"
            InsertCommand="INSERT INTO [Products] ([Partnum], [Descrip], [Auto], [Quantity], [Price]) VALUES (?, ?, ?, ?, ?)"
            OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT Partnum, Descrip, Auto, Quantity, Price, format(Quantity * Price, 'currency') AS InvValue FROM Products"
            UpdateCommand="UPDATE [Products] SET [Descrip] = ?, [Auto] = ?, [Quantity] = ?, [Price] = ? WHERE [Partnum] = ? AND [Descrip] = ? AND [Auto] = ? AND [Quantity] = ? AND [Price] = ?">
            <DeleteParameters>
                <asp:Parameter Name="original_Partnum" Type="String" />
                <asp:Parameter Name="original_Descrip" Type="String" />
                <asp:Parameter Name="original_Auto" Type="String" />
                <asp:Parameter Name="original_Quantity" Type="Int16" />
                <asp:Parameter Name="original_Price" Type="Single" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Descrip" Type="String" />
                <asp:Parameter Name="Auto" Type="String" />
                <asp:Parameter Name="Quantity" Type="Int16" />
                <asp:Parameter Name="Price" Type="Single" />
                <asp:Parameter Name="original_Partnum" Type="String" />
                <asp:Parameter Name="original_Descrip" Type="String" />
                <asp:Parameter Name="original_Auto" Type="String" />
                <asp:Parameter Name="original_Quantity" Type="Int16" />
                <asp:Parameter Name="original_Price" Type="Single" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Partnum" Type="String" />
                <asp:Parameter Name="Descrip" Type="String" />
                <asp:Parameter Name="Auto" Type="String" />
                <asp:Parameter Name="Quantity" Type="Int16" />
                <asp:Parameter Name="Price" Type="Single" />
            </InsertParameters>
        </asp:AccessDataSource>
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" DataKeyNames="Partnum,Customer,Saledate" DataSourceID="AccessDataSource2"
            ForeColor="#333333" GridLines="None">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                <asp:BoundField DataField="Customer" HeaderText="Customer" ReadOnly="True" SortExpression="Customer" />
                <asp:BoundField DataField="Saledate" HeaderText="Saledate" ReadOnly="True" SortExpression="Saledate" />
            </Columns>
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <EditRowStyle BackColor="#999999" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <asp:AccessDataSource ID="AccessDataSource2" runat="server" DataFile="~/App_Data/Products.mdb"
            SelectCommand="SELECT * FROM [Sales] WHERE ([Partnum] = ?)">
            <SelectParameters>
                <asp:ControlParameter ControlID="FormView1" Name="Partnum" PropertyName="SelectedValue"
                    Type="String" />
            </SelectParameters>
        </asp:AccessDataSource>
    
    </div>
    </form>
</body>
</html>
