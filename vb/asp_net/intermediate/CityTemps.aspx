<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CityTemps.aspx.vb" Inherits="CityTemps" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID" DataSourceID="AccessDataSource2"
            Width="384px">
            <EditItemTemplate>
                ID:
                <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>'></asp:Label><br />
                City:
                <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>'>
                </asp:TextBox><br />
                Temp:
                <asp:TextBox ID="TempTextBox" runat="server" Text='<%# Bind("Temp") %>'>
                </asp:TextBox><br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                    Text="Update">
                </asp:LinkButton>
                <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                    Text="Cancel">
                </asp:LinkButton>
            </EditItemTemplate>
            <InsertItemTemplate>
                City:
                <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>'>
                </asp:TextBox><br />
                Temp:
                <asp:TextBox ID="TempTextBox" runat="server" Text='<%# Bind("Temp") %>'>
                </asp:TextBox><br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                    Text="Insert">
                </asp:LinkButton>
                <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                    Text="Cancel">
                </asp:LinkButton>
            </InsertItemTemplate>
            <ItemTemplate>
                <table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/sun.jpg" /></td>
                        <td style="width: 153px">
                            <asp:Label ID="CityLabel" runat="server" Text='<%# Bind("City") %>'></asp:Label><br />
                            <asp:Label ID="TempLabel" runat="server" Text='<%# Bind("Temp") %>'></asp:Label><br />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:FormView>
        <asp:AccessDataSource ID="AccessDataSource2" runat="server" DataFile="~/App_Data/CityTemps.mdb"
            SelectCommand="SELECT * FROM [CityTemps] WHERE ([ID] = ?)" ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM [CityTemps] WHERE [ID] = ? AND [City] = ? AND [Temp] = ?" InsertCommand="INSERT INTO [CityTemps] ([ID], [City], [Temp]) VALUES (?, ?, ?)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE [CityTemps] SET [City] = ?, [Temp] = ? WHERE [ID] = ? AND [City] = ? AND [Temp] = ?">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadioButtonList1" Name="ID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="original_ID" Type="Int32" />
                <asp:Parameter Name="original_City" Type="String" />
                <asp:Parameter Name="original_Temp" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="Temp" Type="Int32" />
                <asp:Parameter Name="original_ID" Type="Int32" />
                <asp:Parameter Name="original_City" Type="String" />
                <asp:Parameter Name="original_Temp" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="ID" Type="Int32" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="Temp" Type="Int32" />
            </InsertParameters>
        </asp:AccessDataSource>
        <hr />
        <br />
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" DataSourceID="AccessDataSource1"
            DataTextField="City" DataValueField="ID" RepeatColumns="3" Width="520px">
        </asp:RadioButtonList><asp:AccessDataSource ID="AccessDataSource1" runat="server"
            DataFile="~/App_Data/CityTemps.mdb" SelectCommand="SELECT * FROM [CityTemps]"></asp:AccessDataSource>
    
    </div>
    </form>
</body>
</html>
