<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmpDetail.aspx.vb" Inherits="EmpDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        &nbsp;&nbsp;<asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False"
            CellPadding="4" DataKeyNames="Id" DataSourceID="AccessDataSource1" ForeColor="#333333"
            GridLines="None" Height="50px" Width="448px">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
            <EditRowStyle BackColor="#999999" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <Fields>
                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname" />
                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname" />
                <asp:BoundField DataField="Ssn" HeaderText="Ssn" SortExpression="Ssn" />
                <asp:BoundField DataField="Add1" HeaderText="Add1" SortExpression="Add1" />
                <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
                <asp:BoundField DataField="Zip" HeaderText="Zip" SortExpression="Zip" />
                <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
            </Fields>
            <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Font-Size="Large" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <HeaderTemplate>
                Employee Details
            </HeaderTemplate>
        </asp:DetailsView>
        <asp:AccessDataSource ID="AccessDataSource1" runat="server" ConflictDetection="CompareAllValues"
            DataFile="~/App_Data/Employee.mdb" DeleteCommand="DELETE FROM [Employee] WHERE [Id] = ? AND [Firstname] = ? AND [Lastname] = ? AND [Ssn] = ? AND [Add1] = ? AND [City] = ? AND [State] = ? AND [Zip] = ? AND [Phone] = ?"
            InsertCommand="INSERT INTO [Employee] ([Id], [Firstname], [Lastname], [Ssn], [Add1], [City], [State], [Zip], [Phone]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
            OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [Id], [Firstname], [Lastname], [Ssn], [Add1], [City], [State], [Zip], [Phone] FROM [Employee] WHERE ([Id] = ?)"
            UpdateCommand="UPDATE [Employee] SET [Firstname] = ?, [Lastname] = ?, [Ssn] = ?, [Add1] = ?, [City] = ?, [State] = ?, [Zip] = ?, [Phone] = ? WHERE [Id] = ? AND [Firstname] = ? AND [Lastname] = ? AND [Ssn] = ? AND [Add1] = ? AND [City] = ? AND [State] = ? AND [Zip] = ? AND [Phone] = ?">
            <DeleteParameters>
                <asp:Parameter Name="original_Id" Type="String" />
                <asp:Parameter Name="original_Firstname" Type="String" />
                <asp:Parameter Name="original_Lastname" Type="String" />
                <asp:Parameter Name="original_Ssn" Type="String" />
                <asp:Parameter Name="original_Add1" Type="String" />
                <asp:Parameter Name="original_City" Type="String" />
                <asp:Parameter Name="original_State" Type="String" />
                <asp:Parameter Name="original_Zip" Type="String" />
                <asp:Parameter Name="original_Phone" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Firstname" Type="String" />
                <asp:Parameter Name="Lastname" Type="String" />
                <asp:Parameter Name="Ssn" Type="String" />
                <asp:Parameter Name="Add1" Type="String" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="State" Type="String" />
                <asp:Parameter Name="Zip" Type="String" />
                <asp:Parameter Name="Phone" Type="String" />
                <asp:Parameter Name="original_Id" Type="String" />
                <asp:Parameter Name="original_Firstname" Type="String" />
                <asp:Parameter Name="original_Lastname" Type="String" />
                <asp:Parameter Name="original_Ssn" Type="String" />
                <asp:Parameter Name="original_Add1" Type="String" />
                <asp:Parameter Name="original_City" Type="String" />
                <asp:Parameter Name="original_State" Type="String" />
                <asp:Parameter Name="original_Zip" Type="String" />
                <asp:Parameter Name="original_Phone" Type="String" />
            </UpdateParameters>
            <SelectParameters>
                <asp:QueryStringParameter Name="Id" QueryStringField="ID" Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="Id" Type="String" />
                <asp:Parameter Name="Firstname" Type="String" />
                <asp:Parameter Name="Lastname" Type="String" />
                <asp:Parameter Name="Ssn" Type="String" />
                <asp:Parameter Name="Add1" Type="String" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="State" Type="String" />
                <asp:Parameter Name="Zip" Type="String" />
                <asp:Parameter Name="Phone" Type="String" />
            </InsertParameters>
        </asp:AccessDataSource>
        &nbsp;&nbsp;
    
    </div>
    </form>
</body>
</html>
