<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProdDetail.aspx.vb" Inherits="ProdDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="Partnum"
            DataSourceID="AccessDataSource1" Height="50px" Width="312px" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None">
            <Fields>
                <asp:BoundField DataField="Partnum" HeaderText="Part Number" ReadOnly="True" SortExpression="Partnum" />
                <asp:BoundField DataField="Descrip" HeaderText="Description" SortExpression="Descrip" />
                <asp:BoundField DataField="Auto" HeaderText="Manufacturer" SortExpression="Auto" />
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
                <asp:BoundField DataField="InvValue" DataFormatString="{0:c}" HeaderText="Inventory Value"
                    ReadOnly="True" SortExpression="InvValue" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
            </Fields>
            <PagerSettings Mode="NumericFirstLast" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
            <EditRowStyle BackColor="#999999" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="Large" ForeColor="White" />
            <InsertRowStyle Font-Bold="True" Font-Size="Large" />
            <HeaderTemplate>
                Inventory Item
            </HeaderTemplate>
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:DetailsView>
        <asp:AccessDataSource ID="AccessDataSource1" runat="server" ConflictDetection="CompareAllValues"
            DataFile="~/App_Data/Products.mdb" DeleteCommand="DELETE FROM [Products] WHERE [Partnum] = ? AND [Descrip] = ? AND [Auto] = ? AND [Quantity] = ? AND [Price] = ?"
            InsertCommand="INSERT INTO [Products] ([Partnum], [Descrip], [Auto], [Quantity], [Price]) VALUES (?, ?, ?, ?, ?)"
            OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT Partnum, Descrip, Auto, Quantity, Price, format(Quantity * Price, 'currency') AS InvValue FROM Products"
            UpdateCommand="UPDATE [Products] SET [Descrip] = ?, [Auto] = ?, [Quantity] = ?, [Price] = ? WHERE [Partnum] = ? AND [Descrip] = ? AND [Auto] = ? AND [Quantity] = ? AND [Price] = ?">
            <DeleteParameters>
                <asp:Parameter Name="original_Partnum" Type="String" />
                <asp:Parameter Name="original_Descrip" Type="String" />
                <asp:Parameter Name="original_Auto" Type="String" />
                <asp:Parameter Name="original_Quantity" Type="Int16" />
                <asp:Parameter Name="original_Price" Type="Single" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Descrip" Type="String" />
                <asp:Parameter Name="Auto" Type="String" />
                <asp:Parameter Name="Quantity" Type="Int16" />
                <asp:Parameter Name="Price" Type="Single" />
                <asp:Parameter Name="original_Partnum" Type="String" />
                <asp:Parameter Name="original_Descrip" Type="String" />
                <asp:Parameter Name="original_Auto" Type="String" />
                <asp:Parameter Name="original_Quantity" Type="Int16" />
                <asp:Parameter Name="original_Price" Type="Single" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Partnum" Type="String" />
                <asp:Parameter Name="Descrip" Type="String" />
                <asp:Parameter Name="Auto" Type="String" />
                <asp:Parameter Name="Quantity" Type="Int16" />
                <asp:Parameter Name="Price" Type="Single" />
            </InsertParameters>
        </asp:AccessDataSource>
    
    </div>
    </form>
</body>
</html>
