<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmpForm.aspx.vb" Inherits="EmpForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FormView ID="FormView1" runat="server" AllowPaging="True" DataKeyNames="Id"
            DataSourceID="AccessDataSource1" Width="624px" CellPadding="4" style="padding-right: 10px; padding-left: 10px; border-left-color: maroon; border-bottom-color: maroon; padding-bottom: 10px; text-transform: capitalize; color: blue; border-top-style: double; border-top-color: maroon; padding-top: 10px; font-family: Arial, Centaur; border-right-style: double; border-left-style: double; border-collapse: collapse; background-color: navajowhite; border-right-color: maroon; border-bottom-style: double" ForeColor="#333333" Height="144px">
            <EditItemTemplate>
                <table style="border-top-width: thin; border-left-width: thin; border-left-color: black;
                    border-bottom-width: thin; border-bottom-color: black; border-top-color: black;
                    border-right-width: thin; border-right-color: black">
                    <tr>
                        <td style="width: 282px; height: 115px">
                            <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid">
                                <tr>
                                    <td style="width: 137px">
                                        ID:</td>
                                    <td style="width: 147px">
                                        <asp:Label ID="IdLabel1" runat="server" Text='<%# Eval("Id") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 137px">
                                        First Name:</td>
                                    <td style="width: 147px">
                                        <asp:TextBox ID="FirstnameTextBox" runat="server" Text='<%# Bind("Firstname") %>'>
                </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 137px">
                                        Last Name:</td>
                                    <td style="width: 147px">
                                        <asp:TextBox ID="LastnameTextBox" runat="server" Text='<%# Bind("Lastname") %>'>
                </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 137px">
                                        SS#:</td>
                                    <td style="width: 147px">
                                        <asp:TextBox ID="SsnTextBox" runat="server" Text='<%# Bind("Ssn") %>'>
                </asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 371px; height: 115px">
                            <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                border-bottom: black thin solid">
                                <tr>
                                    <td style="width: 100px">
                                        Address:</td>
                                    <td style="width: 257px">
                                        <asp:TextBox ID="Add1TextBox" runat="server" Text='<%# Bind("Add1") %>'>
                </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        City:</td>
                                    <td style="width: 257px">
                                        <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>'>
                </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        State:</td>
                                    <td style="width: 257px">
                                        <asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>'>
                </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        Zip:</td>
                                    <td style="width: 257px">
                                        <asp:TextBox ID="ZipTextBox" runat="server" Text='<%# Bind("Zip") %>'>
                </asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                    Text="Update">
                </asp:LinkButton>
                <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                    Text="Cancel">
                </asp:LinkButton>
            </EditItemTemplate>
            <InsertItemTemplate>
                <table style="border-top-width: thin; border-left-width: thin; border-left-color: black;
                    border-bottom-width: thin; border-bottom-color: black; border-top-color: black;
                    border-right-width: thin; border-right-color: black">
                    <tr>
                        <td style="width: 282px; height: 115px" valign="top">
                            <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid">
                                <tr>
                                    <td style="width: 137px">
                                        ID:</td>
                                    <td style="width: 147px">
                                        <asp:Label ID="IdLabel1" runat="server" Text='<%# Eval("Id") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 137px">
                                        First Name:</td>
                                    <td style="width: 147px">
                                        <asp:TextBox ID="FirstnameTextBox" runat="server" Font-Bold="True" Text='<%# Bind("Firstname") %>'></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 137px">
                                        Last Name:</td>
                                    <td style="width: 147px">
                                        <asp:TextBox ID="LastnameTextBox" runat="server" Font-Bold="True" Text='<%# Bind("Lastname") %>'></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 137px">
                                        SS#:</td>
                                    <td style="width: 147px">
                                        <asp:TextBox ID="SsnTextBox" runat="server" Font-Bold="True" Text='<%# Bind("Ssn") %>'></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 371px; height: 115px" valign="top">
                            <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                border-bottom: black thin solid">
                                <tr>
                                    <td style="width: 100px">
                                        Address:</td>
                                    <td style="width: 257px">
                                        <asp:TextBox ID="Add1TextBox" runat="server" Text='<%# Bind("Add1") %>'>
                </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        City:</td>
                                    <td style="width: 257px">
                                        <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>'>
                </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        State:</td>
                                    <td style="width: 257px">
                                        <asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>'>
                </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        Zip:</td>
                                    <td style="width: 257px">
                                        <asp:TextBox ID="ZipTextBox" runat="server" Text='<%# Bind("Zip") %>'>
                </asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                    Text="Insert">
                </asp:LinkButton>
                <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                    Text="Cancel">
                </asp:LinkButton>
            </InsertItemTemplate>
            <ItemTemplate>
                &nbsp;<br />
                <table style="border-top-width: thin; border-left-width: thin; border-left-color: black;
                    border-bottom-width: thin; border-bottom-color: black; border-top-color: black;
                    border-right-width: thin; border-right-color: black">
                    <tr>
                        <td style="width: 225px; height: 115px">
                            <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid">
                    <tr>
                        <td style="width: 137px">
                            ID:</td>
                        <td style="width: 102px">
                <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 137px">
                            First Name:</td>
                        <td style="width: 102px">
                <asp:Label ID="FirstnameLabel" runat="server" Text='<%# Bind("Firstname") %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 137px">
                            Last Name:</td>
                        <td style="width: 102px">
                <asp:Label ID="LastnameLabel" runat="server" Text='<%# Bind("Lastname") %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 137px">
                            SS#:</td>
                        <td style="width: 102px">
                <asp:Label ID="SsnLabel" runat="server" Text='<%# Bind("Ssn") %>'></asp:Label></td>
                    </tr>
                            </table>
                        </td>
                        <td style="width: 371px; height: 115px">
                            <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                border-bottom: black thin solid">
                                <tr>
                                    <td style="width: 100px">
                                        Address:</td>
                                    <td style="width: 257px">
                            <asp:Label ID="Add1Label" runat="server" Text='<%# Bind("Add1") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        City:</td>
                                    <td style="width: 257px">
                            <asp:Label ID="CityLabel" runat="server" Text='<%# Bind("City") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        State:</td>
                                    <td style="width: 257px">
                            <asp:Label ID="StateLabel" runat="server" Text='<%# Bind("State") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        Zip:</td>
                                    <td style="width: 257px">
                            <asp:Label ID="ZipLabel" runat="server" Text='<%# Bind("Zip") %>'></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                Employee Absences:<br />
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id,Date"
                    DataSourceID="AccessDataSource1">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                        <asp:BoundField DataField="Date" HeaderText="Date" ReadOnly="True" SortExpression="Date" />
                        <asp:BoundField DataField="Hours" HeaderText="Hours" SortExpression="Hours" />
                        <asp:BoundField DataField="Excused" HeaderText="Excused" SortExpression="Excused" />
                    </Columns>
                    <EmptyDataTemplate>
                        This employee has no absences
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/Employee.mdb"
                    SelectCommand="SELECT * FROM [Absent] WHERE ([Id] = ?)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="FormView1" Name="Id" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:AccessDataSource>
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit"
                    Text="Edit"></asp:LinkButton>
                <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete"
                    Text="Delete"></asp:LinkButton>
                <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New"
                    Text="New"></asp:LinkButton>
            </ItemTemplate>
            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
            <EditRowStyle BackColor="#999999" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="#284775" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderTemplate>
                <div style="font-size: large; width: 192px; height: 32px">
                    Employee Absences</div>
            </HeaderTemplate>
        </asp:FormView>
        <asp:AccessDataSource ID="AccessDataSource1" runat="server" ConflictDetection="CompareAllValues"
            DataFile="~/App_Data/Employee.mdb" DeleteCommand="DELETE FROM [Employee] WHERE [Id] = ? AND [Firstname] = ? AND [Lastname] = ? AND [Ssn] = ? AND [Add1] = ? AND [City] = ? AND [State] = ? AND [Zip] = ?"
            InsertCommand="INSERT INTO [Employee] ([Id], [Firstname], [Lastname], [Ssn], [Add1], [City], [State], [Zip]) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
            OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [Id], [Firstname], [Lastname], [Ssn], [Add1], [City], [State], [Zip] FROM [Employee]"
            UpdateCommand="UPDATE [Employee] SET [Firstname] = ?, [Lastname] = ?, [Ssn] = ?, [Add1] = ?, [City] = ?, [State] = ?, [Zip] = ? WHERE [Id] = ? AND [Firstname] = ? AND [Lastname] = ? AND [Ssn] = ? AND [Add1] = ? AND [City] = ? AND [State] = ? AND [Zip] = ?">
            <DeleteParameters>
                <asp:Parameter Name="original_Id" Type="String" />
                <asp:Parameter Name="original_Firstname" Type="String" />
                <asp:Parameter Name="original_Lastname" Type="String" />
                <asp:Parameter Name="original_Ssn" Type="String" />
                <asp:Parameter Name="original_Add1" Type="String" />
                <asp:Parameter Name="original_City" Type="String" />
                <asp:Parameter Name="original_State" Type="String" />
                <asp:Parameter Name="original_Zip" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Firstname" Type="String" />
                <asp:Parameter Name="Lastname" Type="String" />
                <asp:Parameter Name="Ssn" Type="String" />
                <asp:Parameter Name="Add1" Type="String" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="State" Type="String" />
                <asp:Parameter Name="Zip" Type="String" />
                <asp:Parameter Name="original_Id" Type="String" />
                <asp:Parameter Name="original_Firstname" Type="String" />
                <asp:Parameter Name="original_Lastname" Type="String" />
                <asp:Parameter Name="original_Ssn" Type="String" />
                <asp:Parameter Name="original_Add1" Type="String" />
                <asp:Parameter Name="original_City" Type="String" />
                <asp:Parameter Name="original_State" Type="String" />
                <asp:Parameter Name="original_Zip" Type="String" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Id" Type="String" />
                <asp:Parameter Name="Firstname" Type="String" />
                <asp:Parameter Name="Lastname" Type="String" />
                <asp:Parameter Name="Ssn" Type="String" />
                <asp:Parameter Name="Add1" Type="String" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="State" Type="String" />
                <asp:Parameter Name="Zip" Type="String" />
            </InsertParameters>
        </asp:AccessDataSource>
    
    </div>
    </form>
</body>
</html>
