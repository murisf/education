<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmpGrid.aspx.vb" Inherits="EmpGrid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" BorderColor="Blue" BorderWidth="5px" CellPadding="5"
            DataKeyNames="Id" DataSourceID="AccessDataSource1">
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="EmpDetail.aspx?ID={0}"
                    DataTextField="Id" HeaderText="ID" />
                <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname" />
                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname" />
                <asp:BoundField DataField="Ssn" HeaderText="Ssn" SortExpression="Ssn" />
            </Columns>
            <HeaderStyle BackColor="PapayaWhip" Font-Bold="True" />
            <AlternatingRowStyle BackColor="LightGray" />
            <PagerSettings Mode="NextPreviousFirstLast" />
        </asp:GridView>
        <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/Employee.mdb"
            SelectCommand="SELECT [Id], [Firstname], [Lastname], [Ssn] FROM [Employee]"></asp:AccessDataSource>
    
    </div>
    </form>
</body>
</html>
