<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProdGrid.aspx.vb" Inherits="ProdGrid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        &nbsp;<asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            DataKeyNames="Partnum" DataSourceID="AccessDataSource1" CellPadding="4" ForeColor="#333333" GridLines="None">
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="partnum" DataNavigateUrlFormatString="ProdDetail.aspx?partnum={0}"
                    DataTextField="partnum" HeaderText="Part Number" />
                <asp:BoundField DataField="Descrip" HeaderText="Descrip" SortExpression="Descrip" />
                <asp:BoundField DataField="Auto" HeaderText="Auto" SortExpression="Auto" />
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <EditRowStyle BackColor="#999999" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <asp:AccessDataSource ID="AccessDataSource1" runat="server" ConflictDetection="CompareAllValues"
            DataFile="~/App_Data/Products.mdb" DeleteCommand="DELETE FROM [Products] WHERE [Partnum] = ? AND [Descrip] = ? AND [Auto] = ? AND [Quantity] = ? AND [Price] = ?"
            InsertCommand="INSERT INTO [Products] ([Partnum], [Descrip], [Auto], [Quantity], [Price]) VALUES (?, ?, ?, ?, ?)"
            OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Products]"
            UpdateCommand="UPDATE [Products] SET [Descrip] = ?, [Auto] = ?, [Quantity] = ?, [Price] = ? WHERE [Partnum] = ? AND [Descrip] = ? AND [Auto] = ? AND [Quantity] = ? AND [Price] = ?">
            <DeleteParameters>
                <asp:Parameter Name="original_Partnum" Type="String" />
                <asp:Parameter Name="original_Descrip" Type="String" />
                <asp:Parameter Name="original_Auto" Type="String" />
                <asp:Parameter Name="original_Quantity" Type="Int16" />
                <asp:Parameter Name="original_Price" Type="Single" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Descrip" Type="String" />
                <asp:Parameter Name="Auto" Type="String" />
                <asp:Parameter Name="Quantity" Type="Int16" />
                <asp:Parameter Name="Price" Type="Single" />
                <asp:Parameter Name="original_Partnum" Type="String" />
                <asp:Parameter Name="original_Descrip" Type="String" />
                <asp:Parameter Name="original_Auto" Type="String" />
                <asp:Parameter Name="original_Quantity" Type="Int16" />
                <asp:Parameter Name="original_Price" Type="Single" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Partnum" Type="String" />
                <asp:Parameter Name="Descrip" Type="String" />
                <asp:Parameter Name="Auto" Type="String" />
                <asp:Parameter Name="Quantity" Type="Int16" />
                <asp:Parameter Name="Price" Type="Single" />
            </InsertParameters>
        </asp:AccessDataSource>
        &nbsp;&nbsp;
    </div>
    </form>
</body>
</html>
