<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmpSelect.aspx.vb" Inherits="EmpSelect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DropDownList ID="ddlEmps" runat="server" AutoPostBack="True" DataSourceID="AccessDataSource1"
            DataTextField="FullName" DataValueField="Id" Width="208px">
        </asp:DropDownList><asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/Employee.mdb"
            SelectCommand="SELECT Id, Firstname, Lastname, Ssn, Firstname & ' ' & Lastname AS FullName FROM Employee"></asp:AccessDataSource>
        <br />
        <br />
        <asp:Label ID="lblOutput" runat="server" Width="208px"></asp:Label>
        &nbsp;
        <br />
        <br />
        <asp:RadioButtonList ID="rblEmps" runat="server" AutoPostBack="True" DataSourceID="AccessDataSource2"
            DataTextField="Lastname" DataValueField="Id" RepeatColumns="3" Width="568px">
        </asp:RadioButtonList><asp:AccessDataSource ID="AccessDataSource2" runat="server"
            DataFile="~/App_Data/Employee.mdb" SelectCommand="SELECT [Id], [Firstname], [Lastname], [Ssn] FROM [Employee]">
        </asp:AccessDataSource>
        </div>
    </form>
</body>
</html>
