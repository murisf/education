<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ASPServerControls.aspx.vb" Inherits="ASPServerControls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ASP Server Controls - VB</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblASP" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtASP" runat="server"></asp:TextBox>
            <asp:Button ID="btnASP" runat="server" Text="Button" />
        </div>
    </form>
</body>
</html>
