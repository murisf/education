
Partial Class Solutions_Colors
    Inherits System.Web.UI.Page

    Protected Sub btnRed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRed.Click
        lblColor.ForeColor = System.Drawing.Color.Red
    End Sub

    Protected Sub btnGreen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGreen.Click
        lblColor.ForeColor = System.Drawing.Color.Green
    End Sub

    Protected Sub btnBlue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBlue.Click
        lblColor.ForeColor = System.Drawing.Color.Blue
    End Sub

    Protected Sub btnRandom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRandom.Click
        lblColor.ForeColor = Drawing.Color.FromArgb(Rnd() * 255, Rnd() * 255, Rnd() * 255)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Randomize()
    End Sub
End Class
