
Partial Class Solutions_SessionState
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session("pagenum") = 0
            lblField.Text = "Name:"
        Else
            Select Case Session("pagenum")
                Case 0
                    Session("result") += lblField.Text & txtField.Text & "<br>"
                    lblField.Text = "Street:"
                    txtField.Text = ""
                Case 1
                    Session("result") += lblField.Text & txtField.Text & "<br>"
                    lblField.Text = "City:"
                    txtField.Text = ""
                Case 2
                    Session("result") += lblField.Text & txtField.Text & "<br>"
                    lblField.Text = "State:"
                    txtField.Text = ""
                Case 3
                    Session("result") += lblField.Text & txtField.Text & "<br>"
                    lblField.Text = "Zip:"
                    txtField.Text = ""
                Case 4
                    Session("result") += lblField.Text & txtField.Text & "<br>"
                    txtField.Text = ""
                    lblField.Text = Session("result")
                    txtField.Visible = False
                    btnField.Text = "Restart"
                Case Else
                    Session("pagenum") = -1
                    Session("result") = ""
                    lblField.Text = "Name:"
                    txtField.Visible = True
                    btnField.Text = "Submit"
            End Select
            Session("pagenum") += 1
        End If
    End Sub
End Class
