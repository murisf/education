<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MPG.aspx.vb" Inherits="Solutions_MPG" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Miles Per Gallon</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div>
            </div>
            Miles Per Gallon Calculater<br />
            <br />
            <table>
                <tr>
                    <td style="width: 100px">
                        Start Miles:</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtStart" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        End Miles:</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtEnd" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Gallons Fuel:</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtGallons" runat="server"></asp:TextBox></td>
                </tr>
            </table>
            <br />
            <asp:Button ID="btnConvert" runat="server" Text="Calculate" /><br />
            <br />
            Miles per gallon:
            <asp:Label ID="lblMPG" runat="server" Width="256px"></asp:Label><br />
            <br />
            &nbsp;<br />
        </div>
    
    </div>
    </form>
</body>
</html>
