
Partial Class Solutions_Birthday
    Inherits System.Web.UI.Page

    Protected Sub btnConvert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConvert.Click
        lblAge.Text = _
          Math.Round(DateDiff(DateInterval.Day, CDate(txtBirthday.Text), Now) / 365.25, 2)
    End Sub
End Class
