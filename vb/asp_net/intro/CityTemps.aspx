<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CityTemps.aspx.vb" Inherits="CityTemps" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>City Temps Example - VB</title>
</head>
<body>
    <form id="form1" runat="server">
        <div title="City Temps">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/sun.jpg" />
            <asp:Label ID="lblCityTemp" runat="server" Height="56px" Text="Choose a city below" Width="264px"></asp:Label>
            <hr />

        </div>
        <table>
            <tr>
                <td style="width: 271px">Font Size:<asp:RadioButton ID="rdo12" runat="server" AutoPostBack="True" GroupName="x" Text="12" /><asp:RadioButton ID="rdo14" runat="server" AutoPostBack="True" GroupName="x" Text="14" /><asp:RadioButton ID="rdo16" runat="server" AutoPostBack="True" GroupName="x" Text="16" /></td>
                <td rowspan="3" style="width: 100px">
                    <asp:ListBox ID="lstColor" runat="server" AutoPostBack="True" Width="96px">
                        <asp:ListItem>Red</asp:ListItem>
                        <asp:ListItem>Green</asp:ListItem>
                        <asp:ListItem>Blue</asp:ListItem>
                        <asp:ListItem>Black</asp:ListItem>
                    </asp:ListBox></td>
            </tr>
            <tr>
                <td style="width: 271px">Font Style:&nbsp;<asp:CheckBox ID="chkBold" runat="server" AutoPostBack="True" Text="Bold" /><asp:CheckBox
                    ID="chkItalic" runat="server" AutoPostBack="True" Text="Italic" /></td>
            </tr>
            <tr>
                <td style="width: 271px">Font Family:&nbsp;
                    <asp:DropDownList ID="ddlFontFamily" runat="server" AutoPostBack="True" Width="176px">
                        <asp:ListItem>Arial</asp:ListItem>
                        <asp:ListItem>Courier New</asp:ListItem>
                        <asp:ListItem>Comic Sans MS</asp:ListItem>
                        <asp:ListItem>Times New Roman</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
        </table>
        <hr />
        <asp:RadioButtonList ID="rblCities" runat="server" AutoPostBack="True" RepeatColumns="3" Width="376px">
        </asp:RadioButtonList><br />
        &nbsp;<a href="AddTemp.aspx">Add City</a>&nbsp;
    </form>
</body>
</html>
