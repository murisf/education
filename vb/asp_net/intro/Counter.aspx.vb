
Partial Class Counter
    Inherits System.Web.UI.Page

    Protected Sub btnViewState_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewState.Click
        ViewState("intcounter") += 1
        btnViewState.Text = "ViewState count=" & ViewState("intcounter")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If ViewState("intcounter") Is Nothing Then
                ViewState("intcounter") = 10
            End If
            If Session("intcounter") Is Nothing Then
                Session("intcounter") = 10
            End If
        End If
    End Sub

    Protected Sub btnEndSession_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEndSession.Click
        Session.Abandon()
    End Sub

    Protected Sub btnSession_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSession.Click
        Session("intcounter") += 1
        btnSession.Text = "Session count=" & Session("intcounter")
    End Sub
End Class
