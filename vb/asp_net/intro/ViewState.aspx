<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewState.aspx.vb" Inherits="Solutions_ViewState" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewState</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblField" runat="server"></asp:Label>
            <asp:TextBox ID="txtField" runat="server"></asp:TextBox>
            <asp:Button ID="btnField" runat="server" Text="Submit" /><br />
        </div>
    </form>
</body>
</html>
