<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Counter.aspx.vb" Inherits="Counter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Counter</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="btnViewState" runat="server" Text="Increment Viewstate variable" /><br />
            <asp:Button ID="btnSession" runat="server" Text="Increment Session variable" /><br />
            <asp:Button ID="btnEndSession" runat="server" Text="End Session" /><br />
            <a href="Counter.aspx">Counter.aspx</a>
        </div>
    </form>
</body>
</html>
