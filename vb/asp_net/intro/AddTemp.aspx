<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddTemp.aspx.vb" Inherits="AddTemp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Temp Example - VB</title>
</head>
<body>
    <form id="form1" runat="server">
        <div title="Add City">
            <table>
                <tr>
                    <td style="width: 100px">City Name:&nbsp;</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td>
                    <td style="width: 253px">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCity"
                            ErrorMessage="City Name Required"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td style="width: 100px">Temperature: &nbsp;&nbsp;</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtTemp" runat="server"></asp:TextBox></td>
                    <td style="width: 253px">
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtTemp"
                            ErrorMessage="Temp must be between -20 and 120" MaximumValue="120" MinimumValue="-20"
                            Type="Integer"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTemp"
                            ErrorMessage="Temp required"></asp:RequiredFieldValidator></td>
                </tr>
            </table>
            &nbsp;<br />
            <asp:Button ID="Button1" runat="server" Text="Add City" Width="136px" />
            <br />
            <a href="CityTemps.aspx"></a>
            <br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="312px" />
            <br />
            <a href="CityTemps.aspx"></a>
        </div>
    </form>
</body>
</html>
