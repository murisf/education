<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesTax.aspx.vb" Inherits="Solutions_SalesTax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
        </div>
        Sales Tax Calculater<br />
        <br />
        <table>
            <tr>
                <td style="width: 100px">
                    Sale Amount:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtSale" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Tax Rate:</td>
                <td style="width: 100px">
                    <asp:Label ID="lblRate" runat="server" Text="7.5" Width="24px"></asp:Label>%</td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnConvert" runat="server" Text="Calculate" /><br />
        <br />
        <table>
            <tr>
                <td style="width: 100px">
                    Tax:</td>
                <td style="width: 100px">
                    <asp:Label ID="lblTax" runat="server" Width="256px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Total Sale:</td>
                <td style="width: 100px">
                    <asp:Label ID="lblTotal" runat="server" Width="256px"></asp:Label></td>
            </tr>
        </table>
        <br />
        &nbsp;<br />
    
    </div>
    </form>
</body>
</html>
