<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BirthdayEx5.aspx.vb" Inherits="Solutions_BirthdayEx5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div>
                <div>
                    <div>
                    </div>
                    Age Calculator<br />
                    <br />
                    <table>
                        <tr>
                            <td style="width: 100px">
                                Birthday:</td>
                            <td style="width: 100px">
                                <asp:TextBox ID="txtBirthday" runat="server"></asp:TextBox>
                            </td>
                            <td style="width: 100px">
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtBirthday"
                                    ErrorMessage="Birthday must be after 1/1/1900" Type="Date" Operator="GreaterThan" ValueToCompare="1/1/1900" Width="264px"></asp:CompareValidator></td>
                        </tr>
                    </table>
                    <br />
                    <asp:Button ID="btnConvert" runat="server" Text="Calculate" /><br />
                    <br />
                    <table>
                        <tr>
                            <td style="width: 100px">
                                Age:</td>
                            <td style="width: 67px">
                                &nbsp;<asp:Label ID="lblAge" runat="server" Width="256px"></asp:Label></td>
                        </tr>
                    </table>
                    <br />
                    &nbsp;<br />
                </div>
            </div>
        </div>
    
    </div>
    </form>
</body>
</html>
