<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GuestBookPswd.aspx.vb" Inherits="Solutions_GuestBookPswd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
        </div>
        <table>
            <tr>
                <td style="width: 100px">
                    Name:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                <td style="width: 100px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                        ErrorMessage="RequiredFieldValidator" Width="152px">Name is required</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Email:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                <td style="width: 100px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="RequiredFieldValidator" Width="168px">Email address is required</asp:RequiredFieldValidator></td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnSign" runat="server" Text="Sign the Guestbook" /><br />
        <br />
        <asp:Label ID="lblThanks" runat="server" Text="Thank You" Visible="False"></asp:Label></div>
    </form>
</body>
</html>
