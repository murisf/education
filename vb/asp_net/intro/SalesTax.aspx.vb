
Partial Class Solutions_SalesTax
    Inherits System.Web.UI.Page

    Protected Sub btnConvert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConvert.Click
        Dim tax As Decimal = lblRate.Text * txtSale.Text / 100
        lblTax.Text = Format(tax, "Currency")
        lblTotal.Text = Format(tax + txtSale.Text, "Currency")
    End Sub
End Class
