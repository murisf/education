
Partial Class Solutions_ViewState
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ViewState("pagenum") = 0
            lblField.Text = "Name:"
        Else
            Select Case ViewState("pagenum")
                Case 0
                    ViewState("result") += lblField.Text & txtField.Text & "<br>"
                    lblField.Text = "Street:"
                    txtField.Text = ""
                Case 1
                    ViewState("result") += lblField.Text & txtField.Text & "<br>"
                    lblField.Text = "City:"
                    txtField.Text = ""
                Case 2
                    ViewState("result") += lblField.Text & txtField.Text & "<br>"
                    lblField.Text = "State:"
                    txtField.Text = ""
                Case 3
                    ViewState("result") += lblField.Text & txtField.Text & "<br>"
                    lblField.Text = "Zip:"
                    txtField.Text = ""
                Case 4
                    ViewState("result") += lblField.Text & txtField.Text & "<br>"
                    txtField.Text = ""
                    lblField.Text = ViewState("result")
                    txtField.Visible = False
                    btnField.Text = "Restart"
                Case Else
                    ViewState("pagenum") = -1
                    ViewState("result") = ""
                    lblField.Text = "Name:"
                    txtField.Visible = True
                    btnField.Text = "Submit"
            End Select
            ViewState("pagenum") += 1
        End If

    End Sub
End Class
