<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TempConv.aspx.vb" Inherits="Solutions_TempConv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        Temperature Converter<br />
        <br />
        <table>
            <tr>
                <td style="width: 100px">
                    Fahrenheit:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtFahr" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Celcius:</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtCels" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnConvert" runat="server" Text="Convert" />
    </form>
</body>
</html>
