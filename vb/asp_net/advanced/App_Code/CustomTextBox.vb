Imports Microsoft.VisualBasic
Namespace Samples.AspNet.VB.Controls

    Public Class CustomTextBox
        Inherits TextBox

        Public Sub Style1()
            Me.Font.Size = New FontUnit(10)
            Me.ForeColor = Drawing.Color.Red
            Me.Font.Bold = True
        End Sub

        Public Sub Style2()
            Me.Font.Size = New FontUnit(14)
            Me.ForeColor = Drawing.Color.Blue
            Me.Font.Bold = False
        End Sub
    End Class
End Namespace
