Imports Microsoft.VisualBasic
Namespace Samples.AspNet.VB.Controls
    Public Class Rectangle
        Inherits WebControl
        Private recWidthValue, recLengthValue As Single
        Public Property RecWidth() As Single
            Get
                Return recWidthValue
            End Get
            Set(ByVal value As Single)
                recWidthValue = value
            End Set
        End Property

        Public Property RecLength() As Single
            Get
                Return recLengthValue
            End Get
            Set(ByVal value As Single)
                recLengthValue = value
            End Set
        End Property

        Public ReadOnly Property Area() As Single
            Get
                If recLength <> Nothing And recWidth <> Nothing Then
                    Return recWidthValue * recLengthValue
                Else
                    Return -1
                End If
            End Get
        End Property

        Public Overridable Property Text() As String
            Get
                Dim s As String = CStr(ViewState("Text"))
                If s Is Nothing Then s = String.Empty
                Return s
            End Get
            Set(ByVal value As String)
                ViewState("Text") = value
            End Set
        End Property

        Protected Overrides Sub RenderContents(ByVal writer As HtmlTextWriter)
            If Area <> -1 Then
                writer.WriteEncodedText("The area is " & Area)
            Else
                writer.WriteEncodedText(Text & " - the area is unknown")
            End If
        End Sub
    End Class
End Namespace
