Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class WeatherService
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function GetTemp(ByVal CityName As String) As Integer
        Dim accessDS As New AccessDataSource("~/App_Data/citytemps.mdb", "select * from citytemps")
        accessDS.SelectCommand = "select temp from citytemps where city = '" & CityName & "'"
        accessDS.DataFile = "~/App_Data/citytemps.mdb"
        accessDS.DataSourceMode = SqlDataSourceMode.DataReader
        Dim rdr As System.Data.OleDb.OleDbDataReader
        rdr = accessDS.Select(New DataSourceSelectArguments)
        rdr.Read()
        Return rdr(0)
    End Function
    <WebMethod()> _
    Public Function GetCities() As ArrayList
        Dim accessDS As New AccessDataSource("~/App_Data/citytemps.mdb", "select * from citytemps")
        accessDS.SelectCommand = "select city from citytemps"
        accessDS.DataFile = "~/App_Data/citytemps.mdb"
        accessDS.DataSourceMode = SqlDataSourceMode.DataReader
        Dim rdr As System.Data.OleDb.OleDbDataReader
        rdr = accessDS.Select(New DataSourceSelectArguments)
        Dim Cities As ArrayList = New ArrayList
        Do While rdr.Read
            Cities.Add(rdr("city"))
        Loop
        Return Cities
    End Function
End Class
