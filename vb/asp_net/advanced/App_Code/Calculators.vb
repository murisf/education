Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Calculators
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function Mileage(ByVal startMiles As Single, ByVal endMiles As Single, ByVal gallons As Single)
        Return (endMiles - startMiles) / gallons
    End Function

    <WebMethod()> _
    Public Function FtoC(ByVal fahr As Single) As Single
        Return 5 / 9 * (fahr - 32)
    End Function

    <WebMethod()> _
    Public Function CtoF(ByVal cels As Single) As Single
        Return 9 / 5 * cels + 32
    End Function

    <WebMethod()> _
    Public Function WeekDay(ByVal dayDate As DateTime) As String
        Return Format(dayDate, "dddd")
    End Function


End Class
