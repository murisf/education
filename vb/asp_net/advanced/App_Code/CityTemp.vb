Imports Microsoft.VisualBasic
Namespace Samples.AspNet.VB.Controls

    Public Class CityTemp
        Inherits CompositeControl

        Dim WithEvents ddlCityList As New System.Web.UI.WebControls.DropDownList
        Dim WithEvents lblOutput As New System.Web.UI.WebControls.Label
        Dim accessDS As New AccessDataSource("~/App_Data/citytemps.mdb", "select * from citytemps")

        Protected Overrides Sub CreateChildControls()
            ddlCityList.DataSource = accessDS
            ddlCityList.DataMember = ""
            ddlCityList.DataTextField = "City"
            ddlCityList.DataValueField = "ID"
            ddlCityList.DataBind()
            Controls.Add(ddlCityList)
            ddlCityList.AutoPostBack = True
            Controls.Add(New LiteralControl("<br>"))
            Controls.Add(lblOutput)
            Controls.Add(New LiteralControl("<br>"))
            lblOutput.Text = "Choose a city"
        End Sub

        Private Sub ddlCityList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCityList.SelectedIndexChanged
            accessDS.SelectCommand = "select temp from citytemps where id = " & ddlCityList.SelectedItem.Value
            accessDS.DataFile = "~/App_Data/citytemps.mdb"
            accessDS.DataSourceMode = SqlDataSourceMode.DataReader
            Dim rdr As System.Data.OleDb.OleDbDataReader
            rdr = accessDS.Select(New DataSourceSelectArguments)
            rdr.Read()
            lblOutput.Text = rdr(0) & " F."
        End Sub
    End Class
End Namespace
