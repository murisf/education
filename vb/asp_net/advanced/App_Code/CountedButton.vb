Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace Samples.AspNet.VB.Controls

    <DefaultProperty("Text"), ToolboxData("<{0}:CountedButton runat=server></{0}:CountedButton>")> _
    Public Class CountedButton
        Inherits Button

        Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
            MyBase.OnClick(e)
            Dim obj As Object = ViewState("count")
            If obj Is Nothing Then obj = 0
            obj += 1
            Me.Text = obj & " clicks"
            ViewState("count") = obj
        End Sub

    End Class
End Namespace
