Imports Microsoft.VisualBasic
Namespace Samples.AspNet.VB.Controls

    Public Class TempConverter
        Inherits CompositeControl

        Private WithEvents origTempTextBox As New TextBox
        Private WithEvents convertedTempLabel As New Label
        Private WithEvents convertButton As New Button

        Protected Overrides Sub CreateChildControls()
            MyBase.CreateChildControls()
            Controls.Add(New LiteralControl("Please enter a Fahrenheit temperature below<br>Fahrenheit temp: "))
            Controls.Add(origTempTextBox)
            Controls.Add(New LiteralControl("<br>Converts to "))
            convertedTempLabel.Text = "?"
            Controls.Add(convertedTempLabel)
            Controls.Add(New LiteralControl(" degrees Celsius<br>"))
            convertButton.Text = "Click to convert"
            Controls.Add(convertButton)
        End Sub

        Private Sub convertButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles convertButton.Click
            convertedTempLabel.Text = 5 / 9 * (origTempTextBox.Text - 32)
        End Sub
    End Class

End Namespace
