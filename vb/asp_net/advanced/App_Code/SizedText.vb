Imports Microsoft.VisualBasic
Namespace ASPSampleControls
    Public Class SizedText
        Inherits WebControl
        Public Property Text() As String
            Get
                Return ViewState("text")
            End Get
            Set(ByVal value As String)
                ViewState("text") = value
            End Set
        End Property
        Public Property Size() As Integer
            Get
                Return ViewState("size")
            End Get
            Set(ByVal value As Integer)
                ViewState("size") = value
            End Set
        End Property
        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            writer.Write("<font size=" & Size & ">" & Text & "</font>")
        End Sub
    End Class
End Namespace
