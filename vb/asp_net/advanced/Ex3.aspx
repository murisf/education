<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Ex3.aspx.vb" Inherits="Ex3" %>
<%@ Register TagPrefix="aspSample" Namespace="Samples.AspNet.VB.Controls"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspSample:CustomTextBox ID="CustomTextBox1" Text="Hello There" runat="server" /><br />
        <asp:Button ID="btnStyle1" runat="server" Text="Style1" />
        <asp:Button ID="btnStyle2" runat="server" Text="Style2" />

    </div>
    </form>
</body>
</html>
