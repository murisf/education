
Partial Class Links
    Inherits System.Web.UI.UserControl

    Public Property Link1() As String
        Get
            Return HyperLink1.NavigateUrl
        End Get
        Set(ByVal value As String)
            HyperLink1.NavigateUrl = value
        End Set
    End Property
    Public Property Link2() As String
        Get
            Return HyperLink2.NavigateUrl
        End Get
        Set(ByVal value As String)
            HyperLink2.NavigateUrl = value
        End Set
    End Property
    Public Property Link3() As String
        Get
            Return HyperLink3.NavigateUrl
        End Get
        Set(ByVal value As String)
            HyperLink3.NavigateUrl = value
        End Set
    End Property
    Public Property Link4() As String
        Get
            Return HyperLink4.NavigateUrl
        End Get
        Set(ByVal value As String)
            HyperLink4.NavigateUrl = value
        End Set
    End Property

End Class
