<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Ex1.aspx.vb" Inherits="Ex1" %>

<%@ Register Src="LinkCounter.ascx" TagName="LinkCounter" TagPrefix="uc2" %>

<%@ Register Src="Links.ascx" TagName="Links" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        Demo of Links.ascx<br />
        <uc1:Links ID="Links1" runat="server" />
        <br />
        <br />
        Demo of LinkCounter.ascx<br />
        <uc2:LinkCounter ID="LinkCounter1" runat="server" />
    
    </div>
    </form>
</body>
</html>
