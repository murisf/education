<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CompositeDemo.aspx.vb" Inherits="CompositeDemo" %>
<%@ Register TagPrefix="aspSample" Namespace="Samples.AspNet.VB.Controls"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspSample:CityTemp ID="CityTemp1" runat="server" />
        <br />
        <aspSample:CityTemp ID="CityTemp2" runat="server" />
        <br />
        <aspSample:CityTemp ID="CityTemp3" runat="server" />
    
    </div>
    </form>
</body>
</html>
