
Partial Class LinkCounter
    Inherits System.Web.UI.UserControl

    Private link1Url, link2Url, link3Url, link4Url As String

    Public Property Link1() As String
        Get
            Return link1Url
        End Get
        Set(ByVal value As String)
            link1Url = value
        End Set
    End Property
    Public Property Link2() As String
        Get
            Return link2Url
        End Get
        Set(ByVal value As String)
            link2Url = value
        End Set
    End Property
    Public Property Link3() As String
        Get
            Return link3Url
        End Get
        Set(ByVal value As String)
            link3Url = value
        End Set
    End Property
    Public Property Link4() As String
        Get
            Return link4Url
        End Get
        Set(ByVal value As String)
            link4Url = value
        End Set
    End Property
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click, LinkButton2.Click, LinkButton3.Click, LinkButton4.Click
        Session("count") += 1
        If sender.id = "LinkButton1" Then
            Response.Redirect(link1Url)
        ElseIf sender.id = "LinkButton2" Then
            Response.Redirect(link2Url)
        ElseIf sender.id = "LinkButton3" Then
            Response.Redirect(link3Url)
        ElseIf sender.id = "LinkButton4" Then
            Response.Redirect(link4Url)
        End If
    End Sub
End Class
