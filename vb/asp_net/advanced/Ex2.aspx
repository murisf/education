<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Ex2.aspx.vb" Inherits="Ex2" %>
<%@ Register TagPrefix="aspSample" Namespace="Samples.AspNet.VB.Controls"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspSample:Rectangle ID="Rectangle1" Text="Hello There" runat="server" />
        <br />
        Length:<asp:TextBox ID="txtLength" runat="server"></asp:TextBox><br />
        Width:
        <asp:TextBox ID="txtWidth" runat="server"></asp:TextBox><br />
        <asp:Button ID="btnDisplay" runat="server" Text="Display Area" /><br />
        The area is:
        <asp:Label ID="lblOutput" runat="server"></asp:Label></div>
    </form>
</body>
</html>
