
Partial Class CityTemps
    Inherits System.Web.UI.Page

    Protected Sub rdo12_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo12.CheckedChanged
        lblCityTemp.Font.Size = New FontUnit(12)
    End Sub

    Protected Sub rdo14_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo14.CheckedChanged
        lblCityTemp.Font.Size = New FontUnit(14)
    End Sub

    Protected Sub rdo16_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo16.CheckedChanged
        lblCityTemp.Font.Size = New FontUnit(16)
    End Sub


    Protected Sub rblCities_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblCities.SelectedIndexChanged
        lblCityTemp.Text = rblCities.SelectedItem.Text & "<br>" & Temps(rblCities.SelectedItem.Value)
    End Sub

    Dim Temps() As Integer '= {78, 89, 92, 90, 78, 86, 73, 91, 80, 114, 96, 91, 91, 86, 86, 84, 85, 92, 70, 76}
    Dim Cities() As String '= {"Chicago", "St. Louis", "Kansas City", "Atlanta", "Fairbanks", "Honolulu", "San Francisco", "Denver", "Los Angeles", "Phoenix", "Dallas", "New Orleans", "Miami", "Washington", "New York", "Boston", "Great Falls", "Boise", "Minn./St. Paul", "Seattle"}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim j As Integer = 0
        Dim strRelPath As String = "data.txt"
        Dim strAbsPath As String = Server.MapPath(strRelPath)
        Try
            FileOpen(1, strAbsPath, OpenMode.Input)
            While Not EOF(1)
                ReDim Preserve Cities(j)
                ReDim Preserve Temps(j)
                Input(1, Cities(j))
                Input(1, Temps(j))
                j += 1
            End While
        Finally
            FileClose(1)
        End Try

        If Not IsPostBack Then
            Dim i As Integer
            For i = 0 To Cities.Length - 1
                Dim nitem As New ListItem(Cities(i), i)
                rblCities.Items.Add(nitem)
            Next
        End If

    End Sub

    Protected Sub chkBold_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkBold.CheckedChanged
        If lblCityTemp.Font.Bold Then
            lblCityTemp.Font.Bold = False
        Else
            lblCityTemp.Font.Bold = True
        End If
    End Sub

    Protected Sub chkItalic_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkItalic.CheckedChanged
        If lblCityTemp.Font.Italic Then
            lblCityTemp.Font.Italic = False
        Else
            lblCityTemp.Font.Italic = True
        End If
    End Sub

    Protected Sub ddlFontFamily_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFontFamily.SelectedIndexChanged
        lblCityTemp.Font.Name = ddlFontFamily.SelectedItem.Text
    End Sub

    Protected Sub lstColor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstColor.SelectedIndexChanged
        lblCityTemp.ForeColor = Drawing.Color.FromName(lstColor.SelectedItem.Text)
    End Sub
End Class
