<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DerivedDemo.aspx.vb" Inherits="DerivedDemo" %>
<%@ Register TagPrefix="aspSample" Namespace="Samples.AspNet.VB.Controls"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <aspSample:CountedButton ID="CountedButton1" runat="server" Text="Click Me" />
    </div>
    </form>
</body>
</html>
