
Partial Class TempLookup
    Inherits System.Web.UI.Page

    Dim proxy As New localhost.WeatherService
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ddlCities.DataSource = proxy.GetCities
            DataBind()
        End If
    End Sub

    Protected Sub ddlCities_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCities.SelectedIndexChanged
        lblOutput.Text = proxy.GetTemp(ddlCities.SelectedItem.Text) & " degrees F."
    End Sub
End Class
