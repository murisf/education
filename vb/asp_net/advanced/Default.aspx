<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="5">
            <tr>
                <td style="width: 100px">
                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="AccessDataSource1"
                        DataTextField="City" DataValueField="ID" Width="152px">
                    </asp:DropDownList><asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/CityTemps.mdb"
                        SelectCommand="SELECT * FROM [CityTemps]"></asp:AccessDataSource>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID" DataSourceID="AccessDataSource2">
                        <EditItemTemplate>
                            ID:
                            <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>'></asp:Label><br />
                            City:
                            <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>'>
                            </asp:TextBox><br />
                            Temp:
                            <asp:TextBox ID="TempTextBox" runat="server" Text='<%# Bind("Temp") %>'>
                            </asp:TextBox><br />
                            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                                Text="Update">
                            </asp:LinkButton>
                            <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                Text="Cancel">
                            </asp:LinkButton>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            City:
                            <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>'>
                            </asp:TextBox><br />
                            Temp:
                            <asp:TextBox ID="TempTextBox" runat="server" Text='<%# Bind("Temp") %>'>
                            </asp:TextBox><br />
                            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                                Text="Insert">
                            </asp:LinkButton>
                            <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                Text="Cancel">
                            </asp:LinkButton>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="TempLabel" runat="server" Text='<%# Bind("Temp") %>'></asp:Label>
                            degress F.
                        </ItemTemplate>
                    </asp:FormView>
                    <asp:AccessDataSource ID="AccessDataSource2" runat="server" DataFile="~/App_Data/CityTemps.mdb"
                        SelectCommand="SELECT * FROM [CityTemps] WHERE ([ID] = ?)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownList1" Name="ID" PropertyName="SelectedValue"
                                Type="Int32" />
                        </SelectParameters>
                    </asp:AccessDataSource>
                </td>
            </tr>
        </table>
        <br />
        <hr />
    
    </div>
        Copyright 2006
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="mailto:biff@umsl.edu">Biff Arfuss</asp:HyperLink>
    </form>
</body>
</html>
