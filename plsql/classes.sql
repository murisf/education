create table classes
(workshop varchar2(40),
ws_date date,
empid varchar2(5),
room varchar2(3) default 'AUD',
limit number(3) default 25,
priv_grp varchar2(30),
price number(5,2));
insert into classes values
('SUCCEEDING AT PONG','13-AUG-08','B2611','24',default,'MO GAMING SOCIETY',35);
insert into classes values
('FASHION TRENDS OF THE 1970s','16-OCT-08','B3544',default,default,null,27.5);
insert into classes values
('FASHION TRENDS OF THE 1980s','27-FEB-09','B2611',default,default,null,27.5);
insert into classes values
('SINGING WITH THE MONKEES','24-APR-10','B2559','23B',10,'MONKEES FAN CLUB',42.5);
insert into classes values
('ELVIS IMPERSONATIONS','21-JUN-10','B2559','23B',15,'MEMPHIS MAFIA',33.5);
insert into classes values
('1950s TV SHOWS','10-AUG-09','B1158',default,default,null,25);
insert into classes values
('TIE DYE MADE SIMPLE','27-MAR-09','B1263','23A',20,'UMSL CLASS OF 1969',52.5);
insert into classes values
('HAIRSTYLES OF THE 1950s','07-MAR-09','B1158','23A',15,'MO BEAUTICIANS GUILD',46.5);
insert into classes values
('SOLVING THE RUBIKS CUBE','18-OCT-08','B2611','24',default,'MO GAMING SOCIETY',35);
insert into classes values
('FASHION TRENDS OF THE 1960s','08-SEP-08','B1263',default,default,null,27.5);
insert into classes values
('MASTERING THE HULA HOOP','03-FEB-10','B2559',default,10,null,48.5);