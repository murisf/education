CREATE OR REPLACE PROCEDURE emp_raise
IS
BEGIN
   UPDATE employee
   SET    hourrate = hourrate + (hourrate * .04);
   COMMIT;
END emp_raise;
