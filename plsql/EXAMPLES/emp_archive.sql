CREATE OR REPLACE PROCEDURE emp_archive
   (i_empid IN employee.empid%TYPE)
IS
   emp_rec employee%ROWTYPE;
BEGIN
-- Retrieve employee info based on ID passed in procedure call
   SELECT *
   INTO   emp_rec
   FROM   employee
   WHERE  empid = i_empid;
-- Insert this data in OLD_EMPS table
   INSERT INTO OLD_EMPS (empid, firstname, midname, lastname, 
                         jobcode, hiredate, hourrate)
   VALUES (emp_rec.empid, emp_rec.firstname, emp_rec.midname, 
           emp_rec.lastname, emp_rec.jobcode, emp_rec.hiredate, 
           emp_rec.hourrate);
-- Delete record from EMPLOYEE table
   DELETE FROM employee
   WHERE empid = i_empid;
END emp_archive;
