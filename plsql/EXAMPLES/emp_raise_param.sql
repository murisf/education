CREATE OR REPLACE PROCEDURE emp_raise
   (i_raise_percent IN NUMBER := .03, 
    io_empid IN OUT employee.empid%TYPE,
    o_newrate OUT employee.hourrate%TYPE)   
IS
BEGIN
   UPDATE employee
   SET    hourrate = hourrate + (hourrate * i_raise_percent)
   WHERE  empid = io_empid
   RETURNING hourrate INTO o_newrate; 
   io_empid := SUBSTR(io_empid, 2, 4); 
-- COMMIT;
END emp_raise;
