DECLARE 

   TYPE lastname_tabtype IS TABLE OF
      employee.lastname%TYPE
      INDEX BY BINARY_INTEGER;

   lastname_tab lastname_tabtype;

   CURSOR lastname_cur IS
      SELECT   lastname
      FROM     employee
      ORDER BY hourrate DESC; 

   v_counter NUMBER(2) := 0; 
   
BEGIN
   
   FOR lastname_rec IN lastname_cur
   LOOP
      v_counter := v_counter + 1; 
      lastname_tab(v_counter) := lastname_rec.lastname;
   END LOOP;

   FOR i IN 1 .. v_counter
   LOOP
      DBMS_OUTPUT.PUT_LINE(lastname_tab(i));
   END LOOP;

END;
