CREATE OR REPLACE PROCEDURE archive_class2

IS 

   TYPE old_class_rectype IS RECORD
      (workshop classes.workshop%TYPE,
       ws_date classes.ws_date%TYPE);
  
   old_class_rec old_class_rectype;    

   TYPE old_class_tabtype IS TABLE OF
      old_class_rectype
      INDEX BY BINARY_INTEGER;

   old_class_tab old_class_tabtype; 

   CURSOR old_class_cur IS
      SELECT workshop, ws_date
      FROM   classes
      WHERE  MONTHS_BETWEEN(SYSDATE, ws_date) >= 7;

   v_counter NUMBER(2) := 0;
 
BEGIN 

   FOR old_class_rec IN old_class_cur
   LOOP
      v_counter := v_counter + 1;
      old_class_tab(v_counter).workshop := old_class_rec.workshop;
      old_class_tab(v_counter).ws_date := old_class_rec.ws_date;
   END LOOP;

   FOR i IN 1 .. v_counter
   LOOP
      INSERT INTO old_class2 (workshop, ws_date)
      VALUES (old_class_tab(i).workshop, old_class_tab(i).ws_date);
      DBMS_OUTPUT.PUT_LINE('Inserted record for ' || old_class_tab(i).workshop);
      DELETE FROM classes 
      WHERE workshop = old_class_tab(i).workshop;
      DBMS_OUTPUT.PUT_LINE('Deleted record for ' || old_class_tab(i).workshop);
   END LOOP; 

END archive_class2;    
