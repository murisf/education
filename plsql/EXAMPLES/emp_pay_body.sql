CREATE OR REPLACE PACKAGE BODY emp_pay_pack
IS

   FUNCTION get_hourrate
      (i_empid IN employee.empid%TYPE)
      RETURN employee.hourrate%TYPE
   IS 
      v_rate employee.hourrate%TYPE;
   BEGIN
      SELECT hourrate
      INTO v_rate
      FROM employee
      WHERE empid = i_empid;
      RETURN (v_rate); 
   END get_hourrate; 

   PROCEDURE calc_wages
      (i_empid IN employee.empid%TYPE,
       i_hours IN week_pay.num_hours%TYPE,
       o_reg_wages OUT week_pay.reg_wages%TYPE,
       o_overtime OUT week_pay.overtime%TYPE)
   IS
      v_hourrate employee.hourrate%TYPE;
   BEGIN
      v_hourrate := get_hourrate(i_empid);
      IF i_hours <= 40
      THEN
         o_reg_wages := i_hours * v_hourrate;
         o_overtime := 0;
      ELSE
         o_reg_wages := 40 * v_hourrate;
         o_overtime := (i_hours - 40) * (1.5 * v_hourrate);
      END IF;
   END calc_wages;

   PROCEDURE payroll
      (i_empid IN employee.empid%TYPE,
       i_hours_worked IN week_pay.num_hours%TYPE := 40)
   IS
      v_reg_wages week_pay.reg_wages%TYPE;
      v_overtime  week_pay.overtime%TYPE;
      v_tax       week_pay.tax%TYPE;
      v_take_home week_pay.take_home%TYPE;
   BEGIN
      calc_wages(i_empid, i_hours_worked, v_reg_wages, v_overtime); 
      v_tax := (v_reg_wages + v_overtime) * c_tax_rate; 
      v_take_home := v_reg_wages + v_overtime - v_tax;
      INSERT INTO week_pay (empid, num_hours, reg_wages, overtime, 
             tax, take_home)
      VALUES (i_empid, i_hours_worked, v_reg_wages, v_overtime, 
             v_tax, v_take_home); 
   -- COMMIT;
   END payroll;

END emp_pay_pack;
