CREATE OR REPLACE TRIGGER log_rate_change
   AFTER UPDATE OF hourrate ON EMPLOYEE
   FOR EACH ROW
BEGIN
   INSERT INTO emp_change (empid, old_hourrate, new_hourrate)
   VALUES (:NEW.empid, :OLD.hourrate, :NEW.hourrate);
END log_rate_change;
