CREATE OR REPLACE FUNCTION temp_convert
   (i_fahrenheit IN NUMBER)
   RETURN NUMBER
IS
   v_celsius NUMBER; 
BEGIN      
   v_celsius := 5 / 9 * (i_fahrenheit - 32);
   RETURN v_celsius;
END temp_convert;
