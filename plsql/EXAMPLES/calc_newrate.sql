CREATE OR REPLACE FUNCTION calc_newrate
   (i_perc_change IN NUMBER,
    i_curr_rate   IN NUMBER)
   RETURN NUMBER
IS
   v_newrate employee.hourrate%TYPE;
BEGIN
   v_newrate := i_curr_rate + (i_curr_rate * i_perc_change);
   RETURN v_newrate;
END calc_newrate;
