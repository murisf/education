DECLARE
   v_raise NUMBER(5,3);
   v_empid employee.empid%TYPE;
   v_newrate employee.hourrate%TYPE;
BEGIN
   v_raise := &p_raise;
   v_empid := UPPER('&p_empid');
   emp_raise(v_raise, v_empid, v_newrate);
   DBMS_OUTPUT.PUT_LINE('Four-digit employee code: ' || v_empid);
   DBMS_OUTPUT.PUT_LINE('New hourly rate: $' || v_newrate);
END;
