CREATE OR REPLACE PROCEDURE eval_raise
IS

   v_raise_percent NUMBER;

-- define cursor to process evaluations for current year
   CURSOR rating_cur
   IS
      SELECT empid, rating 
      FROM eval
      WHERE TO_CHAR(eval_date, 'YYYY') = TO_CHAR(SYSDATE, 'YYYY');

-- define function to return percent raise based on eval rating
   FUNCTION get_raise_percent
      (i_rating IN eval.rating%TYPE)
      RETURN NUMBER
   IS 
      v_raise NUMBER;
   BEGIN
      v_raise :=
         CASE i_rating
            WHEN 1 THEN .02
            WHEN 2 THEN .03
            WHEN 3 THEN .04
         END;
      RETURN v_raise; 
   END get_raise_percent; 

-- updates hourly rates in EMPLOYEE table (similar to EMP_RAISE) 
   PROCEDURE update_hourrate
      (i_raise_percent IN NUMBER, 
       i_empid IN employee.empid%TYPE)   
   IS
   BEGIN
      UPDATE employee
      SET    hourrate = cal_newrate(i_raise_percent, hourrate)
      WHERE  empid = i_empid;
   -- COMMIT;
   END update_hourrate; 

BEGIN
-- body of main procedure
-- use cursor FOR loop to process raise for each employee
   FOR rating_rec IN rating_cur 
   LOOP

   -- calls local function to determine % raise
      v_raise_percent := get_raise_percent(rating_rec.rating);

   -- DBMS_OUTPUT.PUT_LINE can be helpful in testing 
      DBMS_OUTPUT.PUT_LINE(rating_rec.empid ||' '|| 
                           rating_rec.rating ||' '|| 
                           v_raise_percent); 

   -- call local procedure to update EMPLOYEE table
      update_hourrate(v_raise_percent, rating_rec.empid);

   END LOOP; 
   
END eval_raise;

SET serveroutput ON;
BEGIN
  eval_raise;
END;
