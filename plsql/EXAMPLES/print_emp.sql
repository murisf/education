CREATE OR REPLACE PROCEDURE print_emp
   (i_empid IN employee.empid%TYPE)
IS
   emp_rec employee%ROWTYPE;
BEGIN
-- Retrieve employee info based on ID passed in procedure call
   SELECT *
   INTO   emp_rec
   FROM   employee
   WHERE  empid = i_empid;
-- Display employee information
   DBMS_OUTPUT.PUT_LINE('Employee name: ' || emp_rec.firstname ||
                         ' ' || emp_rec.lastname);
   DBMS_OUTPUT.PUT_LINE('Job code: ' || emp_rec.jobcode);
   DBMS_OUTPUT.PUT_LINE('Hire date: ' || 
                         TO_CHAR(emp_rec.hiredate, 'MM/DD/YYYY'));
END print_emp;

SET serveroutput ON;
BEGIN
  print_emp('A1465');
END;