CREATE OR REPLACE TRIGGER log_rate_change
   AFTER INSERT OR DELETE OR UPDATE OF hourrate ON EMPLOYEE
   FOR EACH ROW
BEGIN
   IF INSERTING THEN
      INSERT INTO emp_change (empid, new_hourrate, operation)
      VALUES (:NEW.empid, :NEW.hourrate, 'INSERT'); 
   ELSIF DELETING THEN
      INSERT INTO emp_change (empid, old_hourrate, operation)
      VALUES (:OLD.empid, :OLD.hourrate, 'DELETE');
   ELSIF UPDATING THEN
      INSERT INTO emp_change (empid, old_hourrate, new_hourrate, 
                              operation)
      VALUES (:NEW.empid, :OLD.hourrate, :NEW.hourrate, 'UPDATE'); 
   END IF;
END log_rate_change;
