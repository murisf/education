CREATE OR REPLACE TRIGGER change_class_view
   INSTEAD OF UPDATE ON emp_classes
   FOR EACH ROW
DECLARE
   v_empid employee.empid%TYPE;
BEGIN
   IF UPDATING ('price')
   THEN
      UPDATE classes 
      SET    price = :NEW.price
      WHERE  workshop = :OLD.workshop; 
   ELSIF UPDATING ('limit')
   THEN
      UPDATE classes
      SET    limit = :NEW.limit
      WHERE  workshop = :OLD.workshop; 
   ELSIF UPDATING ('lastname')
   THEN 
   -- find ID associated with new name
      SELECT empid
      INTO   v_empid
      FROM   employee
      WHERE  lastname = :NEW.lastname;
   -- update classes table with new ID
      UPDATE classes
      SET    empid = v_empid
      WHERE  workshop = :OLD.workshop;
   END IF;
END change_price_view;

 