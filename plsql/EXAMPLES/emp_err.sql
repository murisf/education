CREATE PROCEDURE emp_err
IS
BEGIN
   UPDATE employees
   SET    hourrate = hourrate + (hourrate * .04);
END emp_err;