create table members
(mem_id varchar2(5),
firstname varchar2(15),
lastname varchar2(15),
memcode varchar2(2) default 'AD',
memdate date);
insert into members values
('AC135','ANNE', 'COOPER', 'ST', '05-JUL-99');
insert into members values
('RP090','RICHARD','POWERS','FM','10-AUG-98');
insert into members values
('AA910','AARON','ATWOOD','AD','27-MAR-01');
insert into members values
('CM818','CHRIS','MARKS','ST','17-DEC-00');
insert into members values
('PS107','PENELOPE','STARR','CH','06-APR-98');
insert into members values
('GR009','GRANT','REDDY','SR','11-OCT-01');
insert into members values
('AH119','ALICIA','HERNANDEZ','SR','22-FEB-02');
insert into members values
('GG292','GREG','GREEN','AD','01-JAN-00');
insert into members values
('MB760','MARIE','BURNS','CH','30-APR-01');
insert into members values
('LS504','LOU','SMALLS','ST','14-DEC-97');
insert into members values
('JJ111','JENNY','JEFFRIES','ST','14-DEC-97');
insert into members values
('TC769','TODD','CHANG','FM','15-MAY-99');
insert into members values
('MT907','MIKE','THOMAS','AD','09-MAR-02');
insert into members values
('ST546','STACY','THURMAN','FM','25-MAY-00');
insert into members values
('RR288','RONALD','ROGAN','FM','27-SEP-01');
insert into members values
('EJ257','EDWARD','JACKSON','AD','30-SEP-01');
insert into members values
('AR492','AARON','ROLLINS','ST','12-DEC-00');
insert into members values
('CB652','CHRIS','BECKER','ST','05-DEC-00');
insert into members values
('JJ891','JULIE','JOSEPH','CH','13-SEP-01');
insert into members values
('RJ533','ROGER','JACKSON','ST','22-JUL-02');
