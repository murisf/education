create table collections
(coll_id varchar2(5),
coll_desc varchar2(30),
num_items number(4),
exhibit_id number(4),
mem_id varchar2(5),
donate_date date,
coll_value number(7,2));
insert into collections values
('5PEZ0','CLASSIC PEZ DISPENSERS',20,1950,'GR009','20-OCT-01',95);
insert into collections values
('5JUKE','1950S JUKEBOX',1,1950,'GR009','01-NOV-01',500);
insert into collections values
('5POOD','POODLE SKIRT',2,1950,'AH119','26-FEB-02',50);
insert into collections values
('5SHOE','SADDLE SHOES',1,1950,'AH119','26-FEB-02',15);
insert into collections values
('5TV00','VINTAGE TV',1,1950,'GR009','01-NOV-01',125);
insert into collections values
('6BEAT','BEATLES RECORDS',6,1960,'TC769','30-MAY-99',65);
insert into collections values
('6PHAT','PILLBOX HAT',1,1960,'ST546','28-JAN-02',7.5);
insert into collections values
('6WOOD','SIGNED WOODSTOCK POSTER',1,1960,'MT907','07-JUN-02',200);
insert into collections values
('6SLOG','SLOGAN BUTTONS',20,1960,'MT907','22-JUL-02',20);
insert into collections values
('6TREK','ORIGINAL STARFLEET UNIFORM',1,1960,'AA910','05-APR-01',250);
insert into collections values
('7POST','BLACKLIGHT POSTERS',15,1970,'MT907','15-APR-02',100);
insert into collections values
('7SHOE','PLATFORM HEELS',5,1970,'JJ111','20-DEC-97',150);
insert into collections values
('7SUIT','LEISURE SUITS',2,1970,'JJ111','20-DEC-97',40);
insert into collections values
('7DISC','DISCO BALL',1,1970,'LS504','02-JAN-98',50);
insert into collections values
('8ATAR','ATARI GAME SYSTEM',1,1980,'AR492','15-DEC-00',60);
insert into collections values
('8GAME','ATARI GAME CARTRIDGES',20,1980,'AR492','15-DEC-00',100);
insert into collections values
('8BETA','BETA MAX PLAYER',1,1980,'EJ257','15-NOV-01',25);
insert into collections values
('8MOVI','BETA MAX MOVIES',10,1980,'EJ257','15-NOV-01',50);
insert into collections values
('8SMUR','SMURF FIGURINES',30,1980,'JJ111','12-OCT-98',60);
insert into collections values
('8CABB','CABBAGE PATCH DOLLS',10,1980,'JJ111','05-FEB-98',400);
insert into collections values
('9POKE','POKEMON TRADING CARDS',100,1990,'JJ891','22-JUL-02',100);
insert into collections values
('9BEAN','BEANIE BABIES',20,1990
,'JJ891','22-JUL-02',100);
insert into collections values
('9ELMO','TICKLE ME ELMO DOLL',1,1990,'PS107','15-MAY-02',10);
