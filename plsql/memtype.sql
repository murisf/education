create table memtype
(memcode varchar2(2), 
mem_descrip varchar2(10),
dues number(5,2));
insert into memtype values
('CH','CHILD',39.95);
insert into memtype values
('ST','STUDENT',49.95);
insert into memtype values
('AD','ADULT',54.95);
insert into memtype values
('SR','SENIOR',44.95);
insert into memtype values
('FM','FAMILY',74.95);
