CREATE TABLE week_pay
(week_ending DATE DEFAULT TO_DATE(NEXT_DAY(SYSDATE-7, 'FRIDAY')),
 empid VARCHAR2(5),
 num_hours NUMBER(4,2),
 reg_wages NUMBER(7,2),
 overtime NUMBER(7,2),
 tax NUMBER(7,2),
 take_home NUMBER(7,2));