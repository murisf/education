create table employee
(empid varchar2(5),
firstname varchar2(15),
midname varchar2(15),
lastname varchar2(15),
jobcode varchar2(3),
hiredate date,
hourrate number(5,2));
insert into employee values
('A1465','ROBERTA','JEAN','PICKNEY','SEC','14-JUN-99',10.65);
insert into employee values
('B2559','BIFF',null,'ARFUSS','DIR','01-NOV-97',39.25);
insert into employee values
('A5150','GINA','ANN','PEREZ','TRG','05-APR-98',11.25);
insert into employee values
('A4115','JANETTE','E.','FLOWERS','SEC','12-DEC-01',10.05);
insert into employee values
('A6395','MICHAEL','F.','CHEN','TRG','01-MAR-01',9.45);
insert into employee values
('B1158','RICHARD','ALONSO','JACKSON','CUR','01-NOV-97',28.5);
insert into employee values
('B1263','OLIVIA',null,'RUDOLF','CUR','06-DEC-97',27.35);
insert into employee values
('A1333','PATRICIA','K.','CARTER','TRG','23-JUN-02',8.5);
insert into employee values
('B3544','JOSEPH','AARON','RICE','CUR','15-MAY-02',23.65);
insert into employee values
('B2611','AMELIA','JANE','HAND','CUR','26-APR-00',24.8);
insert into employee values
('A1568','RAY','P.','LEVY','TRG','30-JAN-02',8.5);
insert into employee values
('A5015','BETTY','JO','RICCI','TRG','04-FEB-02',8.5);
insert into employee values
('A7812','JUDY','N.','LEE','TRG','25-JUN-00',10.25);
insert into employee values
('B7610','BEVERLY','SUE','WATERS','DEV','27-FEB-00',22.1);
insert into employee values
('B6655','DANIEL','Z.','POPOV','DEV','27-FEB-00',22.1);
insert into employee values
('B3678','RUTH','A.','OLIVER','TRC','06-AUG-00',18.75);
insert into employee values
('B9819','ANNA','M.','HILL','ACC','12-OCT-01',15.95);
