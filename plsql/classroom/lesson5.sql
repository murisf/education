SET serveroutput ON;
DECLARE
  TYPE last_tabType IS TABLE OF
   employee.lastname%TYPE INDEX BY Binary_integer;
  
  lastName_tab last_tabType;
  
  CURSOR lastname_cur IS
    SELECT lastname FROM employee ORDER BY hourrate desc;
  v_counter NUMBER(2) := 0;
BEGIN
  --load up the array
  FOR lastname_rec IN lastname_cur
  LOOP
    v_counter := v_counter + 1;
    lastname_tab(v_counter) := lastname_rec.lastname;
  END LOOP;
  -- loop through array and print each last name
  FOR i IN 1..v_counter
  LOOP
    DBMS_OUTPUT.PUT_LINE(lastname_tab(i));
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('Fifth element: ' || lastname_tab(5));
  DBMS_OUTPUT.PUT_LINE('Num employees: ' || lastname_tab.count);
END;