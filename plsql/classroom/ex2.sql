CREATE OR REPLACE PROCEDURE coll_inflation
 (i_id IN collections.coll_id%TYPE,
  i_apprec IN NUMBER := .1)
IS
  e_bad_id EXCEPTION;
BEGIN
  UPDATE collections
  SET coll_value = appreciation(i_apprec, coll_value)
  WHERE coll_id = UPPER(i_id);
  
  IF SQL%NOTFOUND
  THEN
    RAISE e_bad_id;
  END IF;
  
EXCEPTION
  WHEN e_bad_id THEN
    DBMS_OUTPUT.PUT_LINE('Please enter a valid collection id.');
END coll_inflation;

EXEC coll_inflation('5PEZ0', .125);
EXEC coll_inflation('5JUKE');
EXEC coll_inflation('5POOD',-0.05);
EXEC coll_inflation('1');

CREATE OR REPLACE PROCEDURE coll_inflation
 (i_id IN collections.coll_id%TYPE,
  i_apprec IN NUMBER := .1,
  o_coll_value OUT collections.coll_value%TYPE,
  o_item_value OUT collections.coll_value%TYPE)
IS
  e_bad_id EXCEPTION;
BEGIN
  UPDATE collections
  SET coll_value = coll_value * (1 + i_apprec)
  WHERE coll_id = UPPER(i_id)
  RETURNING coll_value, (coll_value/num_items)
    INTO o_coll_value, o_item_value;
  
  IF SQL%NOTFOUND
  THEN
    RAISE e_bad_id;
  END IF;
  
EXCEPTION
  WHEN e_bad_id THEN
    DBMS_OUTPUT.PUT_LINE('Please enter a valid collection id.');
END coll_inflation;

VARIABLE g_coll_value NUMBER;
VARIABLE g_item_value NUMBER;

EXECUTE coll_inflation('9POKE', 0.05, :g_coll_value, :g_item_value);

PRINT g_coll_value;
PRINT g_item_value;

DECLARE
  v_coll_value collections.coll_value%TYPE;
  v_item_value collections.coll_value%TYPE;
BEGIN
  coll_inflation('9BEAN', .2, v_coll_value, v_item_value);
  DBMS_OUTPUT.PUT_LINE('New collection value: $' || v_coll_value);
  DBMS_OUTPUT.PUT_LINE('Value per item:       $' || v_item_value);
END;

CREATE OR REPLACE PROCEDURE get_mem_info
  (i_memid  IN members.mem_id%TYPE,
   o_firstname OUT members.firstname%TYPE,
   o_lastname OUT members.lastname%TYPE,
   o_memcode OUT members.memcode%TYPE)
IS
BEGIN
  SELECT firstname, lastname, memcode
  INTO o_firstname, o_lastname, o_memcode
  FROM members
  WHERE mem_id = UPPER(i_memid);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Try again with a valid member ID.');
END get_mem_info;

SET serveroutput ON;
DECLARE 
  v_fname members.firstname%TYPE;
  v_lname members.lastname%TYPE;
  v_memcode members.memcode%TYPE;
BEGIN
  get_mem_info('AC', v_fname, v_lname, v_memcode);
  DBMS_OUTPUT.PUT_LINE(v_fname || ' ' || v_lname || ' is a ' ||
    v_memcode || ' member.');
END;


CREATE OR REPLACE PROCEDURE new_mem
 (i_id IN members.mem_id%TYPE,
  i_fname IN members.firstname%TYPE,
  i_lname IN members.lastname%TYPE,
  i_memcode IN members.memcode%TYPE := 'AD',
  i_memdate IN members.memdate%TYPE := sysdate)
IS
BEGIN
  INSERT INTO members (mem_id, firstname, lastname, memcode, memdate)
  VALUES (upper(i_id), upper(i_fname), upper(i_lname), upper(i_memcode),
    i_memdate);
END new_mem;

EXECUTE new_mem('ts111', 'tony', 'stalls', 'st', '11-APR-03')
EXECUTE new_mem('ll231', 'larry', 'lake')
EXECUTE new_mem('sy890', 'sidney', 'young', i_memdate =>'11-APR-03')