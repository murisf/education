create or replace Procedure emp_raise is
-- declare local variables
begin
-- executable code
  update employee
  set hourrate = hourrate * 1.04;
end emp_raise;

--drop procedure emp_raise;
--execute emp_raise;
Begin
  emp_raise;
end;