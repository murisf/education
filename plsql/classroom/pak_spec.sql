CREATE OR REPLACE PACKAGE emp_pay_pack
IS
  c_tax_rate CONSTANT NUMBER := .15;
  
  PROCEDURE payroll
    (i_empid IN employee.empid%TYPE,
     i_hours_worked IN week_pay.num_hours%TYPE := 40);
     
END emp_pay_pack;