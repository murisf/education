CREATE OR REPLACE PROCEDURE emp_raise
 (i_raise_percent IN NUMBER := .03,
  i_empid IN employee.empid%TYPE,
  o_newrate OUT employee.hourrate%TYPE)
IS
  --declare local variables
BEGIN
  UPDATE employee
  SET hourrate = hourrate * (1 + i_raise_percent)
  WHERE empid = i_empid
  RETURNING hourrate INTO o_newrate;
  
  --SELECT hourrate INTO o_newrate
  --FROM employee
  --WHERE empid = i_empid;
END emp_raise;

--positional notation
--execute emp_raise(.05, 'B2559');

--named notation, [must be used if setting default in parameter list]
--execute emp_raise(i_empid =>'B2559', i_raise_percent => .05);

--VARIABLE g_newrate NUMBER;
--EXECUTE emp_raise(.05, 'B2559', :g_newrate);
--PRINT g_newrate;

--Anonymous block
SET serveroutput ON;
DECLARE
  v_newRate employee.hourrate%TYPE;
BEGIN
  emp_raise(.04, 'B1263', v_newrate);
  DBMS_OUTPUT.PUT_LINE('new rate: ' || to_char(v_newRate, '$9999.99'));
END;

--Should cause error
SET serveroutput ON;
DECLARE
  v_newRate employee.hourrate%TYPE;
BEGIN
  emp_raise(.04, 'B', v_newrate);
  DBMS_OUTPUT.PUT_LINE('new rate: ' || to_char(v_newRate, '$9999.99'));
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Please pass a valid employee ID');
END;

SELECT * FROM EMPLOYEE WHERE EMPID = 'B';
