SET serveroutput ON;
CREATE OR REPLACE PACKAGE mem_pack
IS

  PROCEDURE new_mem
    (i_memid IN members.mem_id%TYPE,
     i_fname IN members.firstname%TYPE,
     i_lname IN members.lastname%TYPE,
     i_memcode IN members.memcode%TYPE,
     i_memdate IN members.memdate%TYPE := SYSDATE);
     
  PROCEDURE del_mem
    (i_memid IN members.mem_id%TYPE);
  
  PROCEDURE change_memcode
    (i_memid IN members.mem_id%TYPE,
     i_memcode IN members.memcode%TYPE);

END mem_pack;

CREATE OR REPLACE PACKAGE BODY mem_pack
IS
  FUNCTION get_dues
    (i_memcode IN members.memcode%TYPE)
    RETURN memtype.dues%TYPE
  IS
    v_dues memtype.dues%TYPE;
  BEGIN
    SELECT dues
    INTO v_dues
    FROM memtype
    WHERE memcode = i_memcode;
    RETURN (v_dues);
  END get_dues;
  
  PROCEDURE new_mem
   (i_memid IN members.mem_id%TYPE,
    i_fname IN members.firstname%TYPE,
    i_lname IN members.lastname%TYPE,
    i_memcode IN members.memcode%TYPE,
    i_memdate IN members.memdate%TYPE := SYSDATE)
  IS
    v_annualdues memtype.dues%TYPE;
  BEGIN
    INSERT INTO members VALUES(i_memid,i_fname,i_lname,i_memcode,i_memdate);
    v_annualdues := get_dues(i_memcode);
    DBMS_OUTPUT.PUT_LINE('Annual dues are: ' || v_annualdues);
  END new_mem;
  
  PROCEDURE del_mem
    (i_memid IN members.mem_id%TYPE)
  IS
  BEGIN
    DELETE FROM members WHERE mem_id = i_memid;
  END del_mem;
  
  PROCEDURE change_memcode
    (i_memid IN members.mem_id%TYPE,
     i_memcode IN members.memcode%TYPE)
  IS
    v_annualdues memtype.dues%TYPE;
  BEGIN
    UPDATE members
    SET memcode = i_memcode
    WHERE mem_id = i_memid;
    v_annualdues := get_dues(i_memcode);
    DBMS_OUTPUT.PUT_LINE('New Annual dues are: ' || v_annualdues);
  END change_memcode;
END mem_pack;

EXECUTE mem_pack.new_mem('Axxxx', 'Muris', 'Fazlic', 'ST');
EXECUTE mem_pack.change_memcode('Axxxx', 'AD');
EXECUTE mem_pack.del_mem('Axxxx');

select * from members;
desc members;