SET serveroutput ON;
DECLARE
  v_reg_tax NUMBER(9,2);
BEGIN
  SELECT SUM(reg_wages) * emp_pay_pack.c_tax_rate
  INTO v_reg_tax
  FROM week_pay;
  
  DBMS_OUTPUT.PUT_LINE('Total tax: ' || TO_CHAR(v_reg_tax,'$99999.99'));
END;

SELECT * FROM week_pay;
EXECUTE emp_pay_pack.payroll('A1465', 30);
EXECUTE emp_pay_pack.payroll('B2559', 40);
EXECUTE emp_pay_pack.payroll('A5150', 50);
EXECUTE emp_pay_pack.payroll('AXXX', 30);