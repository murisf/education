CREATE OR REPLACE PROCEDURE coll_inflation IS
BEGIN
  UPDATE collections
  SET coll_value = coll_value * 1.10
  WHERE exhibit_id IN ('1950', '1960');
  
  UPDATE collections
  SET coll_value = coll_value * 1.10
  WHERE exhibit_id NOT IN ('1950', '1960');
END coll_inflation;

CREATE OR REPLACE PROCEDURE cleanup_coll IS
  c_curr_year CONSTANT NUMBER(4) := TO_NUMBER(TO_CHAR(SYSDATE, 'YYYY'));
BEGIN
  DELETE FROM collections
  WHERE exhibit_id <= c_curr_year - 50;
  DBMS_OUTPUT.PUT_LINE('Rows deleted: ' || SQL%ROWCOUNT);
END cleanup_coll;

--select * from collections;