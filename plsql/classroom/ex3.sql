CREATE OR REPLACE FUNCTION circ_area
 (i_radius IN NUMBER)
RETURN NUMBER
IS
  c_pie CONSTANT NUMBER := 3.14159;
  v_area NUMBER;
BEGIN
  v_area := c_pie * i_radius * i_radius;
  RETURN v_area;
END circ_area;

VARIABLE g_area NUMBER;
EXECUTE :g_area := circ_area(2);
PRINT g_area;

SELECT circ_area(2) FROM dual;

CREATE OR REPLACE FUNCTION class_tax
 (i_price IN NUMBER)
RETURN NUMBER
IS
  c_tax_rate CONSTANT NUMBER := 0.075;
  v_tax NUMBER;
BEGIN
  v_tax := i_price * c_tax_rate;
  RETURN v_tax;
END class_tax;

SELECT workshop, price, class_tax(price) AS tax FROM classes;
SELECT workshop, (price + class_tax(price)) AS total FROM classes;

CREATE OR REPLACE FUNCTION appreciation
 (i_appreciation_rate IN NUMBER,
  i_coll_value IN NUMBER)
RETURN NUMBER
IS
  v_new_value NUMBER;
BEGIN
  v_new_value := i_coll_value * (1 + i_appreciation_rate);
  RETURN v_new_value;
END appreciation;

--revised coll_inflation in ex2.sql
EXECUTE coll_inflation('9ELMO', 0.05);

CREATE OR REPLACE FUNCTION class_code
 (i_workshop IN classes.workshop%TYPE,
  i_date IN classes.ws_date%TYPE) 
RETURN VARCHAR2
IS
  v_class_code VARCHAR2(7);
BEGIN
  v_class_code := SUBSTR(i_workshop,1,3) || TO_CHAR(i_date, 'MMYY');
  RETURN v_class_code;
END class_code;

SELECT workshop, ws_date, class_code(workshop, ws_date) AS "class code" FROM classes;
ALTER TABLE classes ADD class_code varchar2(7);
UPDATE classes SET class_code = class_code(workshop, ws_date);
