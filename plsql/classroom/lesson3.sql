CREATE OR REPLACE FUNCTION temp_converter
 (i_fah IN NUMBER)
RETURN NUMBER
IS
  v_cel NUMBER;
BEGIN
  v_cel := 5/9 * (i_fah - 32);
  RETURN v_cel;
END temp_converter;

VARIABLE g_cel number;
EXECUTE :g_cel := temp_converter(212);
PRINT g_cel;

SELECT temp_date, high_temp as FAH, round(temp_converter(high_temp),2) as CELSIUS
FROM temps
WHERE temp_converter(high_temp) > 20;

CREATE OR REPLACE FUNCTION cal_newrate
 (i_raise_perc IN NUMBER, i_curr_rate NUMBER)
RETURN NUMBER
IS
  v_newrate employee.hourrate%TYPE;
BEGIN
  v_newrate := i_curr_rate * (1 + i_raise_perc);
  RETURN v_newrate;
END cal_newrate;
