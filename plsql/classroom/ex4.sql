SET serveroutput ON;
CREATE OR REPLACE PROCEDURE ex_4_procedure
  (i_exhibit_id IN exhibits.exhibit_id%TYPE)
IS
  exhibit_rec exhibits%ROWTYPE;
BEGIN
  SELECT *
  INTO exhibit_rec
  FROM exhibits
  WHERE exhibit_id = i_exhibit_id;
  
  DBMS_OUTPUT.PUT_LINE('Exhibit Id : ' || exhibit_rec.exhibit_id);
  DBMS_OUTPUT.PUT_LINE('Room       : ' || exhibit_rec.room);
  DBMS_OUTPUT.PUT_LINE('Employee Id: ' || exhibit_rec.empid);
  
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Please enter a valid exhibit id');
END ex_4_procedure;

SET serveroutput ON;
EXECUTE ex_4_procedure('1950');
EXECUTE ex_4_procedure('1940');

CREATE OR REPLACE PROCEDURE coll_list
  (i_emp_id IN employee.empid%TYPE)
IS
  TYPE coll_desc_tabType IS TABLE OF
   collections.coll_desc%TYPE INDEX BY Binary_integer;
  
  coll_desc_table coll_desc_tabType;
  
  CURSOR colldesc_cur IS
    SELECT coll_desc
    FROM collections c
    WHERE exhibit_id = (SELECT exhibit_id
                        FROM exhibits
                        WHERE empid = i_emp_id)
    ORDER BY donate_date desc;
  v_counter NUMBER(2) := 0;
BEGIN
  FOR colldesc_rec IN colldesc_cur
  LOOP
    v_counter := v_counter + 1;
    coll_desc_table(v_counter) := colldesc_rec.coll_desc;
  END LOOP;
  
  IF coll_desc_table.EXISTS(1) THEN
    FOR i IN 1..v_counter
    LOOP
      DBMS_OUTPUT.PUT_LINE(coll_desc_table(i));
    END LOOP;
    
    DBMS_OUTPUT.PUT_LINE('Num of collections: ' || coll_desc_table.count);
  ELSE
    DBMS_OUTPUT.PUT_LINE('Try again with a valid curator ID.');
  END IF;
  
END coll_list;

EXECUTE coll_list('B1158');
EXECUTE coll_list('A1465');

CREATE TABLE old_classes (class varchar(20));

CREATE OR REPLACE PROCEDURE ex_4_a
IS
  TYPE oldclasses_tabType IS TABLE OF
   classes.workshop%TYPE INDEX BY Binary_integer;
  
  oldclasses_table oldclasses_tabType;
  
  CURSOR oldclasses_cur IS
    SELECT workshop
    FROM classes
    WHERE ws_date < SYSDATE - 7;
  v_counter NUMBER(2) := 0;
BEGIN
  FOR oldclasses_rec IN oldclasses_cur
  LOOP
    v_counter := v_counter + 1;
    oldclasses_table(v_counter) := oldclasses_rec.workshop;
  END LOOP;
  
  IF oldclasses_table.EXISTS(1) THEN
    FOR i IN 1..v_counter
    LOOP
      INSERT INTO old_classes VALUES (oldclasses_table(i));
      DELETE FROM classes WHERE workshop = oldclasses_table(i);
    END LOOP;
  END IF;
END;

EXECUTE ex_4_a;
select * from classes;