CREATE OR REPLACE TRIGGER no_del
  BEFORE DELETE ON exhibits
BEGIN
  IF TO_CHAR(SYSDATE, 'MM') = 03 THEN
      RAISE_APPLICATION_ERROR(-20002, 'No deletion this month');
  END IF;
END no_pay_change;

DELETE FROM exhibits;

CREATE TABLE priv_group
( priv_grp VARCHAR2(20),
  fname VARCHAR2(20),
  lname VARCHAR2(20),
  phone VARCHAR2(10)
);

CREATE OR REPLACE TRIGGER priv_grp
  AFTER INSERT OR UPDATE OF priv_grp ON classes
  FOR EACH ROW
  WHEN(NEW.priv_grp IS NOT NULL)
BEGIN
  INSERT INTO priv_group(priv_grp)
  VALUES(:NEW.priv_grp);
END priv_grp;

CREATE TABLE class_log
( change_date DATE DEFAULT SYSDATE,
  change_type VARCHAR2(6),
  workshop VARCHAR2(40),
  new_ws_date DATE,
  old_ws_date DATE,
  new_instruct VARCHAR2(5),
  old_instruct VARCHAR2(5)
);

CREATE OR REPLACE TRIGGER audit_class
  AFTER INSERT OR DELETE OR UPDATE OF ws_date, empid ON classes
  FOR EACH ROW
BEGIN
  IF INSERTING THEN
    INSERT INTO class_log

select * from classes;