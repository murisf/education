CREATE VIEW emp_classes AS
   (SELECT workshop, price, limit,
       price * limit AS max_income,
       lastname, hourrate * 4 AS emp_pay
    FROM classes c, employee e
    WHERE c.empid = e.empid);

