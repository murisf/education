CREATE OR REPLACE TRIGGER audit_class
   AFTER INSERT OR DELETE OR 
   UPDATE OF ws_date, empid ON classes
   FOR EACH ROW
BEGIN
   IF INSERTING THEN
      INSERT INTO class_log (change_type, workshop,
          new_ws_date, new_instruct)
      VALUES ('INSERT',:NEW.workshop, :NEW.ws_date, 
          :NEW.empid);
   ELSIF DELETING THEN
      INSERT INTO class_log (change_type, workshop,
          old_ws_date, old_instruct)
      VALUES ('DELETE',:OLD.workshop, :OLD.ws_date, 
          :OLD.empid);
   ELSIF UPDATING THEN
      INSERT INTO class_log (change_type, workshop,
          new_ws_date ,old_ws_date, new_instruct, old_instruct)
      VALUES ('UPDATE',:NEW.workshop, :NEW.ws_date, :OLD.ws_date, 
          :NEW.empid, :OLD.empid);
   END IF;
END audit_class;