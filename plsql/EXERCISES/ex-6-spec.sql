CREATE OR REPLACE PACKAGE mem_pack
IS

   PROCEDURE new_mem
     (i_memid IN members.mem_id%TYPE,
      i_firstname IN members.firstname%TYPE,
      i_lastname IN members.lastname%TYPE,
      i_memcode IN members.memcode%TYPE,
      i_memdate IN members.memdate%TYPE DEFAULT SYSDATE);
   
   PROCEDURE del_mem
     (i_memid IN members.mem_id%TYPE);

   PROCEDURE del_mem
     (i_lastname IN members.lastname%TYPE,
      i_firstname IN members.firstname%TYPE);

   PROCEDURE change_memcode
     (i_memid IN members.mem_id%TYPE,
      i_memcode IN members.memcode%TYPE);

END mem_pack;