CREATE OR REPLACE PACKAGE BODY coll_pack
IS

  FUNCTION appreciation
    (i_curr_value IN collections.coll_value%TYPE,
     i_apprec_rate IN NUMBER)
     RETURN NUMBER
  IS
     v_new_value collections.coll_value%TYPE;
  BEGIN
     v_new_value := i_curr_value + (i_curr_value * i_apprec_rate);
     RETURN v_new_value;
  END appreciation;
 
  PROCEDURE coll_inflation
    (i_coll_id     IN collections.coll_id%TYPE,
     i_apprec_rate IN NUMBER := .1,
     o_coll_value  OUT collections.coll_value%TYPE,
     o_item_value  OUT collections.coll_value%TYPE)
  IS
  -- declare user-defined exception
     e_bad_id EXCEPTION; 

  BEGIN
     UPDATE collections
     SET    coll_value = appreciation(coll_value, i_apprec_rate)
     WHERE  coll_id = UPPER(i_coll_id)
     RETURNING coll_value, (coll_value/num_items) 
       INTO o_coll_value, o_item_value;

  -- raise exception when no rows are updated
     IF SQL%NOTFOUND
     THEN 
       RAISE e_bad_id;
     END IF;      

  EXCEPTION
     WHEN e_bad_id THEN
       DBMS_OUTPUT.PUT_LINE('Try again with a valid collection ID.');

  END coll_inflation;

END coll_pack;