CREATE OR REPLACE PROCEDURE coll_inflation
IS
BEGIN
   UPDATE collections
   SET    coll_value = coll_value + (coll_value * .1);
END coll_inflation;
