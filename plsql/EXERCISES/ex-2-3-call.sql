DECLARE
   v_fname members.firstname%TYPE;
   v_lname members.lastname%TYPE;
   v_memcode members.memcode%TYPE;
BEGIN
   get_mem_info('AB135', v_fname, v_lname, v_memcode);
   DBMS_OUTPUT.PUT_LINE(v_fname || ' ' || v_lname || ' is a ' || 
                        v_memcode || ' member.');
END;