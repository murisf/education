CREATE OR REPLACE FUNCTION class_tax
   (i_price classes.price%TYPE)
   RETURN NUMBER
IS
   c_tax_rate CONSTANT NUMBER := .075;
   v_tax_amt  NUMBER(5,2);
BEGIN
   v_tax_amt := i_price * c_tax_rate;
   RETURN v_tax_amt;
END class_tax;