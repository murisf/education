CREATE OR REPLACE FUNCTION class_code
   (i_ws_name  classes.workshop%TYPE,
    i_ws_date  classes.ws_date%TYPE)
   RETURN VARCHAR2
IS
   v_class_code VARCHAR2(7);
BEGIN
   v_class_code := SUBSTR(i_ws_name, 1, 3) ||
                   TO_CHAR(i_ws_date, 'MM') ||
                   TO_CHAR(i_ws_date, 'YY');
   RETURN v_class_code;
END class_code;