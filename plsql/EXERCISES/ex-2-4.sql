CREATE OR REPLACE PROCEDURE new_mem
   (i_memid     members.mem_id%TYPE,
    i_firstname members.firstname%TYPE,
    i_lastname  members.lastname%TYPE,
    i_memcode   members.memcode%TYPE := 'AD',
    i_memdate   members.memdate%TYPE := SYSDATE)
IS
BEGIN
    INSERT INTO members (mem_id, firstname, lastname, memcode, memdate)
    VALUES (UPPER(i_memid), UPPER(i_firstname), UPPER(i_lastname), 
            UPPER(i_memcode), i_memdate);
END new_mem;