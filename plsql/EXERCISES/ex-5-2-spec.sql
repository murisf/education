CREATE OR REPLACE PACKAGE coll_pack
IS

  FUNCTION appreciation
    (i_curr_value IN collections.coll_value%TYPE,
     i_apprec_rate IN NUMBER)
     RETURN NUMBER;

   PROCEDURE coll_inflation
     (i_coll_id     IN collections.coll_id%TYPE,
      i_apprec_rate IN NUMBER := .1,
      o_coll_value  OUT collections.coll_value%TYPE,
      o_item_value  OUT collections.coll_value%TYPE);

END coll_pack;