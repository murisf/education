CREATE OR REPLACE FUNCTION appreciation
   (i_curr_value IN collections.coll_value%TYPE,
    i_apprec_rate IN NUMBER)
   RETURN NUMBER
IS
   v_new_value collections.coll_value%TYPE;
BEGIN
   v_new_value := i_curr_value + (i_curr_value * i_apprec_rate);
   RETURN v_new_value;
END appreciation;