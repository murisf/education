CREATE OR REPLACE PROCEDURE get_exhibit_info
   (i_exhibit_id exhibits.exhibit_id%TYPE)
IS
   exhibit_rec exhibits%ROWTYPE;
BEGIN
   SELECT *
   INTO   exhibit_rec
   FROM   exhibits
   WHERE  exhibit_id = i_exhibit_id;
   DBMS_OUTPUT.PUT_LINE('Exhibit ID: ' || exhibit_rec.exhibit_id);
   DBMS_OUTPUT.PUT_LINE('Room number: ' || exhibit_rec.room);
   DBMS_OUTPUT.PUT_LINE('Curator: ' || exhibit_rec.empid);
EXCEPTION
   WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Try again with a valid exhibit ID.');
END get_exhibit_info;
