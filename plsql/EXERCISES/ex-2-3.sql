CREATE OR REPLACE PROCEDURE get_mem_info
   (i_memid     IN  members.mem_id%TYPE,
    o_firstname OUT members.firstname%TYPE,
    o_lastname  OUT members.lastname%TYPE,
    o_memcode   OUT members.memcode%TYPE)
IS
BEGIN
   SELECT firstname, lastname, memcode
   INTO   o_firstname, o_lastname, o_memcode
   FROM   members
   WHERE  mem_id = UPPER(i_memid);
EXCEPTION
   WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Try again with a valid member ID.');
END get_mem_info;