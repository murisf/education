CREATE OR REPLACE PROCEDURE coll_inflation
   (i_coll_id     IN collections.coll_id%TYPE,
    i_apprec_rate IN NUMBER := .1)
IS
-- declare user-defined exception
   e_bad_id EXCEPTION; 

BEGIN
   UPDATE collections
   SET    coll_value = coll_value + (coll_value * i_apprec_rate)
   WHERE  coll_id = UPPER(i_coll_id);

-- raise exception when no rows are updated
   IF SQL%NOTFOUND
   THEN 
      RAISE e_bad_id;
   END IF;      

EXCEPTION
   WHEN e_bad_id THEN
      DBMS_OUTPUT.PUT_LINE('Try again with a valid collection ID.');

END coll_inflation;
