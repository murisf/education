CREATE OR REPLACE PROCEDURE cleanup_coll
IS 
   c_curr_year CONSTANT NUMBER(4) := TO_NUMBER(TO_CHAR(SYSDATE, 'YYYY'));
BEGIN
   DELETE FROM collections
   WHERE exhibit_id <= c_curr_year - 50;
   DBMS_OUTPUT.PUT_LINE('Rows deleted: ' || SQL%ROWCOUNT); 
END cleanup_coll;

