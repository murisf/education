CREATE OR REPLACE PROCEDURE coll_list
   (i_curid IN employee.empid%TYPE)
IS
   TYPE coll_tabtype IS TABLE OF 
      collections.coll_desc%TYPE
      INDEX BY BINARY_INTEGER;

   coll_tab coll_tabtype;

   CURSOR coll_cur IS
      SELECT   coll_desc
      FROM     collections c, exhibits e
      WHERE    c.exhibit_id = e.exhibit_id
           AND empid = i_curid
      ORDER BY donate_date; 

   v_counter NUMBER(2) := 0;

BEGIN
   
   FOR coll_rec IN coll_cur
   LOOP
      v_counter := v_counter + 1;
      coll_tab(v_counter) := coll_rec.coll_desc;
   END LOOP;    

   IF coll_tab.EXISTS(1) THEN
      DBMS_OUTPUT.PUT_LINE('Collections managed by ' || i_curid);

      FOR i IN 1 .. v_counter
         LOOP
         DBMS_OUTPUT.PUT_LINE(coll_tab(i));
      END LOOP;   

      DBMS_OUTPUT.PUT_LINE('Total number of collections: ' ||
                           coll_tab.COUNT);
   ELSE
      DBMS_OUTPUT.PUT_LINE('Try again with a valid curator ID.');
   END IF;      

END coll_list; 