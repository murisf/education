CREATE OR REPLACE TRIGGER priv_grp
   AFTER INSERT OR UPDATE OF priv_grp 
   ON classes
   FOR EACH ROW
   WHEN (NEW.priv_grp IS NOT NULL)
BEGIN
   INSERT INTO priv_group (grp_name)
   VALUES (:NEW.priv_grp);
END priv_grp;