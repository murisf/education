CREATE TABLE old_classes
(workshop VARCHAR2(40));

CREATE OR REPLACE PROCEDURE archive_classes

IS 

   TYPE old_class_tabtype IS TABLE OF
      classes.workshop%TYPE
      INDEX BY BINARY_INTEGER;

   old_class_tab old_class_tabtype; 

   CURSOR old_class_cur IS
      SELECT workshop
      FROM   classes
      WHERE  MONTHS_BETWEEN(SYSDATE, ws_date) >= 7;

   v_counter NUMBER(2) := 0;
 
BEGIN 

   FOR old_class_rec IN old_class_cur
   LOOP
      v_counter := v_counter + 1;
      old_class_tab(v_counter) := old_class_rec.workshop;
   END LOOP;

   FOR i IN 1 .. v_counter
   LOOP
      INSERT INTO old_classes (workshop)
      VALUES (old_class_tab(i));
      DBMS_OUTPUT.PUT_LINE('Inserted record for ' || old_class_tab(i));
      DELETE FROM classes 
      WHERE workshop = old_class_tab(i);
      DBMS_OUTPUT.PUT_LINE('Deleted record for ' || old_class_tab(i));
   END LOOP; 

END archive_classes;    
