CREATE OR REPLACE FUNCTION circ_area
   (i_rad IN NUMBER)
   RETURN NUMBER
IS
   c_pi CONSTANT NUMBER := 3.14159;
   v_area NUMBER;
BEGIN
   v_area := 2 * c_pi * (i_rad * i_rad);
   RETURN v_area;
END circ_area;