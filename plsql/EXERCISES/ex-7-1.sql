CREATE OR REPLACE TRIGGER no_exh_del
   BEFORE DELETE ON exhibits
BEGIN
   IF TO_CHAR(SYSDATE, 'MM') = '04'
   THEN 
      RAISE_APPLICATION_ERROR(-20500, 
         'No exhibit deletions allowed during April');
   END IF;
END no_exh_del;