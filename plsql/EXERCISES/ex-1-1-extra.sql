CREATE OR REPLACE PROCEDURE coll_inflation
IS
   CURSOR coll_cur IS 
      SELECT DISTINCT exhibit_id 
      FROM collections; 
   v_inflation NUMBER; 
BEGIN
   FOR coll_rec IN coll_cur   
   LOOP
      IF coll_rec.exhibit_id IN (1950, 1960)
      THEN
         v_inflation := .1; 
      ELSE 
         v_inflation := .05;
      END IF; 
      UPDATE collections
      SET    coll_value = coll_value + (coll_value * v_inflation)
      WHERE  exhibit_id = coll_rec.exhibit_id;
   END LOOP;
END coll_inflation;
