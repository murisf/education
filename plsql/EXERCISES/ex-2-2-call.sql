DECLARE
   v_coll_value collections.coll_value%TYPE;
   v_item_value collections.coll_value%TYPE;
BEGIN
   coll_inflation('9BEAN', .2, v_coll_value, v_item_value);
   DBMS_OUTPUT.PUT_LINE('New collection value: $' || v_coll_value);
   DBMS_OUTPUT.PUT_LINE('Value per item:       $' || v_item_value);
END;