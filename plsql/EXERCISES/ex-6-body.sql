CREATE OR REPLACE PACKAGE BODY mem_pack
IS

   v_dues memtype.dues%TYPE;      

   v_counter NUMBER(2); 

   FUNCTION get_dues
      (i_memcode IN members.memcode%TYPE)
      RETURN memtype.dues%TYPE
   IS
   BEGIN
      SELECT dues
      INTO   v_dues
      FROM   memtype
      WHERE  memcode = i_memcode;      
      RETURN v_dues; 
   END get_dues;

   PROCEDURE new_mem
     (i_memid IN members.mem_id%TYPE,
      i_firstname IN members.firstname%TYPE,
      i_lastname IN members.lastname%TYPE,
      i_memcode IN members.memcode%TYPE,
      i_memdate IN members.memdate%TYPE DEFAULT SYSDATE)
   IS 
   BEGIN
      INSERT INTO members (mem_id, firstname, lastname, 
           memcode, memdate)
      VALUES (i_memid, i_firstname, i_lastname, 
           i_memcode, i_memdate);
      v_dues := GET_DUES(i_memcode);
      DBMS_OUTPUT.PUT_LINE('Annual dues are: $' || v_dues);
      v_counter := v_counter + 1;
      DBMS_OUTPUT.PUT_LINE('Total memberships: ' || v_counter);
   END new_mem;
   
   PROCEDURE del_mem
     (i_memid IN members.mem_id%TYPE)
   IS
   BEGIN
     DELETE FROM members
     WHERE mem_id = i_memid;
     v_counter := v_counter - 1;
     DBMS_OUTPUT.PUT_LINE('Total memberships: ' || v_counter);
   END del_mem;

   PROCEDURE del_mem
     (i_lastname IN members.lastname%TYPE,
      i_firstname IN members.firstname%TYPE)
   IS
   BEGIN
     DELETE FROM members
     WHERE lastname = i_lastname
       AND firstname = i_firstname;
     v_counter := v_counter - 1;
     DBMS_OUTPUT.PUT_LINE('Total memberships: ' || v_counter); 
   END del_mem;  

   PROCEDURE change_memcode
     (i_memid IN members.mem_id%TYPE,
      i_memcode IN members.memcode%TYPE)
   IS
   BEGIN
     UPDATE members
     SET    memcode = i_memcode
     WHERE  mem_id = i_memid;
     v_dues := GET_DUES(i_memcode);
     DBMS_OUTPUT.PUT_LINE('New dues are: $' || v_dues);
   END change_memcode;

BEGIN 
   SELECT COUNT(mem_id)
   INTO v_counter
   FROM members;

END mem_pack;