create table eval 
 (empid varchar2(5),
  eval_year number(4),
  eval_date DATE, 
  rating number(1),
  eval_count number(3));
DECLARE
    v_eval_year eval.eval_year%TYPE;
    CURSOR emps_cur
    IS
       SELECT empid, hiredate FROM employee
       ORDER BY hiredate;
 BEGIN
    FOR v_eval_year IN 2000 .. 2003
    LOOP
       FOR emp_rec IN emps_cur
       LOOP
          EXIT WHEN TO_NUMBER(TO_CHAR(emp_rec.hiredate, 'YYYY')) > v_eval_year;
          INSERT INTO eval(empid, eval_year)
          VALUES (emp_rec.empid, v_eval_year);
       END LOOP;
--    DBMS_OUTPUT.PUT_LINE (v_eval_year);
    END LOOP;
END;
/
UPDATE eval
SET eval_date = '02-MAY-00'
WHERE eval_year = 2000;
UPDATE eval
SET eval_date = '11-MAY-01'
WHERE eval_year = 2001;
UPDATE eval
SET eval_date = '08-MAY-02'
WHERE eval_year = 2002;
UPDATE eval
SET eval_date = '02-MAY-03'
WHERE eval_year = 2003;
alter table eval
drop column eval_year;
update eval
set eval_count = rownum;
UPDATE eval
SET rating = 2
WHERE MOD(eval_count, 3) = 0 OR MOD(eval_count, 2) = 0;
UPDATE eval
SET rating = 1
WHERE MOD(eval_count, 4) = 0;
UPDATE eval
SET rating = 3
WHERE rating IS NULL;
alter table eval
drop column eval_count;

