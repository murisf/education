<!DOCTYPE html>
<?php
if (isset($_GET['mn'])) {
    $mn = intval($_GET['mn']);
} else {
    $mn = 0;
}

$dbhost = "localhost";
$dbuser = "root";
$dbpassword = "";
$dbname = "mook";

$con = mysql_connect($dbhost, $dbuser, $dbpassword);

if (!$con) {
    die('Could not connect: ' . mysql_error());
}

mysql_select_db($dbname, $con);

$tblArr = array();
$tblArr[] = "books";
$tblArr[] = "film";
$tblArr[] = "library";
$tblArr[] = "music";
$tblArr[] = "videogames";

$table_name = $tblArr[$mn];

$sql = "SHOW COLUMNS FROM $table_name";
$result = mysql_query($sql);

while ($record = mysql_fetch_array($result)) {
    $fields[] = $record['0'];
}

$optArr = array();
$optArr[] = "Books";
$optArr[] = "Film";
$optArr[] = "Library";
$optArr[] = "Music";
$optArr[] = "Videogames";

$data2dArr = array();

$query = "SELECT * FROM  $table_name";
$result = mysql_query($query);

while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    $i = 0;
    foreach ($line as $col_value) {
        $data2dArr[$i][] = $col_value;
        $i++;
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mooks Library</title>
    </head>
    <body>
        <table>
            <tr>
                <?php
                for ($i = 0; $i < count($optArr); $i++) {
                    ?>
                    <td style="width: 7em">
                        <?php
                        if ($mn == $i) {
                            ?>
                            <b><?php print $optArr[$i]; ?></b>
                            <?php
                        } else {
                            ?>
                            <a href="index.php?mn=<?php print $i; ?>">
                                <?php print $optArr[$i]; ?>
                            </a>
                            <?php
                        }
                        ?>
                    </td>
                    <?php
                }
                ?>
            </tr>
        </table>
        <hr />
        <table>
            <tr>
                <?php
                for ($i = 0; $i < count($fields); $i++) {
                    ?>
                    <th style="width: 8em"><?php print $fields[$i]; ?></th>
                        <?php
                    }
                    ?>
            </tr>
            <?php
            for ($j = 0; $j < count($data2dArr[0]); $j++) {
                ?>
                <tr>
                    <?php
                    for ($k = 0; $k < count($fields); $k++) {
                        ?>
                        <td><?php print $data2dArr[$k][$j]; ?></td>
                        <?php
                    }
                    ?>
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
<?php
mysql_close($con);
?>