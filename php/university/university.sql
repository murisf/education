-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2015 at 05:53 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `university`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `course_number` varchar(10) NOT NULL,
  `course_name` varchar(50) DEFAULT '',
  `credit_hours` int(1) unsigned DEFAULT '0',
  `department` varchar(10) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_number`, `course_name`, `credit_hours`, `department`) VALUES
('CS1310', 'Intro to Computer Science', 4, 'CS'),
('CS3320', 'Data Structures', 4, 'CS'),
('MATH2410', 'Discrete Mathematics', 3, 'MATH'),
('CS3380', 'Database', 3, 'CS'),
('MATH2010', '0', 3, 'MATH'),
('CS3130', 'Web Programming', 3, 'CS'),
('CS4280', 'Compilers', 3, 'CS');

-- --------------------------------------------------------

--
-- Table structure for table `grade_report`
--

CREATE TABLE IF NOT EXISTS `grade_report` (
  `student_number` int(10) unsigned NOT NULL,
  `section_identifier` int(10) NOT NULL,
  `grade` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_report`
--

INSERT INTO `grade_report` (`student_number`, `section_identifier`, `grade`) VALUES
(17, 112, 'B'),
(17, 119, 'C'),
(8, 85, 'A'),
(8, 92, 'A'),
(8, 102, 'B'),
(8, 135, 'A'),
(23, 135, 'A'),
(5, 123, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `mysection`
--

CREATE TABLE IF NOT EXISTS `mysection` (
  `section_identifier` int(10) unsigned NOT NULL,
  `course_number` varchar(10) NOT NULL,
  `semester` varchar(10) DEFAULT '',
  `myyear` varchar(10) DEFAULT '',
  `instructor` varchar(10) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mysection`
--

INSERT INTO `mysection` (`section_identifier`, `course_number`, `semester`, `myyear`, `instructor`) VALUES
(85, 'MATH2410', 'Fall', '07', 'King'),
(92, 'CS1310', 'Fall', '07', 'Anderson'),
(102, 'CS3320', 'Spring', '08', 'Knuth'),
(112, 'MATH2410', 'Fall', '08', 'Chang'),
(119, 'CS1310', 'Fall', '08', 'Anderson'),
(135, 'CS3380', 'Fall', '08', 'Stone'),
(146, 'CS1234', 'Spring', '9', 'Larry'),
(64, 'CS1000', 'Fall', '9', 'Stark'),
(33, 'CS0100', 'Spring', '06', 'Ben'),
(12, 'CS1280', 'Fall', '05', 'Tim');

-- --------------------------------------------------------

--
-- Table structure for table `prerequisite`
--

CREATE TABLE IF NOT EXISTS `prerequisite` (
  `course_number` varchar(10) NOT NULL,
  `prerequisite_number` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prerequisite`
--

INSERT INTO `prerequisite` (`course_number`, `prerequisite_number`) VALUES
('CS3380', 'CS3320'),
('CS3380', 'MATH2410'),
('CS3320', 'CS1310'),
('CS4280', 'CS2270'),
('CS1234', 'CS1000');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_number` int(10) unsigned NOT NULL,
  `name` varchar(30) DEFAULT '',
  `myclass` int(1) unsigned DEFAULT '0',
  `major` varchar(10) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_number`, `name`, `myclass`, `major`) VALUES
(17, 'Smith', 1, 'CS'),
(8, 'Brown', 2, 'CS'),
(23, 'dan', 23, 'BS'),
(12, 'Someone', 4, 'CS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
 ADD PRIMARY KEY (`course_number`);

--
-- Indexes for table `mysection`
--
ALTER TABLE `mysection`
 ADD PRIMARY KEY (`section_identifier`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`student_number`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
