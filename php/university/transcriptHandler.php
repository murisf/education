<?php
if (isset($_GET['tr'])) {
    $tr = intval($_GET['tr']);
} else {
    $tr = 0;
}

$dbhost = "localhost";
$dbuser = "root";
$dbpassword = "";
$dbname = "university";

$con = mysql_connect($dbhost, $dbuser, $dbpassword);

if (!$con) {
    die('Could not connect: ' . mysql_error());
}

mysql_select_db($dbname, $con);

// Arrays
$headArr = array();
$fields = array();
$transcript = array();

$headArr[] = "Home";
$headArr[] = "Transcript";

$fields[] = "Student_name";
$fields[] = "Course_number";
$fields[] = "Grade";
$fields[] = "Semester";
$fields[] = "Year";
$fields[] = "Section";

// Select the students with proper criteria
$query = "
	SELECT	name, course_number, grade, semester, myyear, m1.section_identifier
	FROM	student AS s1, mysection AS m1, grade_report AS g1
	WHERE	s1.student_number = g1.student_number
	AND		g1.section_identifier = m1.section_identifier
	ORDER BY	s1.student_number DESC, g1.section_identifier
";

$result = mysql_query($query);

while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    $i = 0;
    foreach ($line as $col_value) {
        $transcript[$i][] = $col_value;
        $i++;
    }
}

// Count the number of reoccuring students
$countArr = array();
$query = "
	SELECT	student_number, count(*)
	FROM	grade_report
	GROUP BY	student_number DESC
";
$result = mysql_query($query);

while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    $i = 0;
    foreach ($line as $col_value) {
        $countArr[$i][] = $col_value;
        $i++;
    }
}

// Transpose function
function transpose($array) {
    array_unshift($array, null);
    return call_user_func_array('array_map', $array);
}

$transcript = transpose($transcript);

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>University</title>
		<link rel="stylesheet" type="text/css" href="css/site.css"/>
    </head>
	<body background="images/Wenjieton.gif">
		<div class="container">
			<table>
				<tr>
					<?php
					for ($i = 0; $i < count($headArr); $i++) {
						?>
						<td style="width: 7em">
							<?php
							if ($tr == $i) {
								?>
								<b><?php print $headArr[$i]; ?></b>
								<?php
							} else {
								?>
								<a href="index.php?mn=<?php print $i; ?>">
									<?php print $headArr[$i]; ?>
								</a>
								<?php
							}
							?>
						</td>
						<?php
					}
					?>
				</tr>
			</table>
			<hr />
			<table>
				<tr center>
					<th>Student_transcript</th>
				</tr>
				<tr>
					<?php
					for ($t = 0; $t < count($fields); $t++) {
						?>
							<th><?php echo $fields[$t];?></th>
						<?php
					}
					?>
				</tr>
				<?php
					for ($i = 0; $i < count($transcript); $i++) {
						?>
						<tr>
						<?php
						for ($j = 0; $j < count($transcript[$i]); $j++) {
							?>
								<td><?php echo $transcript[$i][$j];?></td>
							<?php
						}
						?>
						</tr>
						<?php
					} 
				?>
			</table>
		</div>
    </body>
</html>

<?php
mysql_close($con);
?>