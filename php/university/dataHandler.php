<?php

if (isset($_GET['mn'])) {
    $mn = intval($_GET['mn']);
} else {
    $mn = 0;
}

$dbhost = "localhost";
$dbuser = "root";
$dbpassword = "";
$dbname = "university";

$con = mysql_connect($dbhost, $dbuser, $dbpassword);

if (!$con) {
    die('Could not connect: ' . mysql_error());
}

mysql_select_db($dbname, $con);

switch($mn){
	case 0:
		if (isset($_POST['student_number_'])) {
			$snumber = intval($_POST['student_number_']);
		} else {
			$snumber = 0;
		}

		if (isset($_POST['name_'])) {
			$sname = $_POST['name_'];
		} else {
			$sname = "";
		}

		if (isset($_POST['myclass_'])) {
			$sclass = intval($_POST['myclass_']);
		} else {
			$sclass = 0;
		}

		if (isset($_POST['major_'])) {
			$smajor = $_POST['major_'];
		} else {
			$smajor = "";
		}

		if ($snumber > 0 && strlen($sname) > 0 && $sclass > 0 && strlen($smajor) > 0) {
			$fields = "(student_number, name, myclass, major)";
			$values = "($snumber, '$sname', $sclass, '$smajor')";

			$query = "INSERT INTO student " . $fields . " VALUES " . $values;
			mysql_query($query);
		}
		break;
	
	case 1:
		if (isset($_POST['course_number_'])) {
			$cnumber = $_POST['course_number_'];
		} else {
			$cnumber = "";
		}

		if (isset($_POST['course_name_'])) {
			$cname = $_POST['course_name_'];
		} else {
			$cname = "";
		}

		if (isset($_POST['credit_hours_'])) {
			$chour = intval($_POST['credit_hours_']);
		} else {
			$chour = 0;
		}

		if (isset($_POST['department_'])) {
			$cdep = $_POST['department_'];
		} else {
			$cdep = "";
		}

		if (strlen($cnumber) > 0 && strlen($cname) > 0 && $chour > 0 && strlen($cdep) > 0) {
			$fields = "(course_number, course_name, credit_hours, department)";
			$values = "('$cnumber', '$cname', $chour, '$cdep')";

			$query = "INSERT INTO course " . $fields . " VALUES " . $values;
			mysql_query($query);
		}
		break;
	
	case 2:
		if (isset($_POST['section_identifier_'])) {
			$secId = intval($_POST['section_identifier_']);
		} else {
			$secId = 0;
		}

		if (isset($_POST['course_number_'])) {
			$secNumb = $_POST['course_number_'];
		} else {
			$secNumb = "";
		}

		if (isset($_POST['semester_'])) {
			$secSem = $_POST['semester_'];
		} else {
			$secSem = "";
		}

		if (isset($_POST['myyear_'])) {
			$secYear = $_POST['myyear_'];
		} else {
			$secYear = "";
		}
		
		if (isset($_POST['instructor_'])) {
			$secInstr = $_POST['instructor_'];
		} else {
			$secInstr = "";
		}

		if ($secId > 0 && strlen($secNumb) > 0 && strlen($secSem) > 0 && strlen($secYear) > 0 && strlen($secInstr) > 0) {
			$fields = "(section_identifier, course_number, semester, myyear, instructor)";
			$values = "($secId, '$secNumb', '$secSem', '$secYear', '$secInstr')";

			$query = "INSERT INTO mysection " . $fields . " VALUES " . $values;
			mysql_query($query);
		}
		break;
	
	case 3:
		if (isset($_POST['student_number_'])) {
			$grsnumb = intval($_POST['student_number_']);
		} else {
			$grsnumb = 0;
		}

		if (isset($_POST['section_identifier_'])) {
			$grsId = intval($_POST['section_identifier_']);
		} else {
			$grsId = 0;
		}

		if (isset($_POST['grade_'])) {
			$grGrade = $_POST['grade_'];
		} else {
			$grGrade = "";
		}

		if ($grsnumb > 0 && $grsId > 0 && strlen($grGrade) > 0) {
			$fields = "(student_number, section_identifier, grade)";
			$values = "('$grsnumb', $grsId, '$grGrade')";

			$query = "INSERT INTO grade_report " . $fields . " VALUES " . $values;
			mysql_query($query);
		}
		break;
	
	case 4:
		if (isset($_POST['course_number_'])) {
			$pcNumb = $_POST['course_number_'];
		} else {
			$pcNumb = "";
		}

		if (isset($_POST['prerequisite_number_'])) {
			$ppNumb = $_POST['prerequisite_number_'];
		} else {
			$ppNumb = "";
		}

		if (strlen($pcNumb) > 0 && strlen($ppNumb) > 0) {
			$fields = "(course_number, prerequisite_number)";
			$values = "('$pcNumb', '$ppNumb')";

			$query = "INSERT INTO prerequisite " . $fields . " VALUES " . $values;
			mysql_query($query);
		}
		break;
	
	case 5:
		if (isset($_POST['course_number_'])) {
			$pcNumb = $_POST['course_number_'];
		} else {
			$pcNumb = "";
		}

		if (isset($_POST['prerequisite_number_'])) {
			$ppNumb = $_POST['prerequisite_number_'];
		} else {
			$ppNumb = "";
		}

		if (strlen($pcNumb) > 0 && strlen($ppNumb) > 0) {
			$fields = "(course_number, prerequisite_number)";
			$values = "('$pcNumb', '$ppNumb')";

			$query = "INSERT INTO prerequisite " . $fields . " VALUES " . $values;
			mysql_query($query);
		}
		break;
		
	case 6:
		if (isset($_POST['course_number_'])) {
			$pcNumb = $_POST['course_number_'];
		} else {
			$pcNumb = "";
		}

		if (isset($_POST['prerequisite_number_'])) {
			$ppNumb = $_POST['prerequisite_number_'];
		} else {
			$ppNumb = "";
		}

		if (strlen($pcNumb) > 0 && strlen($ppNumb) > 0) {
			$fields = "(course_number, prerequisite_number)";
			$values = "('$pcNumb', '$ppNumb')";

			$query = "INSERT INTO prerequisite " . $fields . " VALUES " . $values;
			mysql_query($query);
		}
		break;
	
	default:
		break;
}

mysql_close($con);

header('Location: index.php?mn=' . $mn);
?>

