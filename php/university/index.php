<?php
if (isset($_GET['mn'])) {
    $mn = intval($_GET['mn']);
} else {
    $mn = 0;
}

if (isset($_GET['tr'])) {
    $tr = intval($_GET['tr']);
} else {
    $tr = 0;
}

$dbhost = "localhost";
$dbuser = "root";
$dbpassword = "";
$dbname = "university";

$con = mysql_connect($dbhost, $dbuser, $dbpassword);

if (!$con) {
    die('Could not connect: ' . mysql_error());
}

mysql_select_db($dbname, $con);

$tblArr = array();
$tblArr[] = "student";
$tblArr[] = "course";
$tblArr[] = "mysection";
$tblArr[] = "grade_report";
$tblArr[] = "prerequisite";

$table_name = $tblArr[$mn];

$sql = "SHOW COLUMNS FROM $table_name";
$result = mysql_query($sql);

while ($record = mysql_fetch_array($result)) {
    $fields[] = $record['0'];
}

// Array outputs
$headArr = array();
$optArr = array();

$headArr[] = "Home";
$headArr[] = "Transcript";

$optArr[] = "Student";
$optArr[] = "Course";
$optArr[] = "Section";
$optArr[] = "Grade Report";
$optArr[] = "Prerequisite";

$data2dArr = array();
$dataInput = array();

$query = "SELECT * FROM  $table_name";
$result = mysql_query($query);

while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    $i = 0;
    foreach ($line as $col_value) {
        $data2dArr[$i][] = $col_value;
        $i++;
    }
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>University</title>
		<link rel="stylesheet" type="text/css" href="css/site.css"/>
    </head>
	<body background="images/Wenjieton.gif">
	<body>
		<div class="container">
			<table>
				<tr>
					<?php
					for ($i = 0; $i < count($headArr); $i++) {
						?>
						<td style="width: 7em">
							<?php
							if ($tr == $i) {
								?>
								<b><?php print $headArr[$i]; ?></b>
								<?php
							} else {
								?>
								<a href="transcriptHandler.php?tr=<?php print $i; ?>">
									<?php print $headArr[$i]; ?>
								</a>
								<?php
							}
							?>
						</td>
						<?php
					}
					?>
				</tr>
			</table>
			<hr />
			<table>
				<tr>
					<?php
					for ($i = 0; $i < count($optArr); $i++) {
						?>
						<td style="width: 7em">
							<?php
							if ($mn == $i) {
								?>
								<b><?php print $optArr[$i]; ?></b>
								<?php
							} else {
								?>
								<a href="index.php?mn=<?php print $i; ?>">
									<?php print $optArr[$i]; ?>
								</a>
								<?php
							}
							?>
						</td>
						<?php
					}
					?>
				</tr>
			</table>
			<hr />
			<table>
				<tr>
					<?php
					for ($i = 0; $i < count($fields); $i++) {
						?>
						<th style="width: 8em"><?php print $fields[$i]; ?></th>
							<?php
						}
						?>
				</tr>
				<?php
				for ($j = 0; $j < count($data2dArr[0]); $j++) {
					?>
					<tr>
						<?php
						for ($k = 0; $k < count($fields); $k++) {
							?>
							<td id="name_<?php print ($j+1) . '_' . ($k+1); ?>"><?php print $data2dArr[$k][$j]; ?></td>
							<?php
						}
						?>
						<td id="namebtn_<?php print $j+1; ?>"><input type="button" onclick="editThisNameAjax(<?php echo json_encode($j+1); ?>)" value="Edit" /></td>
					</tr>
					<?php
				}
				?>
			</table>
			<hr/>
				
			<div id="errMsg">&nbsp;</div>
			<form action="dataHandler.php?mn=<?php print $mn; ?>" method="POST" onsubmit="return validateForm();">
				<?php
				for ($j = 0; $j < count($fields); $j++) {
					?>
					<div class="row">
						<input id="<?php echo $fields[$j] . "_";?>" type="text" name="<?php echo $fields[$j] . "_";?>"></input>
					</div>
					<?php
				}
				?>
				<td><input type="submit" value="Add"></input><td>
			</form>
		</div>
    </body>
</html>

<script>
	var xmlhttp;
	var data = JSON.parse('<?php echo json_encode( $data2dArr ) ?>');
	var field = JSON.parse('<?php echo json_encode( $fields ) ?>');
	
	function loadXMLDoc(url, cfunc) { // Connection using ajax
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = cfunc;
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}

	function editThisNameAjax(row) {
		var val = [];
		for(var col = 1; col < field.length + 1; col++){
			val[col - 1] = document.getElementById("name_" + row + "_" + col).innerHTML;
		}

		var myurl = "ajax/editThisNameAjax.php?mn=" + <?php echo $mn; ?> + "&val=" + val;
		
		loadXMLDoc(myurl, function () {
			if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
				result = xmlhttp.responseText;
				var pstr = [];
				pstr = result.split(",");
				
				for(var col = 1; col < data.length+1; col++){
					// Text Field
					document.getElementById("name_" + row + "_" + col).innerHTML = 
						"<input id=\"newname_" + row + "_" + col +"\" type=\"text\" value=\"" + pstr[col - 1] + "\" />";
				}
				// Button
				document.getElementById("namebtn_" + row).innerHTML = 
					"<input type=\"button\" onclick=\"updateThisNameAjax(" + row + ")\" value=\"Update\" />";
			}
		});
	}
	
	function updateThisNameAjax(row) {
		var val = [];
		for(var col = 1; col < data.length+1; col++){
			val[col - 1] = document.getElementById("newname_" + row + "_" + col).value;
			if(validate(val[col - 1], col - 1) === false){
				return false;
			}
		}
		var myurl = "ajax/updateThisNameAjax.php?mn=" + <?php echo $mn; ?> + "&val=" + val;

		loadXMLDoc(myurl, function () {
			if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
				result = xmlhttp.responseText;
				console.log(result);
				var pstr = [];
				pstr = result.split(",");
				
				for(var col = 1; col < data.length+1; col++){
					// Text Field
					document.getElementById("name_" + row + "_" + col).innerHTML = pstr[col - 1];
				}
				document.getElementById("namebtn_" + row).innerHTML = "<input type=\"button\" onclick=\"editThisNameAjax(" + row + ")\" value=\"Edit\" />";
			}
		});
	}

	function validate(str, x){
		if(str == null || str == ""){
			document.getElementById("errMsg").innerHTML = "Please fill in all fields!";
			return false;
		}
		else if(field[x] == "student_number" && isNaN(str)){
			document.getElementById("errMsg").innerHTML = "Please use an integer for " + field[x] + "!";
			return false;
		}
		else if(field[x] == "myclass" && isNaN(str)){
			document.getElementById("errMsg").innerHTML = "Please use an integer for " + field[x] + "!";
			return false;
		}
		else if(field[x] == "credit_hours" && isNaN(str)){
			document.getElementById("errMsg").innerHTML = "Please use an integer for " + field[x] + "!";
			return false;
		}
		else if(field[x] == "section_identifier" && isNaN(str)){
			document.getElementById("errMsg").innerHTML = "Please use an integer for " + field[x] + "!";
			return false;
		}
		else if(field[x] == "myyear" && isNaN(str)){
			document.getElementById("errMsg").innerHTML = "Please use an integer for " + field[x] + "!";
			return false;
		}
	}
	
	function validateForm(){
		var temp;
		for(var j = 0; j < field.length; j++){
			temp = document.getElementById(field[j] + '_').value;
			if(validate(temp, j) === false){
				return false;
			}
		}
	}

</script>

<?php
mysql_close($con);
?>