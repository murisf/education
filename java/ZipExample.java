package corejava.zip;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipExample {

	public static void main(String[] args) {

		try {
			ZipInputStream zin = new ZipInputStream(new FileInputStream(
					"zipdb.zip"));
			
			ZipEntry entry;
			
			while ((entry = zin.getNextEntry()) != null) {
				System.out.println(entry.getName());
				
				Scanner in = new Scanner(zin);
				
				while (in.hasNextLine()) {
					System.out.println(in.nextLine());
				}
				
				zin.closeEntry();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
