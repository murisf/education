package corejava.xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlTest {

	public static void main(String[] args) {

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document doc = builder.parse(new File("sample.xml"));

			Element root = doc.getDocumentElement();

			System.out.println(root.getTagName());
			
			NodeList children = root.getChildNodes();
			
			for (int i = 0; i < children.getLength(); i++) {
				Node child = children.item(i);
				
				if (child instanceof Element) {
					Element childElement = (Element) child;
					System.out.println(childElement.getTagName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
