
public class TestAccount {
    public static void main(String[] args) {
        Account acct1 = new Account();
        System.out.println(acct1.getDate());
    }
}

class Account {
    private int id = 0;
    private double balance = 0;
    private double annualInterestRate = 0;
    private java.util.Date date = new java.util.Date();
    private String dateCreated = date.toString();
    
    Account (){
    }
    
    Account (int newId, double newBalance) {
        id = newId;
        balance = newBalance;
    }
    
    public int getId(){
        return id;
    }
    public double getBalance(){
        return balance;
    }
    public double getAnnualInterestRate(){
        return annualInterestRate;
    }
    public String getDate(){
        return dateCreated;
    }
    public void setId(int newId){
        id = newId;
    }
    public void setBalance(double newBalance){
        balance = newBalance;
    }
    public void setAnnualInterestRate(double an_IR){
        annualInterestRate = an_IR;
    }
    
}
