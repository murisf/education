package corejava.networking;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class URLConnectionTest {

	public static void main(String[] args) {
		
		try {
			String urlName = "http://horstmann.com";
			URL url = new URL(urlName);
			URLConnection connection = url.openConnection();
			
			connection.connect();
			
			Scanner in = new Scanner(connection.getInputStream());
			
			while (in.hasNextLine()) {
				System.out.println(in.nextLine());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
