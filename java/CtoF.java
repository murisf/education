//Muris Fazlic
//cs2261 prog2.1
//02/13/2013

import javax.swing.JOptionPane;

public class CtoF {
    public static void main(String[] args) {
        String string = JOptionPane.showInputDialog(null, 
                "Enter Celsius degrees",
                "Celsius to Fahrenheit Conversion",
                JOptionPane.QUESTION_MESSAGE);
        
        double celsius = Double.parseDouble(string);
        double fahrenheit = (9.0/5) * celsius + 32;
        
        System.out.println("You entered " + celsius + " degrees Celsisu which" +
                " is " + fahrenheit + " in Fahrenheit degrees ");
        
        String message = "You entered " + celsius + " degrees Celsisu which" +
                " is " + fahrenheit + " in Fahrenheit degrees ";
        JOptionPane.showMessageDialog(null, message);
    }
}
