// Muris Fazlic
// cs2261 prog1.2
// 14/02/2013
import javax.swing.JOptionPane;

public class QuadEq {
    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "I will calculate the result" + 
                " of the quadratic equation", 
                "Quadratic Equation.", JOptionPane.INFORMATION_MESSAGE);
        String a = JOptionPane.showInputDialog(null, "Enter value for a", 
                "Value of a", JOptionPane.QUESTION_MESSAGE);
        String b = JOptionPane.showInputDialog(null, "Enter value for b", 
                "Value of b", JOptionPane.QUESTION_MESSAGE);
        String c = JOptionPane.showInputDialog(null, "Enter value for c",
                "Value of c", JOptionPane.QUESTION_MESSAGE);
        
        double num_a = Double.parseDouble(a);
        double num_b = Double.parseDouble(b);
        double num_c = Double.parseDouble(c);
        
        double discriminant = num_b * num_b - (4 * num_a * num_c);
        
        if (discriminant > 0) {
            double result1 = (-1 * num_b + Math.pow(discriminant, 0.5)) / 2 * num_a;
            double result2 = (-1 * num_b - Math.pow(discriminant, 0.5)) / 2 * num_a;
            JOptionPane.showMessageDialog(null, "The roots are " + 
                    result1 + " and " + result2, "Result", JOptionPane.INFORMATION_MESSAGE);
            }
        else if (discriminant == 0) {
            double result = (-1 * num_b + Math.pow(discriminant, 0.5)) / 2 * num_a;
            JOptionPane.showMessageDialog(null, "The root is " + result, 
                "Result", JOptionPane.INFORMATION_MESSAGE);
            }
        else {
            JOptionPane.showMessageDialog(null, "The equation has no real roots", 
                "Result", JOptionPane.INFORMATION_MESSAGE);
            }
    }
}
