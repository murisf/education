
public class TestRectangle {
    public static void main(String[] args) {
        
        Rectangle rec1 = new Rectangle(40,4);
        Rectangle rec2 = new Rectangle(35.9,3.5);
        
        System.out.println("Rectangle 1 dimensions are:");
        System.out.println("Width: " + rec1.width);
        System.out.println("Height: " + rec1.height);
        System.out.println("Area: " + rec1.getArea());
        System.out.println("Perimeter: " + rec1.getPerimeter());
        
        System.out.println("Rectangle 2 dimensions are:");
        System.out.printf("Width: %.2f", rec2.width);
        System.out.println();
        System.out.printf("Height: %.2f", rec2.height);
        System.out.println();
        System.out.printf("Area: %.2f", rec2.getArea());
        System.out.println();
        System.out.printf("Perimeter: %.2f", rec2.getPerimeter());
        System.out.println();
                
    }
}

class Rectangle {
    double width = 1;
    double height = 1;
    
    Rectangle(){
    }
    
    Rectangle(double newHeight, double newWidth){
        height = newHeight;
        width = newWidth;
    }
    
    double getArea(){
        return height * width;
    }
    
    double getPerimeter() {
        return (2*height) + (2*width);
    }
}
