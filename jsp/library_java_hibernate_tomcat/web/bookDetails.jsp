<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Using Hibernate with Eclipse, Ant and Tomcat</title>
   </head>
   <body>

      <h1 align="center"><c:out value="${title}"/></h1>
      <hr>
      <p>
      <form action="<c:out value="${action}"/>">
         <table align="center">
            <tr><td>Id:</td><td><input type="text" value="<c:out value="${book.id}"/>" maxlength="8" name="id" size="8" <c:out value="${isReadOnly}"/>></td></tr>
            <tr><td>Author firstname:</td><td><input value="<c:out value="${book.firstName}"/>" type="text" maxlength="24" name="firstname" size="24"></td></tr>
            <tr><td>Author surname:</td><td><input type="text" value="<c:out value="${book.surname}"/>" maxlength="24" name="surname" size="24"></td></tr>
            <tr><td>Book title:</td><td><input type="text" value="<c:out value="${book.title}"/>" maxlength="96" name="title" size="96"></td></tr>
            <tr><td>Price:</td><td><input type="text" value="<c:out value="${book.price}"/>" maxlength="8" name="price" size="8"></td></tr>
            <tr><td>Year:</td><td><input type="text" value="<c:out value="${book.myyear}"/>" maxlength="4" name="year" size="4"></td></tr>
            <tr><td>Inventory:</td><td><input type="text" value="<c:out value="${book.inventory}"/>" maxlength="4" name="inventory" size="4"></td></tr>
            <tr><td>Description:</td><td><input type="text" value="<c:out value="${book.description}"/>" maxlength="30" name="description" size="30"></td></tr>
         </table>
         <table align="center">
            <tr><td><input type="submit" value="Save" alt="S"></td><td><td><input type="reset" title="Reset" alt="R"></td></tr>
         </table>
      </form>
      <hr>
      <a href="<c:url value="/index.jsp"/>">Home</a>  <a href="<c:url value="/listBooks.htm"/>">List Books</a>
   
   </body>
</html>
