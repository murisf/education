<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Using Hibernate with Eclipse, Ant and Tomcat</title>
   </head>
   <body>

      <h1 align="center">List books</h1>
      <hr>
      <p>
      <form action="actionFromList.htm">
         <table align="center" bgcolor="#c6d7ff" border="1" cellpadding="2" cellspacing="0">
            <tr><th></th><th>Id</th><th>Author<br>firstname</th><th>Author<br>surname</th><th>Book title</th><th>Price</th><th>Year</th><th>Inventory</th><th>Description</th></tr>
            <c:forEach items="${bookList}" var="book">
               <tr>
                  <td><input type="radio" value="${book.id}" name="selected" onselect="document.bgColor=blue"/></td>
                  <td><c:out value="${book.id}"/></td>
                  <td><c:out value="${book.firstName}"/></td>
                  <td><c:out value="${book.surname}"/></td>
                  <td><c:out value="${book.title}"/></td>
                  <td align="right"><f:formatNumber value="${book.price}" pattern="####.00"/></td>
                  <td align="right"><c:out value="${book.myyear}"/></td>
                  <td align="right"><c:out value="${book.inventory}"/></td>
                  <td><c:out value="${book.description}"/></td>
               </tr>
            </c:forEach>
         </table>
         <table align="center">
            <tr><td><input type="submit" name="submit" value="Delete"></td><td><input name="submit" type="submit" value="Edit"></td><td><td><input type="reset" title="Reset"></td></tr>
         </table>
      </form>
      <hr>
      <a href="<c:url value="/index.jsp"/>">Home</a>  <a href="<c:url value="/createBook.htm"/>"> Create a book</a>
   
   </body>
</html>
