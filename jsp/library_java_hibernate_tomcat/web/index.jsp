<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Using Hibernate with Eclipse, Ant and Tomcat</title>
   </head>
   <body>

      <h1 align="center">Using Hibernate with Eclipse, Ant and Tomcat</h1>
      <hr>
      <p>
      <a href="<c:url value="/listBooks.htm"/>">List all books</a>
      <p>
      <a href="<c:url value="/createBook.htm"/>">Create a book</a>
      <p>   
      <p>   
      <hr>
   
   </body>
</html>
