package com.javux.cbook.servlet;


import java.io.*;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import com.javux.cbook.dao.BooksDAO;
import com.javux.cbook.dao.DatabaseException;
import com.javux.cbook.entity.Books;

public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 6723471178342776147L;
    private BooksDAO bookDAO;
    private static String DELETE_ACTION = "Delete";
    private static String EDIT_ACTION = "Edit";
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            String id = request.getRequestURI();
            id = id.substring(request.getContextPath().length());
            String page = null;
            if(id.startsWith("/listBooks.htm")){
                List<Books> list = bookDAO.listBooks();
                request.setAttribute("bookList", list);
                page = "/listBooks.jsp";
            } else if(id.startsWith("/createBook.htm")) {
                page = "/bookDetails.jsp";
                request.setAttribute("title", "Create Book");
                request.setAttribute("action", "saveBook.htm");
            } else if(id.startsWith("/saveBook.htm")) {
                Books book = new Books();
                book.setId(request.getParameter("id"));
                book.setFirstName(request.getParameter("firstname"));
                book.setSurname(request.getParameter("surname"));
                book.setTitle(request.getParameter("title"));
                book.setMyyear(new Integer(Integer.parseInt(request.getParameter("year"))));
                book.setPrice(Float.parseFloat(request.getParameter("price")));
                book.setInventory(Integer.parseInt(request.getParameter("inventory")));
                book.setDescription(request.getParameter("description"));
                bookDAO.saveBooks(book);
                page = "/listBooks.htm";
            } else if(id.startsWith("/updateBook.htm")) {
                Books book = bookDAO.getBook(request.getParameter("id"));
                book.setFirstName(request.getParameter("firstname"));
                book.setSurname(request.getParameter("surname"));
                book.setTitle(request.getParameter("title"));
                book.setMyyear(Integer.parseInt(request.getParameter("year")));
                book.setPrice(Float.parseFloat(request.getParameter("price")));
                book.setInventory(Integer.parseInt(request.getParameter("inventory")));
                book.setDescription(request.getParameter("description"));
                bookDAO.updateBook(book);
                page = "/listBooks.htm";
            } else if(id.startsWith("/actionFromList.htm")) {
                String action = request.getParameter("submit");
                if(action.equals(DELETE_ACTION) ==  true) {
                	bookDAO.deleteBook(request.getParameter("selected"));
                    page = "/listBooks.htm";
                } else if(action.equals(EDIT_ACTION) == true) {
                    String bookId = request.getParameter("selected");
                    if(id == null){
                        page = "/listBooks.htm";
                    } else {
                        Books book = bookDAO.getBook(bookId);
                        request.setAttribute("book", book);
                        request.setAttribute("title", "Edit Book");
                        request.setAttribute("action", "updateBook.htm");
                        request.setAttribute("isReadOnly", "readonly");
                        page = "/bookDetails.jsp";
                    }
                }
            }
            request.getRequestDispatcher(page).forward(request, response);
        } catch(DatabaseException he){
            throw new ServletException(he);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
    
    public void init() throws ServletException {
       
        bookDAO = new BooksDAO();
    }


}
