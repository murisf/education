package com.javux.cbook.entity;

public class Books implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String surname;
	private String firstName;
	private String title;
	private Float price;
	private Integer myyear;
	private String description;
	private Integer inventory;

	public Books() {
	}

	public Books(String id) {
		this.id = id;
	}

	public Books(String id, String surname, String firstName, String title,
			Float price, Integer myyear, String description, Integer inventory) {
		this.id = id;
		this.surname = surname;
		this.firstName = firstName;
		this.title = title;
		this.price = price;
		this.myyear = myyear;
		this.description = description;
		this.inventory = inventory;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Float getPrice() {
		return this.price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Integer getMyyear() {
		return this.myyear;
	}

	public void setMyyear(Integer myyear) {
		this.myyear = myyear;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getInventory() {
		return this.inventory;
	}

	public void setInventory(Integer inventory) {
		this.inventory = inventory;
	}

}
