package com.javux.cbook.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.javux.cbook.entity.Books;

public class BooksDAO extends DAO {
	
	public Books getBook(String id) throws DatabaseException {
		Books book = null;
		
		try {
			begin();
			book = (Books)getSession().load(Books.class, id);
			commit();
		} catch (HibernateException e) {
			rollback();
			throw new DatabaseException("Could not get book "
					+ id);
		}
		
		return book;
	}
	
	public List<Books> listBooks() throws DatabaseException {
		List<Books> blist = new ArrayList<Books>();
		try {
			begin();
			Query q = getSession().createQuery("from Books");
			List<?> qlist = q.list();
			
			for ( Object item : qlist ) {
				blist.add((Books)item);
			}
			commit();
			return blist;
		} catch (HibernateException e) {
			rollback();
			throw new DatabaseException("Could not list the books ");
		}
	}
	
	public void saveBooks(Books book) throws DatabaseException {
		try {
			begin();
			getSession().save(book);
			commit();
		} catch (HibernateException e) {
			rollback();
			throw new DatabaseException("Could not save book "
					+ book.getId());
		}
	}
	
	public void deleteBook(Books book) throws DatabaseException {
		try {
			begin();
			getSession().delete(book);
			commit();
		} catch (HibernateException e) {
			rollback();
			throw new DatabaseException("Could not delete book "
					+ book.getId());
		}
	}
	
	public void deleteBook(String id) throws DatabaseException {
		try {
			begin();
			Session mySession = getSession();
			Books book = (Books)mySession.load(Books.class, id);
			mySession.delete(book);
			commit();
		} catch (HibernateException e) {
			rollback();
			throw new DatabaseException("Could not delete book "
					+ id);
		}
	}
	
	public void updateBook(Books book) throws DatabaseException {
		try {
			begin();
			getSession().update(book);
			commit();
		} catch (HibernateException e) {
			rollback();
			throw new DatabaseException("Could not update book "
					+ book.getId());
		}
	}

}
