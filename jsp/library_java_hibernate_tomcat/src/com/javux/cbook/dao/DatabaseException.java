package com.javux.cbook.dao;

public class DatabaseException extends Exception{
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of DatabaseException */
    public DatabaseException(String msg) {
        super(msg);
    }

    public DatabaseException(Exception e) {
        super(e);
    }
    
}

