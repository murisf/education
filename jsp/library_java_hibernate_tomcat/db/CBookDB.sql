Drop TABLE if exists books;

CREATE TABLE books(
     id varchar (8) NOT NULL,
	 surname varchar (24),
	 first_name varchar (24),
	 title varchar (96),
	 price float,
     myyear int,
	 description varchar (30),
     inventory int,
     primary key (id)
);

INSERT INTO books (id, surname, first_name, title, price, myyear, description, inventory)
     VALUES('201', 'Duke', '', 'My Early Years: Growing up on *7',
	    30.75, 1995, 'What a cool book.', 20)
;
INSERT INTO books (id, surname, first_name, title, price, myyear, description, inventory)
     VALUES('202', 'Jeeves', '', 'Web Servers for Fun and Profit', 40.75,
	    2000, 'What a cool book.', 20)
;
INSERT INTO books (id, surname, first_name, title, price, myyear, description, inventory)
     VALUES('203', 'Masterson', 'Webster', 'Web Components for Web Developers',
	    27.75, 2000, 'What a cool book.', 20)
;
INSERT INTO books (id, surname, first_name, title, price, myyear, description, inventory)
     VALUES('205', 'Novation', 'Kevin', 'From Oak to Java: The Revolution of a Language',
	    10.75, 1998, 'What a cool book.', 20)
;
INSERT INTO books (id, surname, first_name, title, price, myyear, description, inventory)
     VALUES('206', 'Gosling', 'James', 'Java Intermediate Bytecodes', 30.95,
	    2000, 'What a cool book.', 20)
;
INSERT INTO books (id, surname, first_name, title, price, myyear, description, inventory)
     VALUES('207', 'Thrilled', 'Ben', 'The Green Project: Programming for Consumer Devices',
	    30.00, 1998, 'What a cool book', 20)
;
INSERT INTO books (id, surname, first_name, title, price, myyear, description, inventory)
     VALUES('208', 'Tru', 'Itzal', 'Duke: A Biography of the Java Evangelist',
	    45.00, 2001, 'What a cool book.', 20)
;