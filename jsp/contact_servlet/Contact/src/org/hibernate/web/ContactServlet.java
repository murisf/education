package org.hibernate.web;

import java.io.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.*;
import org.hibernate.web.Contact;
import org.hibernate.cfg.Configuration;

@WebServlet("/ContactServlet")
public class ContactServlet extends javax.servlet.http.HttpServlet {
	
	private static final long serialVersionUID = 1L;
	Session session = null;

	public ContactServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		// send XHTML page to client

		// start XHTML document
		out.println("<?xml version = \"1.0\"?>");

		out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD "
				+ "XHTML 1.0 Strict//EN\" \"http://www.w3.org"
				+ "/TR/xhtml1/DTD/xhtml1-strict.dtd\">");

		out.println("<html xmlns = \"http://www.w3.org/1999/xhtml\">");

		// head section of document
		out.println("<head>");
		out.println("<title>A Simple Servlet Example</title>");
		out.println("</head>");

		// body section of document
		out.println("<body>");
		out.println("<h1>Welcome to Servlets!</h1>");
		out.println("<p>");
		out.println("Inserting Record<b>...</b>");

		try {
			// This step will read hibernate.cfg.xml and prepare hibernate for
			// use
			SessionFactory sessionFactory = new Configuration().configure()
					.buildSessionFactory();
			session = sessionFactory.openSession();
			// Create new instance of Contact and set values in it by reading
			// them from form object
			System.out.println("Inserting Record");
			Contact contact = new Contact();
			// contact.setId(6);
			contact.setFirstname("a");
			contact.setLastname("b");
			contact.setEmail("c");

			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				session.save(contact);
				tx.commit();
			} catch (HibernateException e) {
				e.printStackTrace();
				if (tx != null && tx.isActive())
					tx.rollback();
			}

			System.out.println("Done");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			// Actual contact insertion will happen at this step
			session.flush();
			session.close();

		}

		out.println("<p>");
		out.println("Done");
		out.println("</body>");

		// end XHTML document
		out.println("</html>");
		out.close(); // close stream to complete the page

	}
}