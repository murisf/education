DROP TABLE IF EXISTS contact;
CREATE TABLE contact (
  id int(10) unsigned NOT NULL auto_increment,
  firstname varchar(30) default NULL,
  lastname varchar(30) default NULL,
  email varchar(40) default NULL,
  PRIMARY KEY  (id)
);