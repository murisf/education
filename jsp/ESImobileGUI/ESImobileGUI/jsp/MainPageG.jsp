<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main Page - Members - Express-Scripts</title>
        <link rel = "stylesheet" href = "css/iphoneMain.css" type = "text/css" />
        <style type="text/css">
            .userheight { top: 132px; }
            .menuleft { left: -15px; }
        </style>
        <!--[if IE]>
           <style type="text/css">
              .userheight { top: 145px; }
              .menuleft { left: 25px; }
           </style>
        <![endif]-->
    </head>
    <body>
        <div id="outframe">
            <img id="logo" src="images/esiLogoStacked.gif" alt="ESI Logo" />
            <p style="font-weight:bold" class="baseFont1 userheight" id="titleLogin">Hello, Javux</p>
            <input id="imglogoff" type="image" src="images/bttnLogOff.gif" name="btnLogOff" width="74" height="22" BORDER="0" ALT="Log Off">
            <a id="homeLoc" class="baseFont0" href="#">home</a>
            <a id="faqLoc" class="baseFont0" href="#">faq</a>
            <a id="contactLoc" class="baseFont0" href="#">contact us</a>
            <a id="cartlink" class="baseFont0" href="#">shopping cart</a>
            <img id="imgcart" src="images/cart.gif" alt="Cart" />
            <span id="lastvisit" class="baseFont1 italic">Last time visited this site:</span>
            <span id="lastvisittime" class="baseFont1 italic" >Sat., Aug 9, 2008</span>
            
            <div id="inframe">
                <img id="inframebg" src="images/inframebg.gif" alt="background" />
                <img id="getstarted" src="images/getstarted.gif" alt="Get Started" />
                <span style="color:black" class="baseFont0 left" id="getstarteditem1">
                    <li> <a href="#">Learn about Home Delivery</a>
                    </li>
                </span>
                <span style="color:black" class="baseFont0 left" id="getstarteditem2">
                    <li> <a href="#">Switch Your Prescriptions to</a> <br/>
                        <!--[if IE]>&nbsp;&nbsp;&nbsp;<![endif]--> <a href="#">Home Delivery</a>
                    </li>
                </span>
                <span style="color:black" class="baseFont0 left" id="getstarteditem3">
                    <li> <a href="#">Fill a New Prescription</a>
                    </li>
                </span>
                
            </div>
            
            <div class='tabsPos' id='header'>
                <ul>
                    <li class='baseFont1' id='selected'><a href='#'>Get Started</a></li>
                    <li class='baseFont'><a href='MainPageA.jsp'>Already Using</a></li>
                    <li class='baseFont'><a href='MainPageC.jsp'>Can't See</a></li>
                </ul>
            </div>
            
            <div id="uprightframe">
                <img id="presarrow" src="images/arrow.gif" alt="arrow1" />
                <img id="presplanarrow" src="images/arrow.gif" alt="arrow2" />
                <img id="drugarrow" src="images/arrow.gif" alt="arrow3" />
                <img id="accountarrow" src="images/arrow.gif" alt="arrow4" />
                
                <div class="menu">
                    
                    <ul id="accountSettingsLoc" class="menuleft">
                        <li><a class="topmenu" href="#">Account Settings<!--[if gt IE 6]><!--></a><!--<![endif]--><!--[if lt IE 7]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                            <ul>
                                <li><a class="submenu left" href="#">&nbsp;Overview</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Patient Information</a></li>
                                <li><a class="submenu left" href="#">&nbsp;E-Mail Settings</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Security Settings</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Change Username</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Change Password</a></li>
                            </ul>
                            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
                        </li>
                    </ul>
                    
                </div>
                
                <div class="menu">
                    
                    <ul id="drugHealthLoc" class="menuleft">
                        <li><a class="topmenu" href="#">Drug &amp; Health Guide<!--[if gt IE 6]><!--></a><!--<![endif]--><!--[if lt IE 7]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                            <ul>
                                <li><a class="submenu left" href="#">&nbsp;Overview</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Drug Library</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Herbs &amp; Supplements</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Drug &amp; News</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Health Conditions &amp; 
                                <br/>&nbsp;&nbsp;=&gt; Treatment Options</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Health Check</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Medical Dictionary</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Senior Corner</a></li>
                            </ul>
                            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
                        </li>
                    </ul>
                    
                </div>
                
                <div class="menu">
                    
                    <ul id="myPresPlanLoc" class="menuleft">
                        <li><a class="topmenu" href="#">My Prescription Plan<!--[if gt IE 6]><!--></a><!--<![endif]--><!--[if lt IE 7]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                            <ul>
                                <li><a class="submenu left" href="#">&nbsp;Overview</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Price a Drug</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Coverage &amp; Copayments</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Drugs Preferred by My Plan</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Find a Pharmacy</a></li>
                                <li><a class="submenu left" href="#">&nbsp;For Your Doctor Visit</a></li>
                            </ul>
                            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
                        </li>
                    </ul>
                    
                </div>
                
                <div class="menu">
                    
                    <ul id="myPrescriptionLoc" class="menuleft">
                        <li><a class="topmenu" href="#">My Prescription<!--[if gt IE 6]><!--></a><!--<![endif]--><!--[if lt IE 7]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                            <ul>
                                <li><a class="submenu left" href="#">&nbsp;Overview</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Order Refills</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Renew Prescriptions</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Fill a New Prescription</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Check Order Status</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Request Center</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Save on My Prescriptions</a></li>
                                <li><a class="submenu left" href="#">&nbsp;Prescription History</a></li>
                            </ul>
                            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
                        </li>
                    </ul>
                    
                </div>
                
            </div>
            
            <input id="imgsave" type="image" src="images/saveOnMyPrescriptions.gif" name="btnSavePres" width="161" height="46" BORDER="0" ALT="Save Prescription">
            <input id="imgprice" type="image" src="images/callPrice.gif" name="btnPriceComp" width="161" height="46" BORDER="0" ALT="Price Comparison">
        </div>
    </body>
</html>