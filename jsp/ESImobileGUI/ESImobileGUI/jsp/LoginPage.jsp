<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign In - Members - Express-Scripts</title>
        <link rel = "stylesheet" href = "css/iphoneLogin.css" type = "text/css" />
        <style type="text/css">
            .titleheight { top: -10px; }
        </style>
        <!--[if IE]>
           <style type="text/css">
              .titleheight { top: 10px; }
           </style>
        <![endif]-->
    </head>
    <body>
        <div class = "outframe">
            <img class="logo" src="images/esiLogo.gif" alt="ESI Logo" />
            <div class = "inframe">
                <p class = "titleLogin titleheight bigFont">Access Your Account</p>
                <img id="inframebg" src="images/inframebg.gif" alt="background" />
                
                <form action="LoginServlet" method="POST">
                    <table class='tableLogin baseFont sep' width='200' border="0">
                        <tr>
                            <td class='center' width='80'>Username: </td>
                            <td class='left' width='120'><input type='text' name='username' size='17' /></td>
                        </tr>
                        <tr>
                            <td class='center' width='80'>Password: </td>
                            <td class='left' width='120'><input type='password' name='password' size='17' /></td>
                        </tr>
                    </table>
                    <input class='imgButton' type="image" src="images/btnSignIn.gif" name="imageButton" width="100" height="25" BORDER="0" ALT="Submit Form">
                </form>
                
                <a class="baseFont2 forgotuname" href="#">Forgot your username?</a>
                <a class="baseFont2 forgotpword" href="#">Forgot your password?</a>
                <p class="new2esi baseFont">New to Express-Scripts.com?</p>
                <a class="baseFont2 activateAct bold" href="#">Activate your account.</a>
                
                <%
                String login = (String) request.getAttribute("login");
            if (login != null) {
                if (login.equals("fail")) {
                %>
                <span class='errormsg'>Invalid Username or Password</span> 
                <span class='errormsg2'>Have you <a href="#">activate your account</a>?</span>  
                <%                }
            }
                %>
                
                                
            </div>
        </div>
    </body>
</html>