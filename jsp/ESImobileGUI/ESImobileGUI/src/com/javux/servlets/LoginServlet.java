package com.javux.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {
   
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
        
          String username = request.getParameter("username");
          String password = request.getParameter("password");
          RequestDispatcher dispatcher = request.getRequestDispatcher("LoginPage.jsp");
          int errorcode = 0;
       
          if ( username == null || password == null ) {
             errorcode = 1;
          } else if ( username.length() == 0 || password.length() == 0 ) {
             errorcode = 1;
          } else if ( username.equals("javux") && password.equals("javux") ) {
        } else {
             errorcode = 1;
          }
      
        if ( errorcode == 0 ) {
             response.sendRedirect("MainPageG.jsp");
          } else {
             request.setAttribute("login", "fail");
             dispatcher.forward(request, response);
          }
 }
}
