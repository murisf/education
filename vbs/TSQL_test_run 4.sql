--prog1sqld31 database

select * from sys.tables
select * from sys.procedures

select * from employee

create procedure emplist as select * from employee

execute emplist

sp_helptext emplist

alter procedure emplist as select * from employee order by empid

drop procedure emplist

--variables

create procedure mymessage
as
declare @msg char(12)
set @msg = 'hello world'
print @msg

mymessage


alter procedure mymessage
as
declare @msg char(12)
set @msg = 'hello world'
print 'do i have you attention?'
print 'today''s message is: ' + @msg

create procedure divide
as
declare @numerator int, @denominator int, @answer int, @msg varchar(20)
select @numerator = 12, @denominator = 3, @answer = @numerator/@denominator,
@msg = 'the answer is: ' + convert(char(3),@answer)
print @msg

--no error when altering the procedure
--but throws an error when running it due to conversion failure
alter procedure divide
as
declare @numerator int, @denominator int, @answer int, @msg varchar(20)
select @numerator = 12, @denominator = 3, @answer = @numerator/@denominator,
@msg = 'the answer is: ' + @answer
print @msg

divide

alter procedure divide
as
declare @numerator int, @denominator int, @answer int, @msg varchar(20)
select @numerator = 12, @denominator = 3, @answer = @numerator/@denominator,
@msg = 'the answer is: ' + convert(char(3),@answer)
print @msg

DIVIDE

--parameter variables
create procedure param_msg
	@firstname varchar(25)
as
	declare @msg varchar(60)
	set @msg = 'Hello, ' + @firstname + '!'
	print @msg

param_msg 'bubba smith'


--multiple parameter values
alter procedure param_msg
	@firstname varchar(25), @lastname varchar(25)
as
	declare @msg varchar(60)
	set @msg = 'Hello, ' + @firstname + ' ' + @lastname + '!'
	print @msg

param_msg bill ,barry

param_msg @lastname = barry, @firstname = bill

--specify a default value for the parameters
alter procedure param_msg
	@firstname varchar(25) = 'John', @lastname varchar(25)='Doe'
as
	declare @msg varchar(60)
	set @msg = 'Hello, ' + @firstname + ' ' + @lastname + '!'
	print @msg

param_msg

--************************************************************************
--just a test
alter procedure param_msg
	@firstname varchar(25) = null, @lastname varchar(25)= null
as
	declare @msg varchar(60)
	set @msg = 'Hello, ' + @firstname + ' ' + @lastname + '!'
	print @msg

/*
note concatenatin NULL to a string results in a NULL overall
use commands below to fix - but may not be around in future SQL versions
*/
set concat_null_yields_null off
set concat_null_yields_null on
--************************************************************************

--modify divide procedure 
alter procedure divide
	@numerator int, @denominator int
as
declare @answer int, @msg varchar(20)
set @answer = @numerator/@denominator
set @msg = 'the answer is: ' + convert(char(3),@answer)
print @msg

DIVIDE 10,5 --2

alter procedure divide
	@numerator int, @denominator int
as
declare @answer numeric(5,2), @msg varchar(20)
set @answer = @numerator/@denominator
set @msg = 'the answer is: ' + convert(char(5),@answer)
print @msg

-- 12/5 = 2.4

--test test test test test test test test test
--this works with float type
alter procedure divide
	@numerator float, @denominator float
as
declare @answer numeric(5,2), @msg varchar(20)
set @answer = @numerator/@denominator
set @msg = 'the answer is: ' + convert(char(5),@answer)
print @msg
divide 12.0, 5.0
--test test test test test test test test test

--Exercise 1

create procedure meminfo
@memid char(5)
as
	select * from members
	where mem_id = @memid

meminfo ac135

--edit procedure to capture column values in variables
alter procedure meminfo
@memid char(5)
as
	declare @fname varchar(15), @lname char(15), @memyear char(4)
	select		@fname = firstname,
				@lname = lastname ,
				@memyear = datepart(yyyy, memdate)
	from members
	where mem_id = @memid
print 'Member name: ' + @fname + ' ' + @lname
print 'Member since: ' + @memyear

--test
meminfo ac135

select  * from memtype
select  * from members

alter procedure meminfo
@memid char(5)
as
	declare @fname varchar(15), @lname char(15), @memyear char(4), @mdues numeric(5,2)
	select		@fname = firstname,
				@lname = lastname ,
				@memyear = datepart(yyyy, memdate),
				@mdues = dues
	from members inner join memtype
		on members.memcode = memtype.memcode
	where mem_id = @memid
print 'Member name: ' + @fname + ' ' + @lname
print 'Member since: ' + @memyear
print 'Membership dues: $' + convert(char(6), @mdues)

--test
meminfo ac135

--manipulating data
create procedure hard_insert
as
insert into memtype values ('DN', 'DONOR',0)

hard_insert

create procedure param_insert
	@mcode char(2), @mdescrip varchar(10) = null, @mdues numeric(5,2) = null
as
	insert into memtype		
	values (@mcode, @mdescrip, @mdues)
	select * from memtype
	where memcode = @mcode

--full row insert
param_insert AR, ARTIST, 10.95

--test default values
param_insert AA

sp_help members

--updating data


create procedure update_dues
	@incr numeric(5,2) = .1
as
	update memtype
	set dues = dues * (1 + @incr)

select  * from memtype

update_dues .05

--deleting rows
 create procedure delete_members
	@mcode char(2) = null --make this NULL as a safety
as
	delete from members
	where memcode = @mcode

--test
select  * from members
where memcode =  'ch'

delete_members sr

-- @@ROWCOUNT
alter procedure delete_members
	@mcode char(2) = null --make this NULL as a safety
as
	delete from members
	where memcode = @mcode
	print 'Number of deleted members: ' + convert(char(3), @@rowcount)

delete_members ch

--Exercise 2

--IF statements

create procedure simple_if
	@num1 int = 0, @num2 int = 0
as
	if @num1 > @num2
		print 'number 1 is greater than number 2'
		print 'thanks for playing'

--this gets both messages
simple_if 2,1

--this gets just 'thanks for playing'
simple_if 1,2

alter procedure simple_if
	@num1 int = 0, @num2 int = 0
as
	if @num1 > @num2
		begin
		print 'number 1 is greater than number 2'
		print 'you''re so smart'
		end
		print 'thanks for playing'

--this gets both messages
simple_if 2,1

--this gets just 'thanks for playing'
simple_if 1,2



create procedure if_else
	@num1 int = 0, @num2 int = 0
as
	if @num1 > @num2
		begin
			print 'number 1 is greater than number 2'
			print 'you''re so smart'
		end
	else
		begin
			print 'number 1 is NOT greater than number 2'
			print 'please try again'
		end
	print 'thanks for playing'

--this gets both messages
if_else 2,1

--this gets just 'thanks for playing'
if_else 1,2

create procedure multiple_ifs
	@num1 int = 0, @num2 int = 0
as
	if @num1 > @num2
		begin
			print 'number 1 is greater than number 2'
			print 'you''re so smart'
		end
	else
		if @num1 < @num2
			begin
				print 'number 1 is less than number 2'
				print 'please try again'
			end
		else
			begin
				print 'numbers are equal'
				print 'being tricky'
			end
	print 'thanks for playing'

--this gets both messages
multiple_ifs 2,1

--this gets just 'thanks for playing'
multiple_ifs 1,2

--third choice
multiple_ifs 2,2



--demo this
multiple_ifs 1, null

alter procedure multiple_ifs
	@num1 int = 0, @num2 int = 0
as
	if @num1 > @num2
		begin
			print 'number 1 is greater than number 2'
			print 'you''re so smart'
		end
	else
		if @num1 < @num2
			begin
				print 'number 1 is less than number 2'
				print 'please try again'
			end
		else
			if @num1 = @num2
				begin
					print 'numbers are equal'
					print 'keep trying'
				end
			else
				begin
					print 'something funky here'
					print 'trying to trick me?'
				end
				
	print 'thanks for playing'

multiple_ifs 1, null

--better to handle the NULLs from the start


alter procedure multiple_ifs
	@num1 int = 0, @num2 int = 0
as
	if @num1 is null or @num2 is null
		print 'you must enter a number for each value'
	else
		if @num1 > @num2
			begin
				print 'number 1 is greater than number 2'
				print 'you''re so smart'
			end
		else
			if @num1 < @num2
				begin
					print 'number 1 is less than number 2'
					print 'please try again'
				end
			else
				if @num1 = @num2
					begin
						print 'numbers are equal'
						print 'keep trying'
					end
				else
					begin
						print 'something funky here'
						print 'trying to trick me?'
					end
				
	print 'thanks for playing'

multiple_ifs 1, null

-- letting SQL server data make decisions (p30)
--select * from members order by memdate


create procedure mem_disc
	@memid char(5) = null
as
	declare @level varchar(10), @DISC NUMERIC(5,2), @mdate smalldatetime
	
	--selects membership date based on ID entered by user
	select  @mdate = memdate
	from members
	where mem_id = @memid

	--determine membership level
	if @mdate < '01-01-05'
		begin
			set @level = 'GOLD'
			set @disc = .1
		end
	else
		begin
			set @level = 'WHITE'
			set @disc = 0
		end

	print 'Member ID: ' + @memid
	print 'Membership Level: ' + @level
	print 'Membership Discount: ' + convert(char(5), @disc*100) + '%'

--gold
mem_disc rp090

--white
mem_disc st546

--p31
alter procedure mem_disc
	@memid char(5) = null
as
	declare @level varchar(10), @DISC NUMERIC(5,2), @mdate smalldatetime
	
	--selects membership date based on ID entered by user
	select  @mdate = memdate
	from members
	where mem_id = @memid

	--determine membership level
	if @mdate < '01-01-03'
		begin
			set @level = 'PLATINUM'
			set @disc = .15
		end
	else
		if @mdate < '01-01-05'
			begin
				set @level = 'GOLD'
				set @disc = .1
			end
		else
			begin
				set @level = 'WHITE'
				set @disc = 0
			end

	print 'Member ID: ' + @memid
	print 'Membership Level: ' + @level
	print 'Membership Discount: ' + convert(char(5), @disc*100) + '%'

--gold
mem_disc rp090

--white
mem_disc st546

--platinum
mem_disc jj111

--p33 (now only students are eligible for the GOLD and PLATINUM levels)
alter procedure mem_disc
	@memid char(5) = null
as
	declare @level varchar(10), @DISC NUMERIC(5,2), @mdate smalldatetime, @mcode char(2)
	
	--selects membership date based on ID entered by user
	select  @mdate = memdate, @mcode = memcode
	from members
	where mem_id = @memid

	--determine membership level
	if @mcode = 'st'
		BEGIN --start section for students only
			if @mdate < '01-01-03'
				begin
					set @level = 'PLATINUM'
					set @disc = .15
				end
			else
				if @mdate < '01-01-05'
					begin
						set @level = 'GOLD'
						set @disc = .1
					end
		END --end student section
			else
				begin
					set @level = 'WHITE'
					set @disc = 0
				end

	print 'Member ID: ' + @memid
	print 'Membership Level: ' + @level
	print 'Membership Discount: ' + convert(char(5), @disc*100) + '%'
	print 'Membership Code: ' + @mcode

--select  * from members order by memdate

--gold (no longer gold)
mem_disc rp090

--white (still white)
mem_disc st546

--platinum (still platinum)
mem_disc jj111

--Exercise 3 (p34 & 35)

--RETURN statement to exit a procedure

/*
MY PROCEDURE IS DIFFERENT THAN IN THE BOOK
I ALTERED IT TO SHOW IF A NUMBER IS ACTUALLY
ENTERED. DEMO THIS TO CLASS
*/

alter procedure exit_if_null
	@num int = null
as
	if @num is null
		begin
			print 'you entered a null value. goodbye'
			return --program bails at this point
		end
	else
		print 'you picked a nice number of: ' + convert(char(5),@num)
	
	print 'thanks for playing'

--causes program to bail
exit_if_null

--causes program to bail
exit_if_null 45


--P36
--USING Ex3-3-Challenge solutions

select * from employee

emp_raise A6395, 5

ALTER PROCEDURE emp_raise 
	@emp_id char(5) = NULL, @rating int = 2 
AS 
DECLARE @raise numeric(5,3), @newrate numeric(5,2)
IF @rating = 3
		SET @raise = .05
ELSE 
		IF @rating = 2
			SET @raise = .03
		ELSE 
			IF @rating = 1
				SET @raise = 0

--IF @raise IS NULL  
	else
		BEGIN 
			PRINT 'Enter 1, 2, or 3 as the rating.'
			PRINT 'No change to employee''s hourly rate'
			return --simply adding this makes the NULL test not necessary
		END 
--ELSE 
--		BEGIN 
			PRINT 'Your raise is ' + CONVERT(varchar(5), (@raise * 100))
			    + '%'
			-- update employee's rate in Employee table
			UPDATE employee
			SET hourrate = hourrate * (1 + @raise)
			WHERE empid = @emp_id
			PRINT 'Update complete.'
--		END 
-- get rate from Employee table
SELECT @newrate = hourrate 
FROM employee
WHERE empid = @emp_id
-- display hourly rate	
PRINT 'Hourly rate is: ' + CONVERT(varchar(8), @newrate)

--testing for the EXISTENCE OF DATA (p38 - two different tests)

ALTER PROCEDURE emp_raise 
	@emp_id char(5) = NULL, @rating int = 2 
AS 
	--***********************************
	--Either one of the next two IF statements would work

	--if @emp_id not in (select empid from employee)
	if not exists (select * from employee where empid = @emp_id)
		begin
			print 'Enter a valid ID'
			return
		end
	--***********************************
DECLARE @raise numeric(5,3), @newrate numeric(5,2)
IF @rating = 3
		SET @raise = .05
ELSE 
		IF @rating = 2
			SET @raise = .03
		ELSE 
			IF @rating = 1
				SET @raise = 0

--IF @raise IS NULL  
	else
		BEGIN 
			PRINT 'Enter 1, 2, or 3 as the rating.'
			PRINT 'No change to employee''s hourly rate'
			return --simply adding this makes the NULL test not necessary
		END 
--ELSE 
--		BEGIN 
			PRINT 'Your raise is ' + CONVERT(varchar(5), (@raise * 100))
			    + '%'
			-- update employee's rate in Employee table
			UPDATE employee
			SET hourrate = hourrate * (1 + @raise)
			WHERE empid = @emp_id
			PRINT 'Update complete.'
--		END 
-- get rate from Employee table
SELECT @newrate = hourrate 
FROM employee
WHERE empid = @emp_id
-- display hourly rate	
PRINT 'Hourly rate is: ' + CONVERT(varchar(8), @newrate)

select * from employee

--NOT a real employee ID
emp_raise g9999

--below IS a real employee ID
emp_raise a1465


--CASE function (p39)


	case memcode
		when 'ch' then .25
		when 'st' then .2
		when 'fm' then .1
		else 0
	end


--nest this in a SELECT statement
select mem_id, memcode,
	case memcode
			when 'ch' then .25
			when 'st' then .2
			when 'fm' then .1
			else 0
		end
	as mem_discount
from members

--use CASE in a stored procedure

create procedure simple_case
	@mcode char(2) = NULL
AS
	declare @disc numeric(3,2)
	set @disc = 
		case @mcode
			when 'ch' then .25
			when 'st' then .2
			when 'fm' then .1
			else 0
		end
print 'Member type: ' + @mcode
print 'Discount: ' + convert(varchar(5),@disc*100) + '%'

simple_case st

--SEARCHED CASE

create procedure searched_case
	@age int = 0
AS
	declare @mcode char(2)
	set @mcode = 
		case
			when @age <= 10 then 'ch'
			when @age <= 21 then 'st'
			when @age <= 54 then 'ad'
			else 'sr'
		end
print 'Member type: ' + @mcode

--test
searched_case 34

--add another condition to the CASE structure

alter procedure searched_case
	@age int = 0, @famnum int = 1
AS
	declare @mcode char(2)
	set @mcode = 
		case
			when @famnum > 1 then 'fm'
			when @age <= 10 then 'ch'
			when @age <= 21 then 'st'
			when @age <= 54 then 'ad'
			else 'sr'
		end
print 'Member type: ' + @mcode

--test
searched_case 34, 2


--SQL data (p43)

alter procedure searched_case
	@memid char(5) = null, @age int = 0, @famnum int = 1
AS
	declare @mcode char(2)
	set @mcode = 
		case
			when @famnum > 1 then 'fm'
			when @age <= 10 then 'ch'
			when @age <= 21 then 'st'
			when @age <= 54 then 'ad'
			else 'sr'
		end
--update members table with new code
update members
set memcode = @mcode
where mem_id = @memid
print 'Member ID: ' + @memid
print 'Membership type changed to: ' + @mcode

--select * from members
searched_case  mt907, 34,2


alter procedure searched_case
	@memid char(5) = null, @age int = 0, @famnum int = 1
AS
	declare @mcode char(2), @oldcode char(2)
	set @mcode = 
		case
			when @famnum > 1 then 'fm'
			when @age <= 10 then 'ch'
			when @age <= 21 then 'st'
			when @age <= 54 then 'ad'
			else 'sr'
		end
--get existing code from members table
select @oldcode = memcode
from members
where mem_id = @memid
--only if old code and new code are different
--update members table with new code
if @oldcode <> @mcode
	begin
		update members
		set memcode = @mcode
		where mem_id = @memid
		print 'Member ID: ' + @memid
		print 'Membership type changed to: ' + @mcode
	end
else
	begin
		print 'no change to current code'
		print 'Membership type is: ' + @mcode
	end

searched_case  mt907, 34,2

--LOOPS (p46)

create procedure credit_loop
	@bal numeric(7,2) = 0
as
	declare @pmt numeric(7,2)
	set @pmt = 300
	while @bal > 0
		begin
			set @bal = @bal - @pmt -- adjust the balance
			print 'New balance: $' + convert(char(10),@bal)
		end

--test it
credit_loop 1500
/*
OUTPUT SHOWN BELOW
New balance: $1200.00   
New balance: $900.00    
New balance: $600.00    
New balance: $300.00    
New balance: $0.00 
*/
--now test with 1600
credit_loop 1600
/*
OUTPUT SHOWN BELOW
New balance: $1300.00   
New balance: $1000.00   
New balance: $700.00    
New balance: $400.00    
New balance: $100.00    
New balance: $-200.00
*/

--we need to correct to not go negative

alter procedure credit_loop
	@bal numeric(7,2) = 0
as
	declare @pmt numeric(7,2)
	set @pmt = 300
	while @bal >= @pmt --check to see if there's money!
		begin
			set @bal = @bal - @pmt -- adjust the balance
			print 'New balance: $' + convert(char(10),@bal)
		end
	if @bal > 0
		print 'Final payment: $' + convert(char(10),@bal)

credit_loop 1500 --output is same as above
credit_loop 1600

/*
NEW OUTPUT
New balance: $1300.00   
New balance: $1000.00   
New balance: $700.00    
New balance: $400.00    
New balance: $100.00    
Final payment: $100.00

FINAL LINE IS ADJUSTED
*/

--USING COUNTERS (p48)

alter procedure credit_loop
	@bal numeric(7,2) = 0
as
	declare @pmt numeric(7,2), @num_pmt int
	set @pmt = 300
	set @num_pmt = 0 --initialize the counter to 0
	while @bal >= @pmt --check to see if there's money!
		begin
			set @bal = @bal - @pmt -- adjust the balance
			set @num_pmt = @num_pmt + 1 -- increment the counter
			print 'New balance after payment #' + convert(char(5),@num_pmt) +': $' + convert(char(10),@bal)
		end
	print '' --print blank line
	print 'total number of regular payments until payoff is: ' + convert(char(5),@num_pmt)
	if @bal > 0
		print 'Plus one final payment of: $' + convert(char(10),@bal)


credit_loop 1500 --output is same as above

/*
New balance after payment #1    : $1200.00   
New balance after payment #2    : $900.00    
New balance after payment #3    : $600.00    
New balance after payment #4    : $300.00    
New balance after payment #5    : $0.00      
 
total number of regular payments until payoff is: 5 
*/

credit_loop 1600

/*
New balance after payment #1    : $1300.00   
New balance after payment #2    : $1000.00   
New balance after payment #3    : $700.00    
New balance after payment #4    : $400.00    
New balance after payment #5    : $100.00    
 
total number of regular payments until payoff is: 5    
Plus one final payment of: $100.00 
*/

create procedure savings_loop
	@monthly_savings numeric(7,2)
as
	declare @total_savings numeric(7,2),
	@interest numeric(4,4), @num_months int

	set @total_savings = 0
	set @interest = .0083
	set @num_months = 0

	while @num_months < 12
		begin
			set @total_savings = @total_savings + @monthly_savings
			set	@total_savings = @total_savings + (@total_savings*@interest)
			set @num_months = @num_months + 1
			print 'Savings after ' + convert(char(2),@num_months) + 
					' months: ' + convert(varchar(10),@total_savings)
		end
	print ''
	print 'Final savings: ' + convert(varchar(10),@total_savings)
--test it
savings_loop 100
/*
Savings after 1  months: 100.83
Savings after 2  months: 202.50
Savings after 3  months: 305.01
Savings after 4  months: 408.37
Savings after 5  months: 512.59
Savings after 6  months: 617.67
Savings after 7  months: 723.63
Savings after 8  months: 830.47
Savings after 9  months: 938.19
Savings after 10 months: 1046.81
Savings after 11 months: 1156.33
Savings after 12 months: 1266.76
 
Final savings: 1266.76
*/

alter procedure savings_loop
	@monthly_savings numeric(7,2)
as
	declare @total_savings numeric(7,2),
	@interest numeric(4,4), @num_months int

	set @total_savings = 0
	set @interest = .0083
	set @num_months = 0

	while @num_months < 12
		begin
			set @total_savings = @total_savings + @monthly_savings
			set	@total_savings = @total_savings + (@total_savings*@interest)
			set @num_months = @num_months + 1
			print 'Savings after ' + convert(char(2),@num_months) + 
					' months: ' + convert(varchar(10),@total_savings)
		--decide if you want to loop again
			if @total_savings >= 1000
				begin
					print 'you reached your goal'
					print ''
					break	
				end
		end	
	print ''
	print 'Final savings: ' + convert(varchar(10),@total_savings)

savings_loop 100

/*
NOW LOOP EXITS AFTER EARNING $1000
Savings after 1  months: 100.83
Savings after 2  months: 202.50
Savings after 3  months: 305.01
Savings after 4  months: 408.37
Savings after 5  months: 512.59
Savings after 6  months: 617.67
Savings after 7  months: 723.63
Savings after 8  months: 830.47
Savings after 9  months: 938.19
Savings after 10 months: 1046.81
you reached your goal
 
 
Final savings: 1046.81
*/

alter procedure savings_loop
	@monthly_savings numeric(7,2)
as
	declare @total_savings numeric(7,2),
	@interest numeric(4,4), @num_months int

	set @total_savings = 0
	set @interest = .0083
	set @num_months = 0

	while @num_months < 12
		begin
			set @total_savings = @total_savings + @monthly_savings
			set	@total_savings = @total_savings + (@total_savings*@interest)
			set @num_months = @num_months + 1
			print 'Savings after ' + convert(char(2),@num_months) + 
					' months: ' + convert(varchar(10),@total_savings)
		--decide if you want to loop again
			if @total_savings >= 1000
				begin
					print 'you reached your goal'
					print ''
					CONTINUE	
				end
			PRINT 'KEEP SAVING...'
			PRINT ''	
		end	
	print ''
	print 'Final savings: ' + convert(varchar(10),@total_savings)

savings_loop 100

/*
Savings after 1  months: 100.83
KEEP SAVING...
 
Savings after 2  months: 202.50
KEEP SAVING...
 
Savings after 3  months: 305.01
KEEP SAVING...
 
Savings after 4  months: 408.37
KEEP SAVING...
 
Savings after 5  months: 512.59
KEEP SAVING...
 
Savings after 6  months: 617.67
KEEP SAVING...
 
Savings after 7  months: 723.63
KEEP SAVING...
 
Savings after 8  months: 830.47
KEEP SAVING...
 
Savings after 9  months: 938.19
KEEP SAVING...
 
Savings after 10 months: 1046.81
you reached your goal
 
Savings after 11 months: 1156.33
you reached your goal
 
Savings after 12 months: 1266.76
you reached your goal
 
 
Final savings: 1266.76
*/

CREATE PROcedure infinite_loop
	@num int = 0
as
	--condition is always true
	while 1 = 1
		begin
			print 'your number is: ' + convert(char(3),@num)
				--break out of the loop if number is 5 and today is monday
			if @num = 5 and datename(dw,getdate()) = 'monday'
				break
			else
				if @num > 10
					break
			set @num = @num + 1	
		end

infinite_loop

/*
your number is: 0  
your number is: 1  
your number is: 2  
your number is: 3  
your number is: 4  
your number is: 5  
your number is: 6  
your number is: 7  
your number is: 8  
your number is: 9  
your number is: 10 
your number is: 11 
*/

alter procedure infinite_loop
	@num int = 0
as
	--condition is always true
	while 1 = 1
		begin
			print 'your number is: ' + convert(char(3),@num)
				--break out of the loop if number is 5 and today is monday
			if @num = 5 and datename(dw,getdate()) = 'thursday'
				break
			else
				if @num > 10
					break
			set @num = @num + 1	
		end

infinite_loop

/*
I altered the code as above and ran this on a thursday!
your number is: 0  
your number is: 1  
your number is: 2  
your number is: 3  
your number is: 4  
your number is: 5 
*/

create procedure member_loop
as
	declare @memyear int, @total_mems int, @new_mems int
	set @total_mems = 0
	
	select @memyear = datepart(yyyy, min(memdate)) from members
	
	while @memyear < datepart(yyyy, getdate())
		begin
			select @new_mems = count(*) from members
			where datepart(yyyy, memdate) = @memyear
			set @total_mems = @total_mems + @new_mems
			print 'year: ' + convert(char(4), @memyear)
			print 'New members: ' + convert (char(3),@new_mems)
			print 'Total members: ' + convert(char(3),@total_mems)
			print ''
			set @memyear = @memyear + 1
		end 

member_loop

/*
year: 2002
New members: 2  
Total members: 2  
 
year: 2003
New members: 2  
Total members: 4  
 
year: 2004
New members: 2  
Total members: 6  
 
year: 2005
New members: 5  
Total members: 11 
 
year: 2006
New members: 8  
Total members: 19 
 
year: 2007
New members: 1  
Total members: 20 
 
year: 2008
New members: 0  
Total members: 20 
 
year: 2009
New members: 0  
Total members: 20 
 
year: 2010
New members: 0  
Total members: 20 
 
year: 2011
New members: 0  
Total members: 20 
 
year: 2012
New members: 0  
Total members: 20 
 
year: 2013
New members: 0  
Total members: 20
*/

--Exercise 5

--transations in procedures (p55)

alter PROCEDURE trg_raise_loop
AS 
	DECLARE @avg_rate numeric(7,2)
	-- get current average rate
	SELECT @avg_rate = AVG(hourrate)
	FROM employee
	WHERE jobcode = 'TRG'
	PRINT 'Initial average rate: $' + CONVERT(char(10), @avg_rate)
	-- use loop to assign 5% raises until average >= $13/hour
	WHILE @avg_rate < 13
		BEGIN 
		--start new transaction
			begin transaction		
			UPDATE employee
			SET hourrate = hourrate * 1.05
			WHERE jobcode = 'TRG'
			-- calculate new average
			SELECT @avg_rate = AVG(hourrate)
			FROM employee
			WHERE jobcode = 'TRG'
			-- print new average
			PRINT 'New average rate: ' + CONVERT(char(10), @avg_rate)
	--if over limit rollback last update
		if @avg_rate > 13
			begin
			print 'average exceeds limit'
			print 'rollback last update'
			rollback
			end	
	--if average within limits commit
		else
			begin
				print 'average still within limit'
				print 'commit last update'
				commit
			end
			
		END

trg_raise_loop

select avg(hourrate) from employee
where jobcode = 'trg'

--cursors (p57)

