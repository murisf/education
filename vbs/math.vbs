option explicit

'declare the variable
dim intMyAnswer

'assign a value to the variable
intMyAnswer = 7 + 3

'use the variable
msgbox  "7 + 3 = " & intMyAnswer & vbcrlf & _
 "Thanks for Mathing with us!"

'msgbox 7 - 3
'msgbox 7 * 3
'msgbox 7 / 3 
'msgbox 7 \ 3
'msgbox 7 mod 3

'msgbox 7 * 7 * 7
'msgbox 7 ^ 3

'msgbox 3 + 7 * 3
'msgbox (3 + 7) * 3