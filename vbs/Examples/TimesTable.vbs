option explicit

dim FsoObject,tblRpt,x

dim mult

set FsoObject = WScript.CreateObject("Scripting.FileSystemObject")

set tblRpt = FsoObject.OpenTextFile("c:\rpt.txt",2,true)

mult = inputbox("Which times table do you want to see? (1,2,3,etc")

tblRpt.writeline("The " & mult & " times tables")
tblRpt.writeline("=====================")

for x = 1 to 10

	tblRpt.writeline(mult & " x " & x & " = " & mult * x & chr(10))

next



