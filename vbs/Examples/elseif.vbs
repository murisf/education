Option Explicit
Dim intAge
intAge = InputBox ("Please Enter your age: ")
	If intAge <= 0 Then
		MsgBox ("Invalid age!")
	ElseIf intAge < 13 Then
		MsgBox ("Still a child")
	ElseIf intAge < 20 Then
		MsgBox ("Teenager")
	ElseIf intAge < 30 Then
		MsgBox ("In the prime of life!")
	Else
		MsgBox ("Wise beyond belief!")
	End If
