Option Explicit
Dim intX, intMyVal, intTotal
intTotal = 100
intMyVal = InputBox("Enter your subtraction value")
For intX = 1 TO 10
    intTotal = intTotal - intMyVal
    if intTotal < 0 then
        exit for
    end if
Next
msgBox "Loop Terminated after " & intX & " runs."
