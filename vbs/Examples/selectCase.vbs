Option Explicit
Dim varMemCode
Dim sngDiscountRate
varMemCode = InputBox("What is your membership category?")
Select Case varMemCode
   Case "CH"
      sngDiscountRate = 0.5
   Case "ST"
      sngDiscountRate = 0.25
   Case "SR"
      sngDiscountRate = 0.3
   Case Else
      sngDiscountRate = 0
End Select
MsgBox("Your discount is " & sngDiscountRate*100 & "%.")
