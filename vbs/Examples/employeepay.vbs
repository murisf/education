Option Explicit
Dim asngHours(4)
Dim sngHourPay, sngEmpPay
Dim sngTotalHours, sngTotalPay
Dim intX

sngHourPay = 6.5

For intX = 0 TO 4
asngHours(intX) = CInt(InputBox ("Enter hours worked by employee " & intX+1))
sngTotalHours = sngTotalHours + asngHours(intX)
Next

For intX = 0 To 4
sngEmpPay = asngHours(intX) * sngHourPay
MsgBox ("The pay for employee " & intX+1 & " is: " & FormatCurrency(sngEmpPay, 2))
Next

sngTotalPay = sngHourPay * sngTotalHours

MsgBox ("The total pay for all employees is " & FormatCurrency(sngTotalPay, 2))
