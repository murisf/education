Option Explicit
Dim intAmount, intTotal
intTotal = 0
Do
   intAmount = InputBox ("Enter a positive amount (or a negative number to quit): ")
   If intAmount >= 0 Then
      intTotal = intTotal + intAmount
   End If
Loop While intAmount >= 0
MsgBox "The total amount entered was " & intTotal & "."
