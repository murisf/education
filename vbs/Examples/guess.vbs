Option Explicit
Dim intGuess, intNumber, intCount
Randomize
intNumber = Int(Rnd * 1000) + 1
MsgBox ("I am thinking of a number between one and one thousand!")
MsgBox ("Can you guess it?")
intCount = 0
Do While intGuess <> intNumber
   intGuess = CInt(InputBox("Guess: "))
   intCount = intCount + 1
   If intGuess < intNumber Then
      MsgBox ("   ... Higher than " & intGuess)
   End If
   If intGuess > intNumber Then
      MsgBox ("   ... Lower than " & intGuess)
   End If
Loop
MsgBox ("That's it!  You guessed it!")
MsgBox ("The number is " & intNumber & "!")
MsgBox ("And it only took you " & intCount & " guesses!")
