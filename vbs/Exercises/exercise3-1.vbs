Option Explicit
Dim sngLowPrice, sngHighPrice
Dim sngPounds, sngTotal
sngLowPrice = .79
sngHighPrice = .99
sngPounds = InputBox("How many pounds of jellybeans would you like?")
If sngPounds >= 3 Then
   sngTotal = sngPounds * sngLowPrice
Else
   sngTotal = sngPounds * sngHighPrice
End If
MsgBox("Your total cost is: " & FormatCurrency(sngTotal))
