Option Explicit
Dim intAge, intDifference
intAge = InputBox ("How old are you?")
If intAge >= 16 Then
   MsgBox ("You can drive.")
Else
   intDifference = 16 - intAge
   MsgBox ("You must wait " & intDifference & " more years.")
End If
