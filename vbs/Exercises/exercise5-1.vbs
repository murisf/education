Option Explicit
Dim sngTotal, sngAveSale
Dim intStore, sngDailySales
sngTotal = 0
intStore = 0
Do
   intStore = intStore + 1
   sngDailySales = InputBox ("Enter the daily sales for store " & intStore & " (type 0 to end).")
   sngTotal = sngTotal + sngDailySales
Loop While sngDailySales <> 0
sngAveSale = sngTotal/(intStore-1)
MsgBox ("Total sales for all stores: " & FormatCurrency(sngTotal, 2))
MsgBox ("Average sales for each store: " & FormatCurrency(sngAveSale, 2))


