Option Explicit
Dim sngRate, intDay, sngHours
Dim sngTotal, sngGrossPay, sngAvePay
sngRate = 6.5
intDay = 1
sngTotal = 0
Do While intDay <= 5
   sngHours = cInt(InputBox ("Enter your hours for day " & intDay & ":"))
   sngTotal = sngTotal + sngHours
   intDay = intDay + 1
Loop
sngGrossPay = sngTotal * sngRate
sngAvePay = sngGrossPay / (intDay - 1)
MsgBox ("Your average daily pay is " & FormatCurrency(sngAvePay, 2))
MsgBox ("Your gross wages for the week are " & FormatCurrency(sngGrossPay, 2))
