Option Explicit
Dim sngAutoVal, intExpLife
Dim sngAnnualDep, intYears, sngDepreciate
sngAutoVal = InputBox("What is the value of the automobile?")
intExpLife = CInt(InputBox("What is the expected life of the automobile?"))
sngAnnualDep = sngAutoVal / intExpLife
sngDepreciate = sngAutoVal
intYears = 1
Do While intYears <= intExpLife
   sngDepreciate = sngDepreciate - sngAnnualDep
   MsgBox ("After Year " & intYears & " auto will be worth " & FormatCurrency(sngDepreciate, 2))
   intYears = intYears + 1
Loop

