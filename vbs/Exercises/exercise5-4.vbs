Option Explicit
Dim intDays
Dim sngGallonsHave, sngGallonsNeed, sngRate
  
sngGallonsHave = 5
sngGallonsNeed = 10000
sngRate = 0.5
    
Do
   sngGallonsHave = sngGallonsHave * sngRate + sngGallonsHave
   intDays = intDays + 1
   
   msgbox "On day " & intDays & chr(10) & "we have " & sngGallonsHave & " gallons"
   
Loop Until sngGallonsHave >= sngGallonsNeed
    
MsgBox("It took " & intDays & " days to reach " & FormatNumber(sngGallonsHave, 0) & " gallons.")
