Option Explicit
Dim intX, intY, sngTotal, sngPercent
Dim asngSales(4)
For intX = 1 To 5
   asngSales(intX-1) = cInt(InputBox("Enter the sales for store " & intX & ": "))
   sngTotal = sngTotal + asngSales(intX-1)
Next

MsgBox ("The total sales for all five stores is: " & FormatCurrency(sngTotal, 2))

For intY = 1 To 5
   sngPercent = (asngSales(intY-1)/sngTotal)*100
   MsgBox ("Store: " & intY & " Sales: " & FormatCurrency(asngSales (intY-1), 2) & _
   									 " Percent of Total: " & FormatNumber(sngPercent, 1))
Next


