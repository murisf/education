Option Explicit
Dim intGrade
intGrade = InputBox("What is the student's numerical grade?")
Select Case intGrade
   Case 90, 95, 100
      MsgBox("The grade is an A.")
   Case 80, 85
      MsgBox("The grade is a B.")
   Case 70, 75
      MsgBox("The grade is a C.")
   Case 60, 65
      MsgBox("The grade is a D.")
   Case 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55
      MsgBox("The grade is an F.")
   Case Else
      MsgBox("The grade must be divisible by 5.")
End Select

