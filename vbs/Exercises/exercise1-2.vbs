option explicit
Dim a
Dim b
Dim c
a = 7 + 2 * 3
b = 12 / 2 * 3
c = (6 - 1 * 3) / 5
MsgBox ("a = " & a & " b = " & b & " c = " & c)

'below are the correct answers to the equations above
'a=13, b=18, c=0.6