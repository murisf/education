Option Explicit
Dim sngJJRate, sngSSRate
Dim sngJJBalance, sngSSBalance, intYear
MsgBox ("John Jones deposits $500.00 at 9%.")
MsgBox ("Stan Smith deposits $600.00 at 5%.")
sngJJRate = .09
sngSSRate = .05
sngJJBalance = 500
sngSSBalance = 600
intYear = 2015
Do
   sngJJBalance = sngJJBalance + sngJJBalance * sngJJRate
   sngSSBalance = sngSSBalance + sngSSBalance * sngSSRate
   intYear = intYear + 1
Loop While sngJJBalance <= sngSSBalance
MsgBox ("Assuming that the current year is 2015 ... ")
MsgBox ("John will have more money than Stan in the year " & intYear-1 & ".")
