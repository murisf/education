Option Explicit
Dim intNumber(3)
Dim intX
For intX = 1 To 4
   intNumber(intX-1) = InputBox("Enter integer " & intX & " of 4: ")
Next
MsgBox ("The integers in reverse order are: ")
For intX = 4 To 1 Step -1
   MsgBox intNumber(intX-1)
Next
