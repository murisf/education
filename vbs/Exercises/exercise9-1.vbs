Option Explicit
Dim sngWholesalePrice, sngFinalCost
Dim intCode, strAnswer
Dim asngMarkup(3)
asngMarkup(0) = .2
asngMarkup(1) = .25
asngMarkup(2) = .4
asngMarkup(3) = .45
Do
   sngWholesalePrice = InputBox("Enter the wholesale price: ")
   Do
      intCode = InputBox("Enter the markup code (1-4): ")
   Loop While intCode < 1 OR intCode > 4
   sngFinalCost = sngWholesalePrice * (1 + asngMarkup(intCode-1))
   MsgBox ("The final cost is: " & FormatCurrency(sngFinalCost, 2))
   strAnswer = InputBox("Do you wish to calculate another (y/n)?")
Loop While strAnswer = "Y" or strAnswer = "y"
