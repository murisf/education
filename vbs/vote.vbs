option explicit

dim intAge, strState

intAge = inputbox("Enter your age")
strState = inputbox("Enter your state - MO, IL, TX....")

if intAge >= 18 and (strState = "MO"  or strState = "mo" or strState = "Missouri" or strState = "missouri") then
	msgbox "You can vote!"
	msgbox "See you in April"
else 
	msgbox "Too young or wrong state"	
end if

msgbox "Thanks for using the Vote-O-Matic!"