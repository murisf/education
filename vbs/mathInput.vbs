option explicit

'declare the variable
dim intMyAnswer, intFirstNumber, intSecondNumber

'get the two numbers
intFirstNumber = cint(inputbox("Enter the first number"))
intSecondNumber = cint(inputbox("Enter the second number"))

'assign a value to the variable
'intMyAnswer = 7 + 3
intMyAnswer = intFirstNumber + intSecondNumber

'use the variable
msgbox  intFirstNumber & " + " & intSecondNumber & " = " & intMyAnswer & vbcrlf & _
 "Thanks for Mathing with us!"

