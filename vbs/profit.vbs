option explicit
dim sngSales, sngExpenses, sngProfit, strYourName

strYourName = inputbox("Enter your name")

sngSales = inputbox(strYourName & ", Enter your sales" , "Enter Sales" , 3000)
sngExpenses = inputbox(strYourName & ", Enter your expenses")

sngProfit = sngSales - sngExpenses

msgbox strYourName & ", Your profit is " & formatcurrency(sngProfit,2)